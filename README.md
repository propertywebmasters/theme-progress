# Installation
- composer require propertywebmasters/progress-theme

# Publish Assets (Required)
- php artisan vendor:publish --tag=progress-theme-assets

# Publish Blade Files
- php artisan vendor:publish --tag=progress-theme-views
