// Tab
function openTab(evt, cityName) {
    // Declare all variables
    var i, tabcontent, tablinks;

    // Get all elements with class="tabcontent" and hide them
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    // Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }

    // Show the current tab, and add an "active" class to the button that opened the tab
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
}

document.addEventListener("DOMContentLoaded", function (event) {
    const navigation = document.querySelector('#navigation-alt');

    customScrollBody();

    window.onscroll = function (e) {
        customScrollBody();
    }

    function customScrollBody() {
        if (navigation !== null) {
            const { pageYOffset } = window;

            if (pageYOffset > 0) {
                navigation.classList.remove('relative', 'bg-transparent');
                navigation.classList.add('top-0', 'fixed');
            } else {
                navigation.classList.remove('top-0', 'fixed');
                navigation.classList.add('relative', 'bg-transparent');
            }
        }
    }

    var mySwiperProperties = new Swiper(".mySwiperProperties", {
        slidesPerView: 1,
        spaceBetween: 30,
        loop: false,
        allowTouchMove: false,
        navigation: {
            prevEl: ".swiper-button-nextBtn",
            nextEl: ".swiper-button-prevBtn",
        },
        breakpoints: {
            "768": {
                slidesPerView: 1,
                spaceBetween: 10,
            },
            "992": {
                slidesPerView: 2,
                spaceBetween: 20,
            }
        },
    });

    let siteSlideSpeed = 8000;
    let slidesWrapper = document.getElementById('home-slides');

    if (slidesWrapper) {
        siteSlideSpeed = slidesWrapper.getAttribute('data-speed');
    }

    // Hero
    var mySwiperHero = new Swiper(".mySwiperHero", {
        slidesPerView: 1,
        loop: false,
        autoplay: {
            delay: siteSlideSpeed,
        },
        pagination: {
            el: ".swiper-pagination-hero",
            clickable: true,
        },
    });

    // mySwiperPropertiesInner
    var mySwiperPropertiesInner = new Swiper(".mySwiperPropertiesInner", {
        slidesPerView: 1,
        spaceBetween: 30,
        loop: false,
        navigation: {
            prevEl: ".swiper-button-nextInner",
            nextEl: ".swiper-button-prevInner",
        },

    });

    // Testimonial
    var Testimonial = new Swiper(".mySwiperTestimonial", {
        slidesPerView: 1,
        spaceBetween: 30,
        loop: false,

        pagination: {
            el: ".swiper-paginationT",
            clickable: true,
        },
    });

    // listings

    var swiper = new Swiper(".mySwiper", {
        slidesPerView: 1,
        spaceBetween: 30,
        initialSlide: 2,
        centeredSlides: true,
        loop: true,
        navigation: {
            nextEl: ".swiper-button-next",
            prevEl: ".swiper-button-prev",
        },
        pagination: {
            el: ".swiper-pagination",
            clickable: true,
        },
        breakpoints: {
            "768": {
                slidesPerView: 1,
                spaceBetween: 10,
            },
            "992": {
                slidesPerView: 2,
                spaceBetween: 20,
            },
            "1199": {
                slidesPerView: 3,
                spaceBetween: 40,
            }
        },
    });

    var swiperS = new Swiper(".mySwiperS", {
        slidesPerView: 1,
        spaceBetween: 30,
        centeredSlides: true,
        loop: true,
        navigation: {
            nextEl: ".swiper-button-next",
            prevEl: ".swiper-button-prev",
        },
        pagination: {
            el: ".swiper-pagination",
            clickable: true,
        },
    });

    // property detail
    var swiperDoubleImage = new Swiper(".mySwiperDoubleImage", {
        slidesPerView: 1,
        loop: true,
        pagination: {
            el: ".swiper-pagination",
            type: "fraction",
        },
        navigation: {
            nextEl: ".swiper-arrow-right",
            prevEl: ".swiper-arrow-left",
        },
        breakpoints: {
            "768": {
                slidesPerView: 1,
            },
            "992": {
                slidesPerView: 2,
            },
            "1199": {
                slidesPerView: 2,
            }
        },
    });

    // property detail
    var swiperSingleImage = new Swiper(".mySwiperSingleImage", {
        slidesPerView: 1,
        loop: true,
        pagination: {
            el: ".swiper-pagination",
            type: "fraction",
        },
        navigation: {
            nextEl: ".swiper-arrow-right",
            prevEl: ".swiper-arrow-left",
        },
        breakpoints: {
            "768": {
                slidesPerView: 1,
            },
            "992": {
                slidesPerView: 1,
            },
            "1199": {
                slidesPerView: 1,
            }
        },
    });

    const hamburger = document.getElementById('hamburger');
    if (hamburger !== null) {
        hamburger.addEventListener('click', function () {
            document.getElementsByTagName('body')[0].classList.toggle('noscroll');
            const nav = document.querySelector('.navigation-bg nav');
            if (nav.classList.contains('bg-transparent')) {
                nav.classList.remove('bg-transparent');
            }
        });
    }

    const fullScreenFilterToggle = document.querySelector('.full-screen-filter-toggle'),
        closeFullScreenFilter = document.querySelector('.close-full-screen-filter'),
        navigationAlt =  document.querySelector('#navigation-alt');

    if (fullScreenFilterToggle) {
        fullScreenFilterToggle.addEventListener('click', function () {
            if (navigationAlt) {
                navigationAlt.classList.remove('z-40');
            }
        });
    }

    if (closeFullScreenFilter) {
        closeFullScreenFilter.addEventListener('click', function () {
            if (navigationAlt) {
                navigationAlt.classList.add('z-40');
            }
        });
    }

    document.querySelectorAll('.location-single').forEach(function (location) {
        location.addEventListener('click', function () {
            window.location.href = this.dataset.url;
        });
    });
});

