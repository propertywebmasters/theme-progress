@extends('layouts.app')

@section('content')

    @push('open-graph-tags')
        @include(themeViewPath('frontend.components.home-open-graph'))
    @endpush

    {{-- site header component, includes navigation --}}
    @include(themeViewPath('frontend.components.header'))

    {{-- Primary hero element --}}
    @include(themeViewPath('frontend.components.hero'))

    @if(isset($reviewServices))
        @include(themeViewPath('frontend.components.hero-reviews'))
    @endif

    @include(themeViewPath('frontend.components.popular-searches'))

    {{-- Featured properties band --}}
    <div class="bg-white">
        @include(themeViewPath('frontend.components.featured-properties'), ['customHeader' => trans('header.featured_properties'), 'properties' => $featuredProperties])
    </div>

    {{-- Features band --}}
    @include(themeViewPath('frontend.components.features'))

    {{-- property alert signup band --}}
    @include(themeViewPath('frontend.components.property-alert-signup'))

    {{-- About us band --}}
    @include(themeViewPath('frontend.components.about'))

    {{-- Latest news band --}}
    @include(themeViewPath('frontend.components.book-valuation'))

    {{-- Latest news band --}}
    @include(themeViewPath('frontend.components.latest-news'))

    {{-- Testimonials band --}}
    @include(themeViewPath('frontend.components.latest-videos'))

    {{-- Testimonials band --}}
    @include(themeViewPath('frontend.components.testimonials'))

    <!-- ========================== Book a valuation ========================= -->
    @include(themeViewPath('frontend.components.newsletter-signup'))

    {{-- site footer --}}
    @include(themeViewPath('frontend.components.footer'))

@endsection
