@php use App\Models\Property; @endphp
@php use App\Models\TenantFeature; @endphp
@extends('layouts.app')

@push('open-graph-tags')
    @include(themeViewPath('frontend.components.property.open-graph'))
@endpush

@section('content')

    @php
        $swiperId = $property->uuid;
        $imageString = $captionString = '';
        foreach ($property->images as $image) {
            $imageString .= getPropertyImage($image->filepath, 'xl').'|';
            $captionString .= $image->caption.'|';
        }
        $imageString = trim($imageString, '|');
        $captionString = trim($captionString, '|');
        $imageCount = $property->images->count()
    @endphp

    {{-- site header component, includes navigation --}}
    @include(themeViewPath('frontend.components.header'), ['transparentNavigation' => false,])

    @php
        $useSinglePropertyImage = hasFeature(TenantFeature::FEATURE_THEME_PROGRESS_SINGLE_PROPERTY_IMAGE);
        if ($useSinglePropertyImage) {
            $galleryClass = 'h-82 min-h-82 max-h-82 md:min-h-132 md:h-180 md:max-h-180';
            $swiperClass = 'mySwiperSingleImage';
        } else {
            $galleryClass = 'h-82 min-h-82 max-h-82 md:min-h-132 md:h-132 md:max-h-132';
            $swiperClass = 'mySwiperDoubleImage';
        }
    @endphp

    <section class="block relative">
        <div class="s-slider swiper {{ $swiperClass }} overflow-hidden md:p-0 z-1 list-none relative z-10 swiper-initialized swiper-horizontal swiper-pointer-events">
            <div class="swiper-wrapper flex z-1 relative w-full transition-transform box-content slider" id="swiper-wrapper-{{ $swiperId }}" aria-live="polite"
                 style="transform: translate3d(0px, 0px, 0px); transition-duration: 0ms;" data-images="{{ $imageString }}" data-captions="{{ $captionString }}">
                @php $i = 1 @endphp
                @foreach ($property->images as $image)
                    <div class="swiper-slide flex-shrink-0" role="group" aria-label="{{ $i }} / {{ $property->images->count() }}">
                        <img class="start-gallery cursor-pointer w-full {{ $galleryClass }} object-cover" src="{{ getPropertyImage($image->filepath, 'xl') }}" alt="img"
                             loading="lazy">
                    </div>
                    @php $i++ @endphp
                @endforeach
            </div>
            @if($property->images->count() > 0)
                <!-- Slider Arrow -->
                <div class="swiper-arrow-left absolute top-1/2 left-4 -mt-2 z-20 cursor-pointer swiper-button-disabled opacity-50" tabindex="-1" role="button"
                     aria-label="Previous slide" aria-controls="swiper-wrapper-{{ $swiperId }}" aria-disabled="true">
                    <img src="{{ themeImage('property/arrow-left.svg') }}" alt="arrow" loading="lazy">
                </div>
                <div class="swiper-arrow-right absolute top-1/2 right-4 -mt-2 z-20 cursor-pointer" tabindex="0" role="button" aria-label="Next slide"
                     aria-controls="swiper-wrapper-{{ $swiperId }}" aria-disabled="false">
                    <img src="{{ themeImage('property/arrow-right.svg') }}" alt="arrow" loading="lazy">
                </div>
                <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span>
            @endif
        </div>

        @if(stripos(url()->previous(), tenantManager()->getTenant()->domain) !== false)
            <div class="absolute w-full z-20 top-6 left-0">
                <div class="container mx-auto px-4">
                    <div class="grid grid-cols-1 sm:grid-cols-2 items-center">
                        <div class="text-left">
                            <a href="{{ url()->previous() }}"
                               class="gap-x-3 inline-block bg-black px-4 py-2 rounded-full text-xs text-white hover:bg-white hover:text-black transition-all duration-500">
                                <img class="inline-block align-middle w-3" src="{{ themeImage('property/small-arrow-left.svg') }}" alt="arrow" loading="lazy">
                                <span class="inline-block align-middle font-normal text-sm">{{ trans('search.back_to_search') }}</span>
                            </a>
                        </div>

                        @if($property->tender_status !== Property::TENDER_STATUS_AVAILABLE)
                            <div class="sm:text-right">
                                <span class="tender-status primary-bg text-white p-2 px-3 inline-block mt-2 sm:mt-0 rounded-3xl">{{ $property->tender_status }}</span>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        @endif
        <div class="absolute w-full z-20 bottom-4 left-0">
            <div class="container mx-auto px-4">
                <div class="flex justify-between">
                    <div class="gap-x-2">
                        @if($useMap && $property->location->latitude !== null && $property->location->longitude !== null)
                            <span data-target="property-location-modal"
                                  class="modal-button cursor-pointer text-xs md:text-sm text-center tracking-wide rounded-3xl border border-white bg-white max-w-xs inline-block py-1 px-2 md:py-2 md:px-5 transition-all hover:text-white text-primary hover:bg-activeCcolor hover:border-activeCcolor duration-500">{{ trans('header.location') }}</span>
                        @endif

                        @if(!hasFeature(TenantFeature::FEATURE_DOCUMENTS_BY_TYPE))
                            @if ($property->documents->count() > 0)
                                <span data-target="property-documents-modal"
                                      class="modal-button cursor-pointer text-xs md:text-sm text-center tracking-wide rounded-3xl border border-white bg-white max-w-xs inline-block py-1 px-2 md:py-2 md:px-5 transition-all hover:text-white text-primary hover:bg-activeCcolor hover:border-activeCcolor duration-500">{{ trans('header.documents') }}</span>
                            @endif
                        @else
                            @include(themeViewPath('frontend.components.property.document-by-type-buttons'))
                        @endif

                        @if ($property->video_url !== null)
                            <span data-target="property-video-modal"
                                  class="modal-button cursor-pointer text-xs md:text-sm text-center tracking-wide rounded-3xl border border-white bg-white max-w-xs inline-block py-1 px-2 md:py-2 md:px-5 transition-all hover:text-white text-primary hover:bg-activeCcolor hover:border-activeCcolor duration-500">{{ trans('header.video') }}</span>
                        @endif

                        @if ($property->virtual_tour_url)
                            <span data-target="property-virtual-tour-modal"
                                  class="modal-button cursor-pointer text-xs md:text-sm text-center tracking-wide rounded-3xl border border-white bg-white max-w-xs inline-block py-1 px-2 md:py-2 md:px-5 transition-all hover:text-white text-primary hover:bg-activeCcolor hover:border-activeCcolor duration-500">{{ trans('header.virtual_tour') }}</span>
                        @endif
                    </div>
                    <div>
                        @if(user())
                            <div class="inline-block mr-4">
                                <a data-target="property-note-modal" href="javascript:" class="modal-button secondary-bg text-white rounded-full p-2 px-6 text-sm tracking-wide">
                                    {{ trans('button.add_property_note') }}
                                </a>
                            </div>
                        @endif
                        <div class="inline-block">
                            <!-- Pagination -->
                            <div
                                class="swiper-pagination bg-white py-1 px-2 md:py-2 md:px-5 rounded-full text-primary swiper-pagination-fraction swiper-pagination-horizontal whitespace-nowrap text-xs md:text-sm">
                                <span class="swiper-pagination-current text-xs md:text-sm">1</span> / <span class="swiper-pagination-total text-xs md:text-sm">5</span>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="bg-white py-6 md:py-10 z-50 transition-all duration-500" id="myHeader">
        <div class="container mx-auto px-4">
            @include(themeViewPath('frontend.components.system-notifications'), ['customClass' => 'mb-6'])

            <div class="flex justify-between flex-col lg:flex-row">
                <!-- Left Side -->
                <div>
                    <h3 class="md:text-4xl text-2xl header-text">{{ $property->displayName() }}</h3>
                    <p class="md:my-4 my-2 tracking-tight text-gray-600 ">{{ $property->location->displayAddress() }}</p>

                    @include(themeViewPath('frontend.components.property.bed-bath-info'), ['showSizing' => true])

                </div>

                <!-- Right side -->
                <div>
                    <div class="flex flex-col lg:flex-row items-start lg:items-end gap-x-3 lg:justify-end md:mt-8 mt-2 lg:mt-0">
                        @if(hasFeature(TenantFeature::FEATURE_STAMP_DUTY_CALCULATOR) && $property->tender_type === Property::TENDER_TYPE_SALE)
                            <a data-target="stamp-duty-calculator-modal" href="javascript:"
                               class="modal-button text-xs leading-loose font-light text-browngrey underline">{{ trans('header.stamp_duty_calculator') }}</a>
                        @endif
                        @if(hasFeature(TenantFeature::FEATURE_MORTGAGE_CALCULATOR) && $property->tender_type === Property::TENDER_TYPE_SALE)
                            <a data-target="mortgage-calculator-modal" href="javascript:"
                               class="modal-button text-xs leading-loose font-light text-browngrey underline">{{ trans('header.mortgage_calculator') }}</a>
                        @endif
                        <span class="text-2xl tracking-tight secondary-text pt-2 md:pt-0">{!! $property->displayPrice() !!}</span>
                    </div>
                    <p class="text-xs lg:text-right tracking-tight font-light mt-1 pb-8">Ref: {{ $property->displayReference() }}</p>
                    <div class="flex gap-x-2 sm:flex-row gap-y-4 sm:gap-y-0">
                        <a class="md:text-sm text-xs text-center tracking-wide rounded-3xl max-w-xs block py-3 md:px-8 px-3 transition-all cta hover:bg-hover hover:text-white duration-500 whitespace-nowrap"
                           href="{{ url()->current() }}#enquire">
                            {{ trans('button.enquire_now') }}
                        </a>

                        @if(hasFeature(TenantFeature::FEATURE_ARRANGE_VIEWING_SYSTEM))
                            <span data-target="book-viewing-modal"
                                  class="modal-button cursor-pointer md:text-sm text-xs text-center tracking-wide cta-border rounded-3xl border max-w-xs block py-3 md:px-8 px-3 transition-all duration-500 whitespace-nowrap">
                                {{ trans('header.arrange_viewing') }}
                            </span>
                        @endif

                        @if(hasFeature(TenantFeature::FEATURE_SHORTLIST_SYSTEM))
                            @if (user() === null)
                                @php
                                    $actionClass = 'modal-button';
                                    $dataAttribs = 'data-target="preauth-modal"';
                                    $imgClass = '';
                                @endphp
                            @else
                                @php
                                    if (in_array($property->uuid, $shortlistedProperties->pluck('property_uuid')->toArray())) {
                                        // this property is in the shortlist
                                        $actionClass = 'shortlist-toggle-simple';
                                        $dataAttribs = 'data-on-class="primary-text fill-current stroke-current" data-property="'.$property->uuid.'"';
                                        $imgClass = 'primary-text fill-current stroke-current';
                                    } else {
                                        // this property is in the shortlist
                                        $actionClass = 'shortlist-toggle-simple';
                                        $dataAttribs = 'data-on-class="primary-text fill-current stroke-current" data-property="'.$property->uuid.'"';
                                        $imgClass = '';
                                    }
                                @endphp
                            @endif

                            <a class="{{ $actionClass }} cursor-pointer hover-lighten text-xs text-center tracking-wide rounded-3xl max-w-xs flex py-3 md:px-8 px-3 transition-all secondary-bg hover:bg-hover text-white gap-x-2 whitespace-nowrap"
                               href="javascript:" title="{{ trans('shortlist.save') }}" {!! $dataAttribs !!}>
                                <span class="text-xs md:text-sm">Save</span> <img src="{{ themeImage('search/heart.svg') }}" class="svg-inject w-3.5 h-auto {{ $imgClass }}"
                                                                                  alt="love" loading="lazy">
                            </a>
                        @endif

                        {{-- <span class="cursor-pointer hover-lighten text-xs text-center tracking-wide rounded-3xl border max-w-xs flex py-3 md:px-8 px-3 transition-all secondary-bg hover:bg-hover text-white gap-x-2 whitespace-nowrap" href="javascript:;">
                            <span class="text-xs md:text-sm">Save</span> <img class="pr-2" src="{{ themeImage('search/heart.svg') }}" alt="heart" loading="lazy">
                        </span> --}}
                    </div>
                </div>


            </div>
        </div>
    </section>

    <section class="bg-whiter pb-4 lg:pb-0 content">
        <div class="container mx-auto px-4">
            <div class="lg:grid lg:grid-cols-12 lg:gap-x-8">
                <div class="col-span-7 lg:pr-7">
                    <!-- Tab links -->
                    <div class="relative pt-4">

                        <div class="tab overflow-hidden border-b-2">
                            <button class="tablinks active px-3 sm:px-6 py-3 text-xs sm:text-sm md:text-lg"
                                    onclick="openTab(event, 'description')">{{ trans('header.property_description') }}</button>
                            @if($property->features->count() > 0)
                                <button class="tablinks px-3 sm:px-6 py-3 text-xs sm:text-sm md:text-lg"
                                        onclick="openTab(event, 'features')">{{ trans('header.property_features') }}</button>
                            @endif
                        </div>
                        <!-- Tab content -->
                        <div id="description" class="tabcontent">
                            @include(themeViewPath('frontend.components.property.description'))
                        </div>
                        @if($property->features->count() > 0)
                            <div id="features" class="tabcontent hidden">
                                @include(themeViewPath('frontend.components.property.features'))
                            </div>
                        @endif
                    </div>
                </div>
                <div class="col-span-5 lg:pl-6">
                    <div class="shadow-md py-9 px-4 lg:px-7 lg:mt-10 mt-8 bg-white" id="enquire">
                        <h3 id="enquire" class="text-2xl leading-normal text-center tracking-tight header-text mb-7">{{ trans('header.enquire_about_this_property') }}</h3>
                        @include(themeViewPath('frontend.forms.request-details'))
                        <div>
                            <span class="block text-center tracking-tight text-base">{{ trans('generic.share_this_property') }}</span>
                            @include(themeViewPath('frontend.components.social.share'), ['url' => route('property.show', [$property->url_key])])
                        </div>

                        <div class="text-center mt-6">

                            @if (hasFeature(TenantFeature::FEATURE_PDF_BEHIND_FORM))
                                <a id="download-pdf" data-target="brochure-request-modal"
                                   class="modal-button text-sm text-center tracking-wide rounded-3xl border max-w-xs inline-block py-3 px-8 cta-border transition-all duration-500"
                                   href="javascript:">
                                    {{ trans('generic.download_pdf') }}
                                </a>
                            @else
                                <a id="download-pdf"
                                   class="text-sm text-center tracking-wide rounded-3xl border max-w-xs inline-block py-3 px-8 cta-border transition-all duration-500"
                                   href="{{ $property->pdf_url }}">{{ trans('generic.download_pdf') }}</a>
                        </div>
                        @endif
                    </div>

                    <div class="mb-8 lg:mb-24">
                        @include(themeViewPath('frontend.components.contact-details'))
                    </div>
                </div>

            </div>

        </div>
    </section>

    @include(themeViewPath('frontend.components.featured-properties'), ['customHeader' => trans('header.similar_properties'), 'properties' => $relatedProperties])
    @include(themeViewPath('frontend.components.newsletter-signup'))

    {{-- site footer --}}
    @include(themeViewPath('frontend.components.footer'))

    {{-- CTA Modals --}}

    @if ($property->video_url !== null)
        @include(themeViewPath('frontend.components.modals.video'))
    @endif
    @if ($property->virtual_tour_url !== null)
        @include(themeViewPath('frontend.components.modals.virtual-tour'))
    @endif
    @if($useMap && $property->location->latitude !== null && $property->location->longitude !== null)
        @include(themeViewPath('frontend.components.modals.location'))
    @endif
    @include(themeViewPath('frontend.components.modals.gallery-overlay'))
    @include(themeViewPath('frontend.components.modals.gallery'))
    @include(themeViewPath('frontend.components.modals.request-details'))
    @include(themeViewPath('frontend.components.modals.book-viewing'))
    @include(themeViewPath('frontend.components.modals.property-note'))
    @include(themeViewPath('frontend.components.modals.mortgage-calculator'))
    @include(themeViewPath('frontend.components.modals.stamp-duty-calculator'))
    @include(themeViewPath('frontend.components.modals.property-note'))
    @include(themeViewPath('frontend.components.modals.share-this-property'))

    @if (hasFeature(TenantFeature::FEATURE_PDF_BEHIND_FORM))
        @include(themeViewPath('frontend.components.modals.brochure-request'))
    @endif

    @if (!hasFeature(TenantFeature::FEATURE_DOCUMENTS_BY_TYPE))
        @if ($property->documents->count() > 0)
            @include(themeViewPath('frontend.components.modals.documents'))
        @endif
    @else
        @if ($floorplanDocs->count())
            @include(themeViewPath('frontend.components.modals.documents-by-type'), ['id' => 'property-floorplan-documents-modal', 'documents' => $floorplanDocs])
        @endif

        @if ($epcDocs->count())
            @include(themeViewPath('frontend.components.modals.documents-by-type'), ['id' => 'property-epc-documents-modal', 'documents' => $epcDocs])
        @endif

        @if ($brochureDocs->count())
            @include(themeViewPath('frontend.components.modals.documents-by-type'), ['id' => 'property-brochure-documents-modal', 'documents' => $brochureDocs])
        @endif

        @if ($otherDocs->count())
            @include(themeViewPath('frontend.components.modals.documents-by-type'), ['id' => 'property-other-documents-modal', 'documents' => $otherDocs])
        @endif
    @endif
@endsection
