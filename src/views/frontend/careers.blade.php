@extends('layouts.app')

@section('content')

    {{-- site header component, includes navigation --}}
    @include(themeViewPath('frontend.components.header'))

    <section id="careers" class="pt-4">
        <div class="container pb-12 px-8 xl:px-0 mx-auto">

            @include('frontend.components.system-notifications', ['customClass' => 'mb-6'])

            <div>
                <h2 class="text-2xl md:text-3xl pb-2 font-medium py-6 header-text">{!! trans('career.careers') !!}</h2>
            </div>
            <hr class="mb-4">

            <div class="text-sm mb-4">
                @include(themeViewPath('frontend.components.page-breadcrumbs'), ['navigation' => [
                   [trans('header.home') => localeUrl('/')],
                   [trans('career.careers') => null],
                ]])
            </div>

            <div class="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 gap-8">
                @foreach($careers as $career)
                    @include(themeViewPath('frontend.components.cards.career'), ['career' => $career, 'data' => $service->findByVacancyUuidAndLocale($career->uuid, app()->getLocale())])
                @endforeach
            </div>

        </div>

        @include(themeViewPath('frontend.components.listings.listings-pagination'), ['data' => $careers, 'extraClass' => 'mb-12'])

    </section>

    <!-- Newsletter Signup Form -->
    @include(themeViewPath('frontend.components.newsletter-signup'))

    {{-- site footer --}}
    @include(themeViewPath('frontend.components.footer'))

@endsection
