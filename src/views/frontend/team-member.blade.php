@extends('layouts.app')

@section('content')

    @php
        $searchControls = searchControlOptions(config('platform-cache.search_controls_enabled'))
    @endphp

    {{-- site header component, includes navigation --}}
    @include(themeViewPath('frontend.components.header'), ['transparentNavigation' => false,])

    <!-- Breadcrumb Area -->
    <section class="bg-white">
        <div class="container px-4 mx-auto ">

            <div class="flex justify-between w-full border-b py-5 md:flex-row flex-col">
                <!--  Breadcrumb -->
                <nav class="bg-grey-light ">
                    <ol class="flex items-center gap-x-2">
                        <li><a href="{{ localeUrl('/') }}" class="active-color text-sm font-light hover:opacity-75 duration-300 primary-text">Home</a></li>
                        <li><span class="primary-text text-sm font-light">&lt;</span></li>
                        <li><a  class="font-light text-sm primary-text" href="{{ localeUrl('/meet-the-team') }}">Meet the Team</a> </li>
                        <li><span class="text-sm font-light text-browngrey">&lt;</span></li>
                        <li class="text-browngrey font-light text-sm">{{ $teamMember->name }}</li>
                    </ol>

                </nav>
            </div>
        </div>
    </section>
    <!-- Content -->

    <section class="py-16">
        <div class="container px-4 mx-auto">

            <!-- Detail of a member -->
            <div class="flex items-start flex-wrap">
                @if ($teamMember->photo)
                    <div class="xl:w-1/4 md:w-1/3 w-full"><img class="h-80 sm:h-100 md:h-80 lg:h-100 w-auto object-cover object-center w-full" src="{{ assetPath($teamMember->photo) }}" alt="img"></div>
                @endif

                <div class="w-full xl:w-3/4 xl:pl-11 md:pl-8 md:w-2/3 mt-6 xl:mt-0">
                    <p class="text-4xl header-text xl:mb-3 mb-2 ">{{ $teamMember->name }}</p>
                    <p class="text-sm text-gray-500 leading-loose tracking-wide uppercase xl:mb-9 mb-4">{{ $teamMember->role }}</p>
                    <p class="text-sm leading-normal tracking-tight font-light">{!! nl2br($teamMember->description) !!}</p>
                    <div class="flex xl:mt-11 mt-6 xl:pt-11 pt-6 border-t team-member-contact-details">
                        @if ($teamMember->email)
                            <p class="text-sm tracking-tight primary-text">
                                {{ $teamMember->email }}
                            </p>
                        @endif
                        
                        @if ($teamMember->email && $teamMember->tel)
                            <p class="text-sm tracking-tight primary-text mx-4"> | </p>
                        @endif

                        @if ($teamMember->tel)
                            <p class="text-sm tracking-tight primary-text">
                                {{ $teamMember->tel }}
                            </p>
                        @endif
                    </div>
                </div>
            </div>



        </div>
    </section>

    <div class="bg-gray-100">
        @include(themeViewPath('frontend.components.featured-properties'), ['customHeader' => trans('header.recent_properties'), 'properties' => $recentProperties, 'bgColor' => 'bg-transparent'])
    </div>

    <!-- Joseph’s Listings -->
{{--    <div class="bg-whiter pt-16 pb-24">--}}
{{--        <div class="container px-4 mx-auto relative">--}}
{{--            <h3 class="text-4xl font-freight leading-loose text-center font-book primary-text mb-9">Joseph’s Listings--}}
{{--            </h3>--}}
{{--            <div class="s-slider swiper mySwiperS overflow-hidden md:p-0 z-1 list-none">--}}
{{--                <div class="swiper-wrapper flex z-1 relative w-full transition-transform box-content ">--}}
{{--                    <!-- Single -->--}}
{{--                    <div class="swiper-slide flex-shrink-0 relative list-single">--}}

{{--                        <div--}}
{{--                            class="s-slider swiper mySwiperPropertiesInner overflow-hidden md:p-0 z-1 list-none relative z-10 max-w-5xl">--}}
{{--                            <div class="swiper-wrapper flex z-1 relative w-full transition-transform box-content ">--}}
{{--                                <div class="swiper-slide flex-shrink-0 relative">--}}
{{--                                    <img class="w-full" src="assets/img/team/img-1.jpg" alt="img">--}}
{{--                                    <div--}}
{{--                                        class="bg-opacity-40 bg-black absolute top-0 left-0 w-full h-full z-0  duration-500 transition-all">--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="swiper-slide flex-shrink-0 relative">--}}
{{--                                    <img class="w-full" src="assets/img/team/img-2.jpg" alt="img">--}}
{{--                                    <div--}}
{{--                                        class="bg-opacity-20 bg-black absolute top-0 left-0 w-full h-full z-0  duration-500 transition-all">--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="swiper-slide flex-shrink-0 relative">--}}
{{--                                    <img class="w-full" src="assets/img/team/img-1.jpg" alt="img">--}}
{{--                                    <div--}}
{{--                                        class="bg-opacity-40 bg-black absolute top-0 left-0 w-full h-full z-0  duration-500 transition-all">--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="swiper-slide flex-shrink-0 relative">--}}
{{--                                    <img class="w-full" src="assets/img/team/img-2.jpg" alt="img">--}}
{{--                                    <div--}}
{{--                                        class="bg-opacity-20 bg-black absolute top-0 left-0 w-full h-full z-0  duration-500 transition-all">--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="cursor-pointer flex absolute right-2 top-2">--}}
{{--                                <img class="swiper-button-nextInner" src="assets/img/team/arrow-left.svg" alt="heart">--}}
{{--                                <img class="swiper-button-prevInner" src="assets/img/team/arrow-right.svg" alt="heart">--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="absolute bottom-8 left-0 xl:px-8 px-4 w-full z-10  transition-all">--}}
{{--                            <h3 class="text-2xl font-book text-white">3 Bedroom Property Title</h3>--}}
{{--                            <span class="text-sm  tracking-tight text-white">Area, This location</span>--}}
{{--                            <p class="text-xl tracking-tight font-medium text-white uppercase md:mt-7 mt-2 mb-4">--}}
{{--                                £324,000</p>--}}
{{--                            <div class="flex justify-between items-center transition-all relative">--}}
{{--                                <div class="flex items-center">--}}
{{--                                    <div class="flex mr-4">--}}
{{--                                        <img src="assets/img/team/bed.svg" alt="bed">--}}
{{--                                        <span class="ml-2 text-sm tracking-tight text-white">3 Bed</span>--}}
{{--                                    </div>--}}
{{--                                    <div class="flex mr-4">--}}
{{--                                        <img src="assets/img/team/bath.svg" alt="bath">--}}
{{--                                        <span class="ml-2 text-sm tracking-tight text-white">2 Bath</span>--}}
{{--                                    </div>--}}
{{--                                    <div class="flex">--}}
{{--                                        <img src="assets/img/team/resize-both.svg" alt="bed">--}}
{{--                                        <span class="ml-2 text-sm tracking-tight text-white">1234 sq ft</span>--}}
{{--                                    </div>--}}
{{--                                </div>--}}

{{--                            </div>--}}

{{--                        </div>--}}
{{--                        <div class="flex items-center absolute top-4 right-16 z-30">--}}
{{--                            <div class="cursor-pointer mr-2 -mt-1">--}}
{{--                                <img src="assets/img/team/heart-white.svg" alt="heart">--}}
{{--                            </div>--}}

{{--                        </div>--}}

{{--                    </div>--}}
{{--                    <!-- Single -->--}}
{{--                    <div class="swiper-slide flex-shrink-0 relative list-single">--}}

{{--                        <div--}}
{{--                            class="s-slider swiper mySwiperPropertiesInner overflow-hidden md:p-0 z-1 list-none relative z-10 max-w-5xl">--}}
{{--                            <div class="swiper-wrapper flex z-1 relative w-full transition-transform box-content ">--}}

{{--                                <div class="swiper-slide flex-shrink-0 relative">--}}
{{--                                    <img class="w-full" src="assets/img/team/img-2.jpg" alt="img">--}}
{{--                                    <div--}}
{{--                                        class="bg-opacity-20 bg-black absolute top-0 left-0 w-full h-full z-0  duration-500 transition-all">--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="swiper-slide flex-shrink-0 relative">--}}
{{--                                    <img class="w-full" src="assets/img/team/img-1.jpg" alt="img">--}}
{{--                                    <div--}}
{{--                                        class="bg-opacity-40 bg-black absolute top-0 left-0 w-full h-full z-0  duration-500 transition-all">--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="swiper-slide flex-shrink-0 relative">--}}
{{--                                    <img class="w-full" src="assets/img/team/img-2.jpg" alt="img">--}}
{{--                                    <div--}}
{{--                                        class="bg-opacity-20 bg-black absolute top-0 left-0 w-full h-full z-0  duration-500 transition-all">--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="swiper-slide flex-shrink-0 relative">--}}
{{--                                    <img class="w-full" src="assets/img/team/img-1.jpg" alt="img">--}}
{{--                                    <div--}}
{{--                                        class="bg-opacity-40 bg-black absolute top-0 left-0 w-full h-full z-0  duration-500 transition-all">--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="cursor-pointer flex absolute right-2 top-2">--}}
{{--                                <img class="swiper-button-nextInner" src="assets/img/team/arrow-left.svg" alt="heart">--}}
{{--                                <img class="swiper-button-prevInner" src="assets/img/team/arrow-right.svg" alt="heart">--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="absolute bottom-8 left-0 xl:px-8 px-4 w-full z-10  transition-all">--}}
{{--                            <h3 class="text-2xl font-book text-white">3 Bedroom Property Title</h3>--}}
{{--                            <span class="text-sm  tracking-tight text-white">Area, This location</span>--}}
{{--                            <p class="text-xl tracking-tight font-medium text-white uppercase md:mt-7 mt-2 mb-4">--}}
{{--                                £324,000</p>--}}
{{--                            <div class="flex justify-between items-center transition-all relative">--}}
{{--                                <div class="flex items-center">--}}
{{--                                    <div class="flex mr-4">--}}
{{--                                        <img src="assets/img/team/bed.svg" alt="bed">--}}
{{--                                        <span class="ml-2 text-sm tracking-tight text-white">3 Bed</span>--}}
{{--                                    </div>--}}
{{--                                    <div class="flex mr-4">--}}
{{--                                        <img src="assets/img/team/bath.svg" alt="bath">--}}
{{--                                        <span class="ml-2 text-sm tracking-tight text-white">2 Bath</span>--}}
{{--                                    </div>--}}
{{--                                    <div class="flex">--}}
{{--                                        <img src="assets/img/team/resize-both.svg" alt="bed">--}}
{{--                                        <span class="ml-2 text-sm tracking-tight text-white">1234 sq ft</span>--}}
{{--                                    </div>--}}
{{--                                </div>--}}

{{--                            </div>--}}

{{--                        </div>--}}
{{--                        <div class="flex items-center absolute top-4 right-16 z-30">--}}
{{--                            <div class="cursor-pointer mr-2 -mt-1">--}}
{{--                                <img src="assets/img/team/heart-white.svg" alt="heart">--}}
{{--                            </div>--}}

{{--                        </div>--}}

{{--                    </div>--}}
{{--                    <!-- Single -->--}}
{{--                    <div class="swiper-slide flex-shrink-0 relative list-single">--}}

{{--                        <div--}}
{{--                            class="s-slider swiper mySwiperPropertiesInner overflow-hidden md:p-0 z-1 list-none relative z-10 max-w-5xl">--}}
{{--                            <div class="swiper-wrapper flex z-1 relative w-full transition-transform box-content ">--}}
{{--                                <div class="swiper-slide flex-shrink-0 relative">--}}
{{--                                    <img class="w-full" src="assets/img/team/img-1.jpg" alt="img">--}}
{{--                                    <div--}}
{{--                                        class="bg-opacity-40 bg-black absolute top-0 left-0 w-full h-full z-0  duration-500 transition-all">--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="swiper-slide flex-shrink-0 relative">--}}
{{--                                    <img class="w-full" src="assets/img/team/img-2.jpg" alt="img">--}}
{{--                                    <div--}}
{{--                                        class="bg-opacity-20 bg-black absolute top-0 left-0 w-full h-full z-0  duration-500 transition-all">--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="swiper-slide flex-shrink-0 relative">--}}
{{--                                    <img class="w-full" src="assets/img/team/img-1.jpg" alt="img">--}}
{{--                                    <div--}}
{{--                                        class="bg-opacity-40 bg-black absolute top-0 left-0 w-full h-full z-0  duration-500 transition-all">--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="swiper-slide flex-shrink-0 relative">--}}
{{--                                    <img class="w-full" src="assets/img/team/img-2.jpg" alt="img">--}}
{{--                                    <div--}}
{{--                                        class="bg-opacity-20 bg-black absolute top-0 left-0 w-full h-full z-0  duration-500 transition-all">--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="cursor-pointer flex absolute right-2 top-2">--}}
{{--                                <img class="swiper-button-nextInner" src="assets/img/team/arrow-left.svg" alt="heart">--}}
{{--                                <img class="swiper-button-prevInner" src="assets/img/team/arrow-right.svg" alt="heart">--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="absolute bottom-8 left-0 xl:px-8 px-4 w-full z-10  transition-all">--}}
{{--                            <h3 class="text-2xl font-book text-white">3 Bedroom Property Title</h3>--}}
{{--                            <span class="text-sm  tracking-tight text-white">Area, This location</span>--}}
{{--                            <p class="text-xl tracking-tight font-medium text-white uppercase md:mt-7 mt-2 mb-4">--}}
{{--                                £324,000</p>--}}
{{--                            <div class="flex justify-between items-center transition-all relative">--}}
{{--                                <div class="flex items-center">--}}
{{--                                    <div class="flex mr-4">--}}
{{--                                        <img src="assets/img/team/bed.svg" alt="bed">--}}
{{--                                        <span class="ml-2 text-sm tracking-tight text-white">3 Bed</span>--}}
{{--                                    </div>--}}
{{--                                    <div class="flex mr-4">--}}
{{--                                        <img src="assets/img/team/bath.svg" alt="bath">--}}
{{--                                        <span class="ml-2 text-sm tracking-tight text-white">2 Bath</span>--}}
{{--                                    </div>--}}
{{--                                    <div class="flex">--}}
{{--                                        <img src="assets/img/team/resize-both.svg" alt="bed">--}}
{{--                                        <span class="ml-2 text-sm tracking-tight text-white">1234 sq ft</span>--}}
{{--                                    </div>--}}
{{--                                </div>--}}

{{--                            </div>--}}

{{--                        </div>--}}
{{--                        <div class="flex items-center absolute top-4 right-16 z-30">--}}
{{--                            <div class="cursor-pointer mr-2 -mt-1">--}}
{{--                                <img src="assets/img/team/heart-white.svg" alt="heart">--}}
{{--                            </div>--}}

{{--                        </div>--}}

{{--                    </div>--}}
{{--                    <!-- Single -->--}}
{{--                    <div class="swiper-slide flex-shrink-0 relative list-single">--}}

{{--                        <div--}}
{{--                            class="s-slider swiper mySwiperPropertiesInner overflow-hidden md:p-0 z-1 list-none relative z-10 max-w-5xl">--}}
{{--                            <div class="swiper-wrapper flex z-1 relative w-full transition-transform box-content ">--}}

{{--                                <div class="swiper-slide flex-shrink-0 relative">--}}
{{--                                    <img class="w-full" src="assets/img/team/img-2.jpg" alt="img">--}}
{{--                                    <div--}}
{{--                                        class="bg-opacity-20 bg-black absolute top-0 left-0 w-full h-full z-0  duration-500 transition-all">--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="swiper-slide flex-shrink-0 relative">--}}
{{--                                    <img class="w-full" src="assets/img/team/img-1.jpg" alt="img">--}}
{{--                                    <div--}}
{{--                                        class="bg-opacity-40 bg-black absolute top-0 left-0 w-full h-full z-0  duration-500 transition-all">--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="swiper-slide flex-shrink-0 relative">--}}
{{--                                    <img class="w-full" src="assets/img/team/img-2.jpg" alt="img">--}}
{{--                                    <div--}}
{{--                                        class="bg-opacity-20 bg-black absolute top-0 left-0 w-full h-full z-0  duration-500 transition-all">--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="swiper-slide flex-shrink-0 relative">--}}
{{--                                    <img class="w-full" src="assets/img/team/img-1.jpg" alt="img">--}}
{{--                                    <div--}}
{{--                                        class="bg-opacity-40 bg-black absolute top-0 left-0 w-full h-full z-0  duration-500 transition-all">--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="cursor-pointer flex absolute right-2 top-2">--}}
{{--                                <img class="swiper-button-nextInner" src="assets/img/team/arrow-left.svg" alt="heart">--}}
{{--                                <img class="swiper-button-prevInner" src="assets/img/team/arrow-right.svg" alt="heart">--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="absolute bottom-8 left-0 xl:px-8 px-4 w-full z-10  transition-all">--}}
{{--                            <h3 class="text-2xl font-book text-white">3 Bedroom Property Title</h3>--}}
{{--                            <span class="text-sm  tracking-tight text-white">Area, This location</span>--}}
{{--                            <p class="text-xl tracking-tight font-medium text-white uppercase md:mt-7 mt-2 mb-4">--}}
{{--                                £324,000</p>--}}
{{--                            <div class="flex justify-between items-center transition-all relative">--}}
{{--                                <div class="flex items-center">--}}
{{--                                    <div class="flex mr-4">--}}
{{--                                        <img src="assets/img/team/bed.svg" alt="bed">--}}
{{--                                        <span class="ml-2 text-sm tracking-tight text-white">3 Bed</span>--}}
{{--                                    </div>--}}
{{--                                    <div class="flex mr-4">--}}
{{--                                        <img src="assets/img/team/bath.svg" alt="bath">--}}
{{--                                        <span class="ml-2 text-sm tracking-tight text-white">2 Bath</span>--}}
{{--                                    </div>--}}
{{--                                    <div class="flex">--}}
{{--                                        <img src="assets/img/team/resize-both.svg" alt="bed">--}}
{{--                                        <span class="ml-2 text-sm tracking-tight text-white">1234 sq ft</span>--}}
{{--                                    </div>--}}
{{--                                </div>--}}

{{--                            </div>--}}

{{--                        </div>--}}
{{--                        <div class="flex items-center absolute top-4 right-16 z-30">--}}
{{--                            <div class="cursor-pointer mr-2 -mt-1">--}}
{{--                                <img src="assets/img/team/heart-white.svg" alt="heart">--}}
{{--                            </div>--}}

{{--                        </div>--}}

{{--                    </div>--}}
{{--                </div>--}}

{{--            </div>--}}
{{--            <div class="swiper-button-next absolute right-12 top-5 cursor-pointer">--}}
{{--                <img src="assets/img/team/sarrow-left.svg" alt="arrow">--}}
{{--            </div>--}}
{{--            <div class="swiper-button-prev absolute top-5 right-4 cursor-pointer">--}}
{{--                <img src="assets/img/team/sarrow-right.svg" alt="arrow">--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}


    @include(themeViewPath('frontend.components.testimonials'))

    <!-- Newsletter Signup Form -->
    @include(themeViewPath('frontend.components.newsletter-signup'))

    {{-- site footer --}}
    @include(themeViewPath('frontend.components.footer'))

@endsection
