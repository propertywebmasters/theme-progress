@extends('layouts.app')

@section('content')

    {{-- site header component, includes navigation --}}
    @include(themeViewPath('frontend.components.header'))

    <!-- Banner -->
    <div class="md:min-h-158 py-16 md:py-0 relative pt-8 bg-cover bg-center flex items-center center-cover-bg bg-lazy-load" data-style="{{ backgroundCSSImage('team.hero') }}">
        <div class="absolute left-0 top-0 w-full h-full bg-black opacity-70"></div>
        <div class="relative z-10 container mx-auto px-4 mt-9">
            <h3 class="text-4xl md:text-5xl lg:text-6xl header-text text-white text-center md:text-left pt-32 pb-3 md:py-0 md:mb-4">
                {{ trans('header.meet_the_team') }}
            </h3>

            @php
                $breadcrumb['/'] = trans('header.home');
                $breadcrumb['javascript:;'] = trans('header.meet_the_team')
            @endphp

            <div class="generic-breadcrumb text-center md:text-left pb-32 md:pb-0">
                @php $i = 1 @endphp
                @foreach($breadcrumb as $url => $anchor)
                    <a class="cta-bg-text-only" href="{{ $url }}">{!! $anchor !!}</a>
                    @if ($i < count($breadcrumb)) <span class="text-white">&gt;</span> @endif
                    @php $i++ @endphp
                @endforeach
            </div>
        </div>
    </div>

    <section id="team-members">
        <div class="container py-12 px-8 xl:px-0 mx-auto">
            {{-- <div class="flex items-center justify-end py-4">
                @if($teamMembers->links())
                    @include(themeViewPath('frontend.components.team-members.team-members-pagination'), ['data' => $teamMembers, 'extraClass' => 'hidden lg:block'])
                @endif
            </div> --}}

            <div class="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-4 gap-8">
                @foreach ($teamMembers as $teamMember)
                    @include(themeViewPath('frontend.components.cards.team-member'), ['teamMember' => $teamMember])
                @endforeach
            </div>

            {{-- @if($teamMembers->links())
                <div class="flex items-center justify-between mt-12 mb-7 md:mb-0">
                    <div></div>

                    <div>
                        <a class="text-sm text-center tracking-wide rounded-full primary-border max-w-xs block transition-all duration-500 bg-transparent primary-text py-4 px-16" href="{{ $teamMembers->nextPageUrl() }}">
                            {{ trans('team.next_page') }}
                        </a>
                    </div>

                    @include(themeViewPath('frontend.components.team-members.team-members-pagination'), ['data' => $teamMembers, 'extraClass' => 'hidden lg:block'])
                </div>
            @endif --}}
        </div>
    </section>

    @if(hasFeature(\App\Models\TenantFeature::FEATURE_MAILING_LIST))
        @include(themeViewPath('frontend.components.newsletter-signup'))
    @endif

    {{-- site footer --}}
    @include(themeViewPath('frontend.components.footer'))

@endsection
