@extends('layouts.app')

@section('content')

    {{-- site header component, includes navigation --}}
    @include(themeViewPath('frontend.components.header'))

    <section class="py-20">
        <div class="container px-4 mx-auto">
            <div class="flex flex-wrap">
                <div class="w-full text-center">
                    @include(themeViewPath('frontend.components.system-notifications'), ['customClass' => 'inline-block mb-6 mt-0'])

                    <h1 class="text-6xl text-center mb-4">{{ trans('generic.thank_you') }}</h1>
                    <div class="text-center">
                        <a class="text-sm text-center tracking-wide rounded-3xl primary-border max-w-xs inline-block bg-transparent primary-text py-3 px-6" href="{{ url()->previous() }}">{{ trans('button.go_back') }}</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    {{-- site footer --}}
    @include(themeViewPath('frontend.components.footer'))

@endsection
