<div class="px-6 py-10 text-white h-full overflow-y-scroll" style="background-color: #333;">
    <h3 class="capitalize-first-letter font-bold mb-6 text-xl">{{ trans('header.arrange_a_viewing') }}</h3>

    <form action="{{ localeUrl('/property/'.$property->url_key.'/viewing') }}"  method="post" enctype="application/x-www-form-urlencoded" class="recaptcha">

        <div class="pb-6">
            <input id="viewing_date" name="date" type="text" class="p-2 pl-0 border-b border-white border-solid block w-full text-sm bg-transparent text-white placeholder-white calendar-icon-white" placeholder="{{ trans('label.choose_a_date') }}" required onfocus="(this.type='date')">
        </div>

        <div class="pb-6">
            <label for="viewing_slots" class="block pb-2 text-sm">{{ trans('label.please_select_upto_3_slots') }} *</label>

            <div class="grid grid-cols-4 gap-2">
                {{-- @todo Replace this with a foreach dateInterval --}}
                <div class="viewing-time-slot"><span class="rounded block w-full bg-gray-500 text-white text-center p-2 text-sm cursor-pointer">09:00</span></div>
                <div class="viewing-time-slot"><span class="rounded block w-full bg-gray-500 text-white text-center p-2 text-sm cursor-pointer">09:30</span></div>
                <div class="viewing-time-slot"><span class="rounded block w-full bg-gray-500 text-white text-center p-2 text-sm cursor-pointer">10:00</span></div>
                <div class="viewing-time-slot"><span class="rounded block w-full bg-gray-500 text-white text-center p-2 text-sm cursor-pointer">10:30</span></div>
                <div class="viewing-time-slot"><span class="rounded block w-full bg-gray-500 text-white text-center p-2 text-sm cursor-pointer">11:00</span></div>
                <div class="viewing-time-slot"><span class="rounded block w-full bg-gray-500 text-white text-center p-2 text-sm cursor-pointer">11:30</span></div>
                <div class="viewing-time-slot"><span class="rounded block w-full bg-gray-500 text-white text-center p-2 text-sm cursor-pointer">12:00</span></div>
                <div class="viewing-time-slot"><span class="rounded block w-full bg-gray-500 text-white text-center p-2 text-sm cursor-pointer">12:30</span></div>
                <div class="viewing-time-slot"><span class="rounded block w-full bg-gray-500 text-white text-center p-2 text-sm cursor-pointer">13:00</span></div>
                <div class="viewing-time-slot"><span class="rounded block w-full bg-gray-500 text-white text-center p-2 text-sm cursor-pointer">13:30</span></div>
                <div class="viewing-time-slot"><span class="rounded block w-full bg-gray-500 text-white text-center p-2 text-sm cursor-pointer">14:00</span></div>
                <div class="viewing-time-slot"><span class="rounded block w-full bg-gray-500 text-white text-center p-2 text-sm cursor-pointer">14:30</span></div>
                <div class="viewing-time-slot"><span class="rounded block w-full bg-gray-500 text-white text-center p-2 text-sm cursor-pointer">15:00</span></div>
                <div class="viewing-time-slot"><span class="rounded block w-full bg-gray-500 text-white text-center p-2 text-sm cursor-pointer">15:30</span></div>
                <div class="viewing-time-slot"><span class="rounded block w-full bg-gray-500 text-white text-center p-2 text-sm cursor-pointer">16:00</span></div>
                <div class="viewing-time-slot"><span class="rounded block w-full bg-gray-500 text-white text-center p-2 text-sm cursor-pointer">16:30</span></div>
            </div>

            {{-- javascript builds up the value for this to be submitted --}}
            <input type="hidden" name="slots" value="">

        </div>

        <div class="">
            <input type="text" name="name" placeholder="{{ trans('contact.full_name') }}" class="block text-white w-full p-2 pl-0 border-b border-white text-sm mb-6 bg-transparent placeholder-white" required>
            <input type="email" name="email" placeholder="{{ trans('contact.email_address') }}" class="block text-white w-full p-2 pl-0 border-b border-white text-sm mb-6 bg-transparent placeholder-white" required>
            <input type="text" name="tel" placeholder="{{ trans('contact.telephone_number') }}" class="block text-white w-full p-2 pl-0 border-b border-white text-sm mb-6 bg-transparent placeholder-white" required>
            <textarea name="comment" placeholder="{{ trans('placeholder.further_details') }}" class="block text-white w-full p-2 pl-0 border-b border-white text-sm mb-6 bg-transparent placeholder-white"></textarea>

            @include(themeViewPath('frontend.forms.recaptcha.recaptcha-fallback-inputs'), ['inputClass' => 'block w-full p-2 pl-0 border-b border-white text-sm mb-6 bg-transparent placeholder-white'])

            <p class="text-left text-xs pb-6 text-white modal-terms opacity-50">{!! trans('terms.modal_terms')  !!}</p>
        </div>

        <div class="">
            <button type="submit" role="button"
                    class="primary-bg inline-block text-center p-4 text-sm leading-normal tracking-wide text-white cursor-pointer capitalize rounded-full px-6">
                {{ trans('button.request_viewing') }}
            </button>
        </div>
        @csrf
    </form>
</div>
