@php
    $class = 'rounded-sm bg-white h-14 w-full px-4 mb-3 font-light border';
    $fallbackQuestions = getRecaptchaFallbackQuestionAnswerSet();
    $randomQuestion = array_rand($fallbackQuestions, 1)
@endphp
<form class="recaptcha" action="{{ localeUrl('/careers/'.$career->url_key) }}" method="post" enctype="multipart/form-data">
    <input type="text" name="name" placeholder="{{ trans('contact.full_name') }}" class="{{ $class }}" value="{{ optional(user())->name() }}" required>
    <input type="email" name="email" placeholder="{{ trans('contact.email_address') }}" class="{{ $class }}" value="{{ optional(user())->email }}" required>
    <input type="text" name="tel" placeholder="{{ trans('contact.telephone_number') }}" value="{{ optional(user())->tel }}" class="{{ $class }}" required>

    <label class="block mb-2 font-medium text-gray-900 dark:text-gray-300" for="file_input">{{ trans('career.upload_cv') }}</label>
    <input class="bg-transparent text-black block w-full py-2 mb-8 focus:outline-none" type="file" name="cv">

    @include(themeViewPath('frontend.forms.recaptcha.recaptcha-fallback-inputs'), ['inputClass' => $class])

    <p class="text-xs text-center modal-terms">{!! trans('terms.modal_terms')  !!}</p>

    <div class="text-center mx-auto mt-4">
        <input type="submit" class="text-sm text-center tracking-wide rounded-3xl border max-w-xs inline-block py-3 px-8 transition-all hover:text-white duration-500 cta cta-text" value="{{ trans('career.send_details') }}">
    </div>
    @csrf
</form>

