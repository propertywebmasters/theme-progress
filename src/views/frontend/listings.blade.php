@extends('layouts.app')

@section('content')

    {{-- site header component, includes navigation --}}
    @include(themeViewPath('frontend.components.header'), ['transparentNavigation' => false,])

    @php $chunks = $properties->chunk(12) @endphp

    @include(themeViewPath('frontend.components.search-filter'))
    @include(themeViewPath('frontend.components.search-filter-breadcrumb'))

    @php
    $cardViewFile = request()->get('list') == true ? 'property-list' : 'property';
    $gridCss = request()->get('list') == true ? 'grid grid-cols-1 gap-4' : 'grid md:grid-cols-2 lg:grid-cols-3 grid-cols-1 lg:gap-9 gap-4'
    @endphp

    @if($properties->count() > 0)
        <section class="mb-6">
            <div class="container mx-auto px-4">
                <div class="{{ $gridCss }}">
                    @foreach($chunks[0] as $i => $property)
                        @php $cardViewFile = $property->is_development ? 'development' : $cardViewFile @endphp
                        @include(themeViewPath('frontend.components.cards.'.$cardViewFile), ['property' => $property])

                        @if (!isset($chunks[1]) && hasFeature(\App\Models\TenantFeature::FEATURE_USE_CARD_LISTINGS_DIVIDER) && $i === (array_key_last($chunks[0]->toArray()) - 1))
                            @include(themeViewPath('frontend.components.cards.listings-divider'), ['list' => request()->get('list')])
                        @endif
                    @endforeach
                </div>
            </div>
        </section>
    @else
        <div class="bg-gray-100 text-center p-12">
            <span class="text-2xl block font-medium pb-4 pb-4 mb-4">{{ trans('search.no_properties_found') }}</span>
            <a class="primary-bg hover-lighten text-white font-bold py-4 md:mt-0 px-8 text-sm uppercase cursor-pointer block lg:inline-block rounded-full"
            href="{{ url()->current() }}#listings-search">{{ trans('search.no_results_button') }}</a>
        </div>
    @endif

    @if(isset($chunks[0], $chunks[1]) && !hasFeature(\App\Models\TenantFeature::FEATURE_USE_CARD_LISTINGS_DIVIDER))
        @include(themeViewPath('frontend.components.listings.listings-divider'))
    @endif

    @if(isset($chunks[1]))
        <section class="mb-6 mt-16">
            <div class="container mx-auto px-4">
                <div class="{{ $gridCss }}">
                    @foreach($chunks[1] as $i => $property)
                        @php $cardViewFile = $property->is_development ? 'development' : $cardViewFile @endphp
                        @include(themeViewPath('frontend.components.cards.'.$cardViewFile), ['property' => $property])

                        @if (hasFeature(\App\Models\TenantFeature::FEATURE_USE_CARD_LISTINGS_DIVIDER) && $i === array_key_first($chunks[1]->toArray()))
                            @include(themeViewPath('frontend.components.cards.listings-divider'), ['list' => request()->get('list')])
                        @endif
                    @endforeach
                </div>
            </div>
        </section>
    @endif

    @include(themeViewPath('frontend.components.listings.listings-pagination'))

    @include(themeViewPath('frontend.components.search-page-content'))

    @include(themeViewPath('frontend.components.newsletter-signup'))

    {{-- site footer --}}
    @include(themeViewPath('frontend.components.footer'))

    {{-- CTA Models --}}
    @include(themeViewPath('frontend.components.modals.create-alert'))
    @include(themeViewPath('frontend.components.modals.share-this-search'))

@endsection
