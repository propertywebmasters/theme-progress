@extends('layouts.app')

@section('content')

    {{-- site header component, includes navigation --}}
    @include(themeViewPath('frontend.components.header'))

    @include(themeViewPath('frontend.components.map-not-available'))

    <div class="hidden lg:block">
        <div class="px-8 xl:px-0 mx-auto">
            @include(themeViewPath('frontend.components.search-filter'))
            @include(themeViewPath('frontend.components.search-filter-breadcrumb'))
            @include(themeViewPath('frontend.components.system-notifications'), ['customClass' => 'mb-6'])
        </div>

        @php
            use App\Models\SiteSetting;
            use App\Services\SiteSetting\Contracts\SiteSettingServiceInterface;
            $siteSettingService = app()->make(SiteSettingServiceInterface::class);
            $polygonService = app()->make(\App\Services\LocationPolygon\Contracts\LocationPolygonServiceInterface::class);
            $defaultLatitude = $siteSettingService->retrieve(SiteSetting::SETTING_DEFAULT_MAP_LATITUDE['key'], config('platform-cache.site_settings_enabled'));
            $defaultLongitude = $siteSettingService->retrieve(SiteSetting::SETTING_DEFAULT_MAP_LONGITUDE['key'], config('platform-cache.site_settings_enabled'));
            $defaultZoom = $siteSettingService->retrieve(SiteSetting::SETTING_DEFAULT_MAP_ZOOM['key'], config('platform-cache.site_settings_enabled'));
            if ($polygonData !== null) {
                $polygonDataJson = $polygonService->convertPolygonDataStructure($polygonData);
            }
        @endphp

        <div class="px-4">
            <div class="grid grid-cols-5 gap-4">
                <div class="col-span-3 relative">
                    <div id="map" class="w-full h-screen bg-gray-100 mx-auto text-center" data-zoom="{{ $defaultZoom }}" data-latitude="{{ $defaultLatitude }}" data-longitude="{{ $defaultLongitude }}" @if(isset($polygonDataJson)) data-polygon='{!! json_encode($polygonDataJson) !!}' @endif>
                        <span class="block pt-64 text-3xl">Loading Map</span>
                    </div>
                </div>
                <div class="col-span-2 relative">
                    <div class="h-screen overflow-y-hidden overflow-y-scroll">
                        <div class="grid grid-cols-2 gap-2">
                            @foreach($properties as $property)
                                @include(themeViewPath('frontend.components.cards.property'), ['property' => $property, 'splitMapMode' => true,])
                            @endforeach
                        </div>

                        <div class="my-4">
                            {{ $properties->appends(request()->query())->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>

    @include(themeViewPath('frontend.components.search-page-content'))

    {{-- site footer --}}
    @include(themeViewPath('frontend.components.footer'))

@endsection
