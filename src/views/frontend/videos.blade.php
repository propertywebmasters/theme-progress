@extends('layouts.app')

@section('content')

    {{-- site header component, includes navigation --}}
    @include(themeViewPath('frontend.components.header'))

    <section id="video-archive" class="pt-7">
        <div class="container px-8 xl:px-0 mx-auto">

            @include('frontend.components.system-notifications', ['customClass' => 'mb-6'])

            <div>
                <h2 class="text-2xl md:text-4xl pb-2 py-6 header-text">{{ trans('header.latest_videos') }}</h2>
            </div>
            <hr class="mb-4">

            <div class="text-sm mb-4">
                <a href="/" class="primary-text">{{ trans('header.home') }}</a> &gt; {{ trans('header.latest_videos') }}
            </div>

            <div class="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 gap-8">
                @foreach($videos as $video)
                    @include(themeViewPath('frontend.components.cards.video'), ['video' => $video])
                @endforeach
            </div>

        </div>

        @include(themeViewPath('frontend.components.listings.listings-pagination'), ['data' => $videos])

    </section>

    {{-- site footer --}}
    @include(themeViewPath('frontend.components.footer'))

@endsection
