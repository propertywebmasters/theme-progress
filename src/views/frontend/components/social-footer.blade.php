@php
$service = app()->make(\App\Services\SocialAccount\Contracts\SocialAccountServiceInterface::class);
$socialAccounts = $service->get();

$siteName = app()->make(\App\Services\SiteSetting\Contracts\SiteSettingServiceInterface::class)->retrieve(\App\Models\SiteSetting::SETTING_SITE_NAME['key'], config('platform-cache.site_settings_enabled'));

$socialCount = 0;
if ($socialAccounts !== null) {
    $networks = collect(\App\Models\SocialAccount::SOCIAL_SYSTEMS)->pluck('name')->toArray();

    foreach ($networks as $network) {
        if ($socialAccounts->$network !== null) {
            $socialCount++;
        }
    }
}
@endphp

<div class="container mx-auto py-4">
    <div class="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 md:gap-4 px-2 lg:px-0">

        <div class="text-center md:text-left">
            <span class="text-xs tracking-tight leading-3">Copyright {{ $siteName }}&copy; {{ date('Y') }}. All rights reserved.</span>
        </div>

        <div class="mx-auto text-center hidden lg:block">
            @if($socialCount > 0)
                @foreach($networks as $network)
                    @if($socialAccounts->$network === null)
                        @continue
                    @endif
                    <a href="{{ $socialAccounts->$network }}" class="px-2 inline-block" target="_BLANK">
                        <img loading="lazy" src="{{ themeImage('social/'.$network.'.svg') }}" class="svg-inject primary-text fill-current h-6" alt="{{ $network }}">
                    </a>
                @endforeach
            @endif
        </div>

        @if(hasFeature(\App\Models\TenantFeature::FEATURE_SHOW_FOOTER_PW_LINK))
            <div class="text-center md:text-right">
                <span class="text-xs">
                    <a class="text-xs tracking-tight leading-3" href="{{ config('footer.pw_website_link') }}" target="_BLANK" rel="noopener">{{ config('footer.pw_website_link_text') }}</a> by Property Webmasters
                </span>
            </div>
        @endif

    </div>
</div>

