@php
    $searchControls = searchControlOptions(config('platform-cache.search_controls_enabled'));
    $absolutelyPosition = $absolutelyPosition ?? false;

    // DO NOT REMOVE - TAILWIND NEEDS TO SEE THESE TO GENERATE THEM
    // md:grid-cols-8 md:grid-cols-7 md:grid-cols-6 md:grid-cols-5
    // lg:grid-cols-10 lg:grid-cols-9 lg:grid-cols-8 lg:grid-cols-7 lg:grid-cols-6 lg:grid-cols-5

    $hasShortTermRentals = hasShortTermRentals();
    $hasLongTermRentals = hasLongTermRentals();
    $hasStudentRentals = isset($searchControls->student_rentals) && $searchControls->student_rentals;
    $hasHolidayRentals = isset($searchControls->holiday_rentals) && $searchControls->holiday_rentals;
@endphp
<div class="absolute {{ isset($video) ? 'top-32' : 'top-52' }} md:top-64 lg:top-1/2 {{ isset($video) ? 'mt-12 md:-mt-4 ' : '-mt-4' }} w-full mx-auto z-20">
    <form id="home-search" action="{{ localeUrl('/search') }}" method="post" enctype="application/x-www-form-urlencoded" class="sm:mt-10 mt-6 text-center mx-auto">
        <div class="relative container text-center mx-auto lg:px-2 px-8 md:px-16 xl:px-48 z-20">
            <div>
                <div class="grid grid-cols-1 lg:grid-cols-8">

                    @if(isset($searchControls->location) && $searchControls->location)
                            @php
                                $roundedClass = 'rounded-t-lg lg:rounded-full lg:rounded-r-none';
                                $borderClass = '';
                            @endphp

                            <div class="lg:col-span-3" style="background-color: transparent;">
                                <div class="w-full h-full pr-2 {{ $roundedClass }}" style="background-color: white;">
                                    <div class="pr-2 py-2 {{ $roundedClass }}">
                                        <div class="pr-3 relative {{ $borderClass }} {{ $roundedClass }}">
                                            <select id="development_location" name="development_location" class="w-full h-8 px-4 text-base bg-white focus:outline-none appearance-none select-carat {{ $roundedClass }}">
                                                <option value="">{{ trans('placeholder.select_location') }}</option>
                                                @foreach($uniqueDevelopmentLocations as $location)
                                                    <option value="{{ $location['url'] }}">{{ $location['text'] }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    @endif

                    <div class="pt-3 bg-white font-bold">
                        {{ trans('auth.or') }}
                    </div>

                    @if(isset($searchControls->property_type) && $searchControls->property_type)
                        @php
                            $roundedClass = '';
                            if ($searchControls->tender_type === 'sale_only' && isset($searchControls->location) && !$searchControls->location) {
                                $roundedClass = 'rounded-t-lg lg:rounded-full lg:rounded-r-none';
                            }
                        @endphp

                        <div class="lg:col-span-3" style="background-color: transparent;">
                            <div class="w-full h-full {{ $roundedClass }}" style="background-color: white;">
                                <div class="pr-2 py-2 {{ $roundedClass }}">
                                    <div class="pr-3 {{ $roundedClass }}">
                                        <input type="text" id="development_name" name="development_name"
                                                class="w-full h-8 px-4 text-base bg-white focus:outline-none appearance-none select-carat {{ $roundedClass }}" placeholder="{{ trans('placeholder.development_name') }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif

                    <div style="background-color: transparent;">
                        <div class="w-full h-full" style="background-color: transparent;">
                            <div class="h-full">
                                <div class="h-full">
                                    <button type="submit" class="rounded-b-lg lg:rounded-full lg:rounded-l-none primary-bg w-full block h-12 lg:h-full px-2 pl-2 lg:px-3 text-white text-center flex justify-center items-center">
                                        <span class="text-base uppercase text-white">{{ trans('label.search') }}</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <input type="hidden" name="locale" value="{{ app()->getLocale() }}">
        <input type="hidden" name="type" value="sale">
        <input type="hidden" name="is_development" value="1">
    </form>

</div>
