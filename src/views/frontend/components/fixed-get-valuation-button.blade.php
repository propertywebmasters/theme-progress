@if(hasFeature(\App\Models\TenantFeature::FEATURE_VALUATION_SYSTEM))
    @php
    $valuationDetails = getValuationUrlDetails()
    @endphp
    <div id="fixed-get-valuation-button" class="transition-all hidden lg:block fixed top-2/3 secondary-bg text-white rounded-tl-full rounded-bl-full z-40" style="transform: translate(0%, -50%); right: -10rem;">
        <a class="valuation-button whitespace-nowrap text-sm text-center tracking-tight font-bold uppercase px-6 py-4 block" target="{{ $valuationDetails['target'] }}" href="{{ $valuationDetails['url'] }}">
            <div class="flex items-center">
                <span class="valuation-button-icon mr-3">
                    <img class="svg-inject text-white w-auto fill-current stroke-current" src="{{ themeImage('valuation.svg') }}" alt="valuation" style="height: 25px;">
                </span>
                <span class="valuation-button-text text-white invisible">
                    {{ trans('label.get_a_valuation') }}
                </span>
            </div>
        </a>
    </div>
@endif
