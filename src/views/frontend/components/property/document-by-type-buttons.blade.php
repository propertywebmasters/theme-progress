@if (hasFeature(\App\Models\TenantFeature::FEATURE_DOCUMENTS_BY_TYPE) && $floorplanDocs->count() > 0)
    <span data-target="property-floorplan-documents-modal" class="modal-button cursor-pointer text-xs md:text-sm text-center tracking-wide rounded-3xl border border-white bg-white max-w-xs inline-block py-1 px-2 md:py-2 md:px-5 transition-all hover:text-white text-primary hover:bg-activeCcolor hover:border-activeCcolor duration-500">{{ trans('generic.floorplans') }}</span>
@endif
@if (hasFeature(\App\Models\TenantFeature::FEATURE_DOCUMENTS_BY_TYPE) && $epcDocs->count() > 0)
    <span data-target="property-epc-documents-modal" class="modal-button cursor-pointer text-xs md:text-sm text-center tracking-wide rounded-3xl border border-white bg-white max-w-xs inline-block py-1 px-2 md:py-2 md:px-5 transition-all hover:text-white text-primary hover:bg-activeCcolor hover:border-activeCcolor duration-500">{{ trans('generic.epc_rating') }}</span>
@endif
@if (hasFeature(\App\Models\TenantFeature::FEATURE_DOCUMENTS_BY_TYPE) && $brochureDocs->count() > 0)
    <span data-target="property-brochure-documents-modal" class="modal-button cursor-pointer text-xs md:text-sm text-center tracking-wide rounded-3xl border border-white bg-white max-w-xs inline-block py-1 px-2 md:py-2 md:px-5 transition-all hover:text-white text-primary hover:bg-activeCcolor hover:border-activeCcolor duration-500">{{ trans('generic.brochure_pdf') }}</span>
@endif
@if (hasFeature(\App\Models\TenantFeature::FEATURE_DOCUMENTS_BY_TYPE) && $otherDocs->count() > 0)
    <span data-target="property-other-documents-modal" class="modal-button cursor-pointer text-xs md:text-sm text-center tracking-wide rounded-3xl border border-white bg-white max-w-xs inline-block py-1 px-2 md:py-2 md:px-5 transition-all hover:text-white text-primary hover:bg-activeCcolor hover:border-activeCcolor duration-500">{{ trans('contact.other') }}</span>
@endif