<h3 class="text-2xl header-text mb-9 mt-12 header-text">{{ trans('header.property_features') }}</h3>
<ul class="grid grid-cols-2 md:gap-x-8">
        @foreach($property->features as $feature)
            @php
                $translation = trans('features.'.strtolower($feature->name));
                $translation = stripos($translation, 'features.') !== false ? $feature->name : $translation
            @endphp
            <li class="leading-normal tracking-tight font-light mb-8 mb-2 md:pb-8 border-b-2">{!! $translation  !!}</li>
        @endforeach
</ul>
