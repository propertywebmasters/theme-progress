@php
$textColour = !isset($textColour) ? 'text-black' : 'text-white';
$showSizing = $showSizing ?? false;
@endphp
<div class="flex gap-x-4 mt-4">
    @if ($property->isHouse())
        @if($property->bedrooms != 0)
            <div class="flex items-center">
                <img class="h-3 svg-inject primary-text" src="{{ themeImage('search/bed.svg') }}" alt="{{ trans('generic.bed') }}" loading="lazy">
                <span class="inline-block ml-2 text-sm tracking-tight {{ $textColour }}">{{ $property->bedrooms }} {{ trans('generic.bed') }}</span>
            </div>
            <div class="flex items-center">
                <img class="h-4 svg-inject primary-text" src="{{ themeImage('search/bath.svg') }}" alt="{{ trans('generic.bath') }}" loading="lazy">
                <span class="inline-block ml-2 text-sm tracking-tight {{ $textColour }}">{{ str_replace('.0', '', $property->bathrooms) }} {{ trans('generic.bath') }}</span>
            </div>
        @endif
    @endif

    @if($showSizing)
        @if($property->internal_size !== null)
            @php $unit = getAreaUnitSymbol($property->area_unit); @endphp
            <div class="flex items-center">
                <img src="{{ themeImage('internal-size.svg') }}" alt="resize" loading="lazy" class="primary-text fill-current stroke-current svg-inject h-5 mt-0.5">
                <span class="inline-block ml-2 text-sm tracking-tight {{ $textColour }}">{{ $property->internal_size }} {!! $unit !!}</span>
            </div>
        @endif
        @if($property->land_size !== null)
            @php $unit = getAreaUnitSymbol($property->area_unit); @endphp
            <div class="flex items-center">
                <img src="{{ themeImage('land-size.svg') }}" alt="resize" loading="lazy" class="primary-text fill-current stroke-current svg-inject h-5 mt-0.5">
                <span class="inline-block ml-2 text-sm tracking-tight {{ $textColour }}">{{ $property->land_size }} {!! $unit !!}</span>
            </div>
        @endif
    @endif

</div>
