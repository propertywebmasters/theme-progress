@php
    use App\Models\Currency;use App\Models\SiteSetting;
    use App\Models\TenantFeature;use App\Services\SiteSetting\Contracts\SiteSettingServiceInterface;use Illuminate\Support\Str;
    $currentCurrency = currentCurrency();
    $logoPath = assetPath(app()->make(SiteSettingServiceInterface::class)->retrieve(SiteSetting::SETTING_LOGO['key'], config('platform-cache.site_settings_enabled')));
    $transparentNavigation = $transparentNavigation ?? true;
    $siteEmail = app()->make(SiteSettingServiceInterface::class)->retrieve(SiteSetting::SETTING_SITE_EMAIL['key']);
    $siteTel = app()->make(SiteSettingServiceInterface::class)->retrieve(SiteSetting::SETTING_SITE_TEL['key'])
@endphp

<section class="navigation-bg">
    @if(hasFeature(TenantFeature::FEATURE_CONTACT_HEADER))
        @include(themeViewPath('frontend.components.contact-header'))
    @endif
    @if(env('CSS_BREAKPOINT_HELPER'))
        @include('frontend.components.helpers.breakpoint-helper')
    @endif

    @if (hasFeature(TenantFeature::FEATURE_FIXED_GET_VALUATION_BUTTON))
        @include(themeViewPath('frontend.components.fixed-get-valuation-button'))
    @elseif(!Request::is('property*'))
        @include(themeViewPath('frontend.components.fixed-social'))
    @endif
    <!-- ================================ Nav ================================ -->

    <nav id="navigation-alt" class="relative navigation-bg @if($transparentNavigation) bg-transparent @endif w-full z-40 nav-lang-{{app()->getLocale()}}">
        <div class="lg:container mx-auto flex justify-between px-4 flex-col lg:flex-row relative lg:items-center py-4">
            <div class="relative">
                <div id="logo-wrapper" class="relative" style="">
                    <a href="{{ localeUrl('/') }}" class="block">
                        <img class="relative block @if (strtolower(getFileExtensionForFilename($logoPath)) === 'svg') svg-inject @endif" src="{{ $logoPath }}" alt="logo">
                    </a>
                </div>
            </div>

            <div class="hidden lg:block absolute lg:relative left-0 top-11 lg:top-0 lg:pb-0 pb-5 w-full lg:w-auto z-10 bg-navigation lg:bg-transparent secondary-bg lg:pl-0">
                <ul class="flex flex-col lg:flex-row xl:gap-x-4 gap-x-2 items-center main-menu">
                    @foreach ($navigation as $nav)
                        @php
                            $anchorLabel = getLocaleAnchorLabel($nav)
                        @endphp

                        <li @if($nav->children->count()) class="relative dropdown-parent" @endif>
                            <a class="text-sm text-right tracking-tight nav-text inline-block transition-all" href="{{ localeUrl($nav->url) }}"
                               data-target="menu-{{ Str::slug($anchorLabel) }}">
                                {{ $anchorLabel }}
                                @if ($nav->children->count() > 0)
                                    <img class="down-carat svg-inject inline-block h-2 ml-2" src="{{ themeImage('down-carat.svg') }}" alt="&or;">
                                @endif
                            </a>
                            @if($nav->children->count() > 0)
                                <div class="relative lg:absolute lg:left-0 hidden lg:-translate-x-1/2 lg:top-10 menu-{{ Str::slug($anchorLabel) }}">
                                    <ul class="bg-white text-gray-500 w-auto text-sm xl:text-md text-gray-900 lg:shadow no-drag">
                                        @foreach ($nav->children as $child)
                                            @php
                                                $childAnchorLabel = getLocaleAnchorLabel($child)
                                            @endphp
                                            <li class="px-4 py-2 hover:bg-gray-200 hover:cursor-pointer border-transparent hover:bg-blue-500 hover:text-white">
                                                <a class="nav-text whitespace-nowrap block w-full text-sm xl:text-md {{ $child->class }}" href="{{ localeUrl($child->url) }}"
                                                   target="{{ $child->target }}">{{ $childAnchorLabel }}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>

                                <div
                                    class="menu-dropdown opacity-0 invisible duration-300 text-xs leading-loose tracking-tight text-white inline-block w-auto bg-primary py-6 px-7 absolute top-14 -left-8 menu-{{ Str::slug($anchorLabel) }}">
                                    <div class="menu-square absolute bg-primary -top-3 left-1/4 z-10 transform rotate-45"></div>
                                    <ul>
                                        @foreach ($nav->children as $child)
                                            @php
                                                $childAnchorLabel = getLocaleAnchorLabel($child)
                                            @endphp
                                            <li>
                                                <a class="block hover:opacity-70 duration-300 {{ $child->class }}" href="{{ localeUrl($child->url) }}"
                                                   target="{{ $child->target }}">
                                                    {{ $childAnchorLabel }}
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                        </li>
                    @endforeach

                    @if(count($tenantCurrencies) > 1)
                        <li class="dropdown-parent relative text-left inline-block tracking-tight cursor-pointer">
                            <div class="inline-block">
                                <div class="inline-flex justify-center w-full nav-text text-sm items-center" aria-expanded="true" aria-haspopup="true"
                                     data-target="menu-currencies">
                                    {{ $currentCurrency->iso }}
                                    <svg class="-mr-1 h-5 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                        <path fill-rule="evenodd"
                                              d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                                              clip-rule="evenodd"/>
                                    </svg>
                                </div>
                            </div>

                            <!-- Dropdown -->
                            <div
                                class="menu-dropdown opacity-0 invisible duration-300 text-xs leading-loose tracking-tight nav-text inline-block w-auto bg-primary py-6 px-7 absolute top-14 -left-8 menu-currencies">
                                <div class="menu-square absolute bg-primary -top-3 left-1/4 z-10  transform rotate-45"></div>
                                <ul>
                                    @foreach($tenantCurrencies as $currencyIso)
                                        <li>
                                            <a class="block duration-300 nav-text" href="{{ localeUrl('/currency/'.strtolower($currencyIso)) }}" rel="nofollow"
                                               title="{{ Currency::getPropertyByIso('name', $currencyIso) }} Currency">
                                                {{ strtoupper($currencyIso) }} - {!! Currency::getPropertyByIso('symbol', $currencyIso) !!}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </li>
                    @endif

                    @if(count(supportedLocales()) >= 2)
                        <li class="dropdown-parent relative text-left inline-block tracking-tight cursor-pointer">
                            <div id="lang" class="inline-block">
                                <div class="inline-flex justify-center w-full nav-text text-sm items-center" aria-expanded="true" aria-haspopup="true" data-target="menu-locale">
                                    {{ Str::upper(app()->getLocale()) }}
                                    <svg class="-mr-1.5 h-5 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                        <path fill-rule="evenodd"
                                              d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                                              clip-rule="evenodd"/>
                                    </svg>
                                </div>
                            </div>

                            @if(count(supportedLocales()) > 1)
                                <!-- Dropdown -->
                                <div
                                    class="menu-dropdown opacity-0 invisible duration-300 text-xs leading-loose tracking-tight nav-text inline-block w-auto bg-primary py-6 px-7 absolute top-14 -left-8 menu-locale">
                                    <div class="menu-square absolute bg-primary -top-3 left-1/4 z-10 transform rotate-45"></div>
                                    <ul>
                                        @foreach(supportedLocales() as $locale)
                                            @if ($locale === app()->getLocale())
                                                @continue
                                            @endif
                                            <li>
                                                <a class="nav-text block duration-300" href="{{ switchLocaleUrl(url()->current(), $locale) }}" rel="nofollow"
                                                   title="{{ countryName($locale) }}">{{ strtoupper($locale) }} - {{ strtoupper(trans('languages.'.$locale)) }}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                        </li>
                    @endif

                    @if(hasFeature(TenantFeature::FEATURE_ACCOUNT_SYSTEM))
                        @if(user())
                            <li>
                                <a class="text-xs text-center tracking-wide nav-text rounded-3xl cta-border max-w-xs block transition-all hover:text-white duration-500 bg-transparent"
                                   href="{{ localeUrl('/account') }}">{{ trans('auth.my_account') }}</a>
                            </li>
                        @else
                            <li>
                                <a id="auth-button"
                                   class="modal-button text-xs text-center tracking-wide nav-text rounded-3xl primary-border max-w-xs block transition-all hover:text-white duration-500 bg-transparent"
                                   data-target="preauth-modal" href="javascript:">{{ trans('auth.signin_register') }}</a>
                            </li>
                        @endif
                    @endif

                    @if (hasFeature(TenantFeature::FEATURE_VALUATION_SYSTEM))
                        <li><a id="valuation-cta" class="text-xs text-center tracking-wide nav-text rounded-3xl max-w-xs block transition-all primary-bg duration-500 lg:mt-0 mt-4"
                               href="{{ localeUrl('/valuation') }}">{{ trans('label.get_a_valuation') }}</a></li>
                    @endif

                </ul>
            </div>

            <div id="hamburger" class="tham tham-e-squeeze tham-w-8 absolute right-4 top-1 lg:hidden" style="top: 50%; transform: translateY(-50%);">
                <div class="tham-box">
                    <div class="tham-inner bg-white"/>
                </div>
            </div>
        </div>
        <div class="flex items-center justify-center absolute right-16 top-2 z-10 lg:hidden" style="top: 50%; transform: translateY(-50%);">
            <a href="mailto:{{ $siteEmail }}"><img class="h-5 mr-4 -mt-1 svg-inject" src="{{ themeImage('email2.svg') }}" alt="email"></a>
            <a href="tel:{{ $siteTel }}"><img class="h-5 mr-2 -mt-1 svg-inject" src="{{ themeImage('phone.svg') }}" alt="phone"></a>
        </div>

        <div id="mobile-menu" class="absolute hidden lg:hidden w-full pt-8 secondary-bg z-40 h-screen top-full left-0">
            <ul class="flex flex-col gap-x-2 items-center main-menu">
                @foreach ($navigation as $nav)
                    @php
                        $anchorLabel = getLocaleAnchorLabel($nav)
                    @endphp

                    <li @if($nav->children->count()) class="relative dropdown-parent" @endif>
                        <a class="text-sm text-right tracking-tight nav-text inline-block transition-all hover:text-secondary"
                           href="{{ $nav->children->count() ? 'javascript:;' : localeUrl($nav->url) }}"
                           data-target="menu-{{ Str::slug($anchorLabel) }}">
                            {{ $anchorLabel }}
                            @if ($nav->children->count() > 0)
                                <img class="down-carat svg-inject inline-block h-2 ml-2" src="{{ themeImage('down-carat.svg') }}" alt="&or;">
                            @endif
                        </a>
                        @if($nav->children->count() > 0)
                            <div class="relative lg:absolute lg:left-0 hidden lg:-translate-x-1/2 lg:top-10 menu-{{ Str::slug($anchorLabel) }}">
                                <ul class="bg-white text-gray-500 w-auto text-sm xl:text-md text-gray-900 lg:shadow no-drag">
                                    @foreach ($nav->children as $child)
                                        @php
                                            $childAnchorLabel = getLocaleAnchorLabel($child)
                                        @endphp
                                        <li class="px-4 py-2 hover:bg-gray-200 hover:cursor-pointer border-transparent hover:bg-blue-500 hover:text-white">
                                            <a class="nav-text whitespace-nowrap block w-full text-sm xl:text-md {{ $child->class }}" href="{{ localeUrl($child->url) }}"
                                               target="{{ $child->target }}">{{ $childAnchorLabel }}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>

                            <div
                                class="menu-dropdown opacity-0 invisible duration-300 text-xs leading-loose tracking-tight text-white inline-block w-auto bg-primary py-6 px-7 absolute top-14 -left-8 menu-{{ Str::slug($anchorLabel) }}">
                                <div class="menu-square absolute bg-primary -top-3 left-1/4 z-10 transform rotate-45"></div>
                                <ul>
                                    @foreach ($nav->children as $child)
                                        @php
                                            $childAnchorLabel = getLocaleAnchorLabel($child)
                                        @endphp
                                        <li>
                                            <a class="block hover:opacity-70 duration-300 {{ $child->class }}" href="{{ localeUrl($child->url) }}" target="{{ $child->target }}">
                                                {{ $childAnchorLabel }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </li>
                @endforeach

                @if(count($tenantCurrencies) > 1)
                    <li class="dropdown-parent relative  text-left inline-block tracking-tight cursor-pointer">
                        <div class="inline-block">
                            <div class="inline-flex justify-center w-full nav-text text-sm items-center" aria-expanded="true" aria-haspopup="true" data-target="menu-currencies">
                                {{ $currentCurrency->iso }}
                                <svg class="-mr-1 h-5 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                    <path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                                          clip-rule="evenodd"/>
                                </svg>
                            </div>
                        </div>

                        <!-- Dropdown -->
                        <div
                            class="menu-dropdown opacity-0 invisible duration-300 text-xs leading-loose tracking-tight text-white inline-block w-auto bg-primary py-6 px-7 absolute top-14 -left-8 menu-currencies">
                            <div class="menu-square absolute bg-primary -top-3 left-1/4 z-10  transform rotate-45"></div>
                            <ul>
                                @foreach($tenantCurrencies as $currencyIso)
                                    <li>
                                        <a class="block hover:opacity-70 duration-300" href="{{ localeUrl('/currency/'.strtolower($currencyIso)) }}" rel="nofollow"
                                           title="{{ Currency::getPropertyByIso('name', $currencyIso) }} Currency">
                                            {{ strtoupper($currencyIso) }} - {!! Currency::getPropertyByIso('symbol', $currencyIso) !!}
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </li>
                @endif

                @if(count(supportedLocales()) >= 2)
                    <li class="dropdown-parent relative  text-left inline-block tracking-tight cursor-pointer">
                        <div id="lang" class="inline-block">
                            <div class="inline-flex justify-center w-full nav-text text-sm items-center" aria-expanded="true" aria-haspopup="true" data-target="menu-locale">
                                {{ Str::upper(app()->getLocale()) }}
                                <svg class="-mr-1.5 h-5 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                    <path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                                          clip-rule="evenodd"/>
                                </svg>
                            </div>
                        </div>

                        @if(count(supportedLocales()) > 1)
                            <!-- Dropdown -->
                            <div
                                class="menu-dropdown opacity-0 invisible duration-300 text-xs leading-loose tracking-tight text-white inline-block w-auto bg-primary py-6 px-7 absolute top-14 -left-8 menu-locale">
                                <div class="menu-square absolute bg-primary -top-3 left-1/4 z-10  transform rotate-45"></div>
                                <ul>
                                    @foreach(supportedLocales() as $locale)
                                        @if ($locale === app()->getLocale())
                                            @continue
                                        @endif
                                        <li>
                                            <a class="block hover:opacity-70 duration-300" href="{{ switchLocaleUrl(url()->current(), $locale) }}" rel="nofollow"
                                               title="{{ countryName($locale) }}">{{ Str::upper($locale) }} - {{ countryName($locale) }}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </li>
                @endif

                @if(hasFeature(TenantFeature::FEATURE_ACCOUNT_SYSTEM))
                    @if(user())
                        <li class="mt-4">
                            <a class="text-sm text-center tracking-wide nav-text rounded-3xl border  max-w-xs block transition-all hover:text-white duration-500 primary-bg cta-text"
                               href="{{ localeUrl('/account') }}">{{ trans('auth.my_account') }}</a>
                        </li>
                    @else
                        <li class="mt-4">
                            <a id="auth-button"
                               class="modal-button text-xs text-center tracking-wide nav-text rounded-3xl lg:border max-w-xs block transition-all hover:text-white duration-500 primary-bg cta-text"
                               data-target="preauth-modal" href="javascript:">{{ trans('auth.signin_register') }}</a>
                        </li>
                    @endif
                @endif

                @if (hasFeature(TenantFeature::FEATURE_VALUATION_SYSTEM))
                    @php
                        $valuationDetails = getValuationUrlDetails()
                    @endphp
                    <li><a id="valuation-cta" class="text-xs text-center tracking-wide rounded-3xl max-w-xs block transition-all cta duration-500 lg:mt-0 mt-4 hover:bg-transparent"
                           href="{{ $valuationDetails['url'] }}" target="{{ $valuationDetails['target'] }}">{{ trans('label.get_a_valuation') }}</a></li>
                @endif

            </ul>
        </div>
    </nav>
</section>
