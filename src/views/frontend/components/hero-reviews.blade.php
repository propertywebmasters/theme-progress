@php
    if (!isset($extraClass)) {
        $extraClass = '';
    }
@endphp
@if($reviewServices->count() > 0)
<div class="review-systems flex items-center justify-center bg-gray-200 w-full text-center {{ $extraClass }} py-4">
   @foreach($reviewServices as $reviewService)
        <div class="mx-1">
            <a id="{{ $reviewService->id }}" href="{!! $reviewService->url !!}" target="_BLANK" class="block">
                <img class="mx-auto h-8 md:h-12 w-auto object-contain review-image review-{{ \Illuminate\Support\Str::slug($reviewService->name) }}" src="{!! assetPath($reviewService->image) !!}">
            </a>
        </div>
    @endforeach
</div>
@endif
