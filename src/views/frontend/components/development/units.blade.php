@php
$entityType = $property->is_development ? 'development' : 'property';
$units = $property->units
@endphp

<h3 class="header-text text-2xl leading-loose tracking-tight text-primary mb-6">{{ trans('header.development_units') }}</h3>

<div class="grid grid-cols-2 lg:grid-cols-8 gap-0 gap-y-4 bg-whiter">

    @foreach($units as $unit)
        @php
        $bathrooms = str_replace('.0', '', $unit->bathrooms);
        $bedrooms = $unit->bedrooms > 0 ? $unit->bedrooms : 'N/A';
        $bathrooms = $unit->bathrooms > 0 ? $bathrooms : 'N/A';
        @endphp

        @php
            $bgImageClass = 'background-position: center center; background-size: cover; cursor: pointer;';
            $bgImageClassData = 'background-position: center center; background-size: cover; cursor: pointer; background-image: url("'.getPropertyImage($unit->images->first()->filepath, 'sm').'");';
        @endphp

        <div class="bg-whiter col-span-2 lg:col-span-1 pr-4">
            <div onclick="window.location='{{ localeUrl('/property/'.$property->url_key) }}'" class="overflow-hidden bg-lazy-load w-full min-h-32 h-32 lg:min-h-20 lg:h-20 w-full overflow-hidden" style="{{ $bgImageClass }}" data-style="{{ $bgImageClassData }}"><!-- --></div>
        </div>
        <div class="bg-whiter">
            <span class="block uppercase text-sm text-gray-600 pt-4">Type</span>
            <span class="block">{{ trans('label.'.$property->tender_type) }}</span>
        </div>
        <div class="bg-whiter">
            <span class="block uppercase text-sm text-gray-600 pt-4">{{ trans('label.reference') }}</span>
            <span class="block">{{ $unit->displayReference() }}</span>
        </div>
        <div class="bg-whiter">
            <span class="block uppercase text-sm text-gray-600 pt-4">{{ trans('label.property_type') }}</span>
            <span class="block">{{ $unit->propertyType->name }}</span>
        </div>
        <div class="bg-whiter">
            <span class="block uppercase text-sm text-gray-600 pt-4">{{ trans('label.bedrooms') }}</span>
            <span class="block">{{ $bedrooms }}</span>
        </div>
        <div class="bg-whiter">
            <span class="block uppercase text-sm text-gray-600 pt-4">{{ trans('label.bathrooms') }}</span>
            <span class="block">{{ $bathrooms }}</span>
        </div>
        <div class="bg-whiter">
            <span class="block uppercase text-sm text-gray-600 pt-4">{{ trans('generic.price') }}</span>
            <span class="block">{!! $unit->displayPrice() !!}</span>
        </div>
        <div class="bg-whiter col-span-2 lg:col-span-1">
            <a href="{{ localeUrl('/property/'.$unit->url_key) }}" class="mt-6 block align-middle w-full text-sm xl:text-base text-center tracking-wide font-bold uppercase rounded-full transition-all py-4 md:py-2 primary-bg text-white block cta cta-text scroll-to">{{ trans('generic.view') }}</a>
        </div>
    @endforeach

</div>
