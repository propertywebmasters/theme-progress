@php
$textColour = !isset($textColour) ? 'text-black' : 'text-white';
$minBeds = $property->min_bedrooms;
$maxBeds = $property->max_bedrooms
@endphp
<div class="flex gap-x-4 mt-4">

    <div class="flex items-center">
        <span class="inline-block text-sm tracking-tight {{ $textColour }}">Ref: {{ $property->displayReference() }}</span>
    </div>

    @if ($property->isHouse())
        @if($property->bedrooms != 0)
            <div class="flex items-center">
                <img class="h-3 svg-inject primary-text" src="{{ themeImage('search/bed.svg') }}" alt="{{ trans('generic.bed') }}" loading="lazy">
                <span class="inline-block ml-2 text-base tracking-tight {{ $textColour }}">{{ $minBeds }} - {{ $maxBeds }} {{ trans('generic.bed') }}</span>
            </div>
        @endif
    @endif
    @if($property->internal_size !== null)
        @php
            $unit = getAreaUnitSymbol($property->area_unit);
        @endphp
        <div class="flex items-center">
            <img class="h-3 svg-inject primary-text" src="{{ themeImage('search/resize.svg') }}" alt="area" loading="lazy">
            <span class="inline-block ml-2 text-base tracking-tight {{ $textColour }}">{{ ucfirst(trans('price_qualifiers.from')) }} {{ str_replace('.0', '', $property->internal_size) }}{!! $unit !!}</span>
        </div>
    @endif
</div>
