@php
    $entityType = $property->is_development ? 'development' : 'property';

    $propertyDescription = $property->descriptions->where('locale', app()->getLocale())->first();
    if ($propertyDescription === null) {
        $propertyDescription = $property->descriptions->first();
    }
@endphp

@if($propertyDescription !== null)
    <div>
        <h3 class="header-text text-2xl leading-loose tracking-tight text-primary mb-6">{{ trans('header.'.$entityType.'_description') }}</h3>
        <p class="text-base leading-normal tracking-tight text-browngrey">{!! optional($propertyDescription)->long !!}</p>

        @if(hasFeature(\App\Models\TenantFeature::FEATURE_PDF_GENERATION))
            @if (hasFeature(\App\Models\TenantFeature::FEATURE_PDF_BEHIND_FORM))
                <a data-target="brochure-request-modal" class="download-pdf mt-8 text-base text-center tracking-wide font-bold header-text uppercase rounded-3xl border border-solid primary-border primary-text inline-block py-3 px-8 transition-all modal-button" href="javascript:">{{ trans('generic.download_pdf') }}</a>
            @else
                <a class="download-pdf mt-8 text-base text-center tracking-wide font-bold header-text uppercase rounded-3xl border border-solid primary-border primary-text inline-block py-3 px-8 transition-all" href="/property/{{ $property->url_key }}/pdf" target="_BLANK">{{ trans('generic.download_pdf') }}</a>
            @endif
        @endif
    </div>
@endif
