@php
$entityType = $property->is_development ? 'development' : 'property'
@endphp
@if ($property->features->isNotEmpty())
        <div>
            <h3 class="header-text text-2xl leading-loose tracking-tight text-primary mb-6">{{ trans('header.'.$entityType.'_features') }}</h3>
            <div>
                <ul class="grid lg:grid-cols-2 grid-cols-1 gap-8">
                    @foreach($property->features as $feature)
                        @php
                            $translation = trans('features.'.strtolower($feature->name));
                            $translation = stripos($translation, 'features.') !== false ? $feature->name : $translation
                    @endphp
                    <li class="py-2 text-base leading-normal tracking-tight border-b border-slid">{{ $translation }}</li>
                @endforeach
            </ul>
        </div>
    </div>
@endif
