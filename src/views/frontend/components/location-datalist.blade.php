@php
    $dataList = app()->make(App\Services\Search\Contracts\SearchServiceInterface::class)->dataListLocations();
    $roundedClass = '';
    $additionalClasses = 'lg:pl-2 border-l-0 pl-0';

    if (isset($searchControls->tender_type) && $searchControls->tender_type !== 'sale_only') {
        $roundedClass = 'lg:rounded-l-none';
        $additionalClasses .= ' lg:border-l';
        $colSpan = 'col-span-3';
    }

    if (isset($searchControls->tender_type) && $searchControls->tender_type === 'sale_only') {
        $colSpan = 'col-span-4';
    }

    if (!isset($extraCss)) {
        $extraCss = '';
    }
@endphp

<div id="location-datalist" class="location-datalist-container {{ $roundedClass }} lg:rounded-r-none my-2 lg:my-0 relative {{ $extraCss }}">

    @include(themeViewPath('frontend.components.location-search-input-full-screen-filter'), ['datalist' => true, 'additionalClasses' => $additionalClasses, 'styles' => 'z-index: 2;'])

    <datalist id="browsers" role="listbox" class="absolute w-full text-xs top-10 left-0" style="left: 0rem !important;">
        <div class="rounded-lg rounded-b-none">
            <div class="drop-shadow-md shadow-inner text-sm overflow-y-scroll overflow-x-hidden max-h-44 md:max-h-52 p-0 secondary-bg text-white border border-grey2">
                @php $i = 1 @endphp
                @foreach($dataList as $text => $value)
                    <option class="overflow-hidden text-xs text-white cursor-pointer hover-primary-bg py-2 px-4 @if($i == 1)  @endif" data-value="{{ $value }}">{{ $text }}</option>
                    @php $i++ @endphp
                @endforeach
            </div>
        </div>
    </datalist>

    <input type="hidden" name="location_url">
</div>
