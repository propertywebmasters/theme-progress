<div class="team-member w-full bg-white @if (isset($css) && $css !== null) {{ $css }} @endif">
    <div class="relative h-100 center-cover-bg bg-lazy-load cursor-pointer {{ $teamMember->description ? 'group' : '' }}" data-style="background-image: url({{ assetPath($teamMember->photo) }})" onclick="window.location=href='/meet-the-team/{{ $teamMember->uuid }}'">
        &nbsp;
    </div>
    <div class="py-2 relative">
        <div class="team-member-info py-0 pb-3 sm:overflow-hidden text-left">
            <a class="block font-medium text-lg" >{{ $teamMember->name }}</a>
            <p class="text-sm font-normal block primary-text text-md mb-2">{{ $teamMember->role }}</p>
        </div>
    </div>
</div>
