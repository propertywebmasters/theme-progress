@php
    use Illuminate\Support\Str;$imageClasses = 'min-h-46 max-h-46 sm:min-h-46 sm:max-h-46 md:min-h-58 md:max-h-58 lg:min-h-68 lg:max-h-68 xl:min-h-68 xl:max-h-68';
    $description = strip_tags(optional($data)->description);
    $description = Str::limit($description, 425)
@endphp

<div class="p-4 lg:p-8 w-full bg-white shadow-md @if (isset($css) && $css !== null) {{ $css }} @endif">
    <div onclick="window.location='{{ localeUrl('/careers/'.$career->url_key) }}'" class="overflow-hidden center-cover-bg bg-lazy-load cursor-pointer"><!-- --></div>

    <div class="grid grid-cols-4">
        <div class="col-span-3">
            <a href="{{ localeUrl('/careers/'.$career->url_key) }}" class="line-clamp-1 overflow-hidden block text-xl font-medium hover:underline primary-text leading-5 inline" title="{!! optional($data)->title !!}">{!! optional($data)->title !!}</a>
        </div>

        <div class="col-span-4 xl:col-span-1 mt-1 xl:mt-0">
            <span class="text-xl xl:float-right font-bold">{{ $career->salary }}</span>
        </div>

        <div class="col-span-4">
            <p class="text-sm font-normal text-justify pt-4 block pb-6 h-60 overflow-hidden">{!! $description !!}</p>
        </div>

        <div class="col-span-2 border-t-2 sm:mt-4">
            <span class="flex-1 text-left text-sm xl:text-base font-bold tracking-wider items-baseline uppercase block pt-6">
                {{ $career->created_at->format('jS F Y') }}
            </span>
        </div>

        <div class="col-span-2 text-right border-t-2 pt-6 sm:mt-4">
            <a class="text-sm xl:text-base primary-text font-bold uppercase hover:underline" href="{{ localeUrl('/careers/'.$career->url_key) }}" title="{!! optional($data)->title !!}">
                {{ trans('generic.read_more') }} <img class="svg-inject inline-block primary-text stroke-current fill-current h-4 pl-1 -mt-0.5" src="{{ themeImage('icons/right-arrow.svg') }}" alt="{{ trans('generic.read_more') }}">
            </a>
        </div>
    </div>
</div>