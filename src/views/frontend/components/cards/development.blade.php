@php
    use Illuminate\Support\Str;
    $images = $property->images->take(3);
    $swiperId = Str::random();

    $splitMapMode = $splitMapMode ?? false;

    $class = $data = '';
    if (!isset($extraCss)) {
        $extraCss = '';
    }
    if ($splitMapMode) {
        $class = 'map-listing cursor-pointer';
        $infoWindowHtml = view('frontend.components.info-window', ['property' => $property])->render();
        $data =  'data-identifier="'.$property->uuid.'" data-latitude="'.$property->location->latitude.'" data-longitude="'.$property->location->longitude.'" data-infowindow="'.str_replace("\n", "", htmlentities($infoWindowHtml)).'"';
    } else {
        $data =  'data-gallery_images="'.propertyImagesString($property).'"';
    }

    $descriptionSummary = null;
    $propertyDescription = $property->descriptions->where('locale', app()->getLocale())->first();
    if ($propertyDescription === null) {
        $propertyDescription = $property->descriptions->first();
    }

    if ($propertyDescription !== null) {
        $tmpDesc = strip_tags($propertyDescription->long);
        $descriptionSummary = \Illuminate\Support\Str::limit($tmpDesc, 300);
    }
@endphp

<div class="b-11 bg-white border border-gray-50 @if(hasFeature(\App\Models\TenantFeature::FEATURE_LISTINGS_INLINE_GALLERY)) inline-listing-gallery @endif @if($splitMapMode) property-listing-map @endif {{ $class }} {{ $extraCss }}" {!! $data !!}">
    <div class="relative">

        @php
            $imageUrl = $property->images->first() === null ? '/img/image-coming-soon.jpg' : $property->images->first()->filepath;
            $bgImageClass = 'min-height: 300px; max-height: 300px; overflow-hidden background-position: center center; background-size: cover; width: 100%; cursor: pointer; background-image: url("'.getPropertyImage($imageUrl).'")';
        @endphp
        <div class="bg-lazy-load center-cover-bg" style="{{ $bgImageClass }}" data-style="{{ $bgImageClass }}" id="property-{{ $property->uuid }}" onclick="window.location='{{ localeUrl('/development/'.$property->url_key) }}'">&nbsp;</div>

        @if($property->tender_status !== \App\Models\Property::TENDER_STATUS_AVAILABLE)
            <div class="absolute tender-status top-2 right-2 primary-bg text-white p-2 px-3 rounded-3xl">{{ $property->tender_status }}</div>
        @endif

        @if(hasFeature(\App\Models\TenantFeature::FEATURE_LISTINGS_INLINE_GALLERY) && !$splitMapMode && $property->images->count() > 1)
            <div class="inline-gallery-control hidden previous-image p-4 absolute left-0 top-1/2 -mt-8 text-white cursor-pointer" data-target="property-{{ $property->uuid }}">
                <img class="inline-block svg-inject text-white fill-current stroke-current h-6 p-0 m-0" src="{{ themeImage('icons/left-carat.svg') }}" loading="lazy">
            </div>
            <div class="inline-gallery-control hidden next-image p-4 absolute right-0 top-1/2 -mt-8 text-white cursor-pointer" data-target="property-{{ $property->uuid }}">
                <img class="inline-block svg-inject text-white fill-current stroke-current h-6 p-0 m-0" src="{{ themeImage('icons/right-carat.svg') }}" loading="lazy">
            </div>
        @endif

        @if(hasFeature(\App\Models\TenantFeature::FEATURE_SHORTLIST_SYSTEM))
            @if (user() === null)
                @php
                $actionClass = 'modal-button';
                $dataAttribs = 'data-target="preauth-modal"';
                $imgClass = '';
                @endphp
            @else
                @php
                    if (in_array($property->uuid, $shortlistedProperties->pluck('property_uuid')->toArray())) {
                        // this property is in the shortlist
                        $actionClass = 'shortlist-toggle-simple';
                        $dataAttribs = 'data-on-class="primary-text fill-current stroke-current" data-property="'.$property->uuid.'"';
                        $imgClass = 'primary-text fill-current stroke-current';
                    } else {
                        // this property is in the shortlist
                        $actionClass = 'shortlist-toggle-simple';
                        $dataAttribs = 'data-on-class="primary-text fill-current stroke-current" data-property="'.$property->uuid.'"';
                        $imgClass = '';
                    }
                @endphp
            @endif

            <a href="javascript:" class="{{ $actionClass }} absolute right-4 -bottom-9" title="{{ trans('shortlist.save') }}" {!! $dataAttribs !!}>
                <img src="{{ themeImage('search/heart.svg') }}" class="svg-inject {{ $imgClass }}" alt="love" loading="lazy">
            </a>
        @endif
    </div>
    <div class="px-5 lg:px-7 md:py-7 pt-5 pb-7 shadow-md min-h-56">
        <div>
            <a href="{{ localeUrl('/development/'.$property->url_key) }}" class="text-xl header-text block line-clamp-2 overflow-hidden pr-20 lg:pr-0">{!! $property->displayName() !!}</a>
            <span class="text-sm tracking-tight text-gray-600 block mb-4 mt-2 line-clamp-1">{!! $property->location->displayAddress() !!}</span>
            <div class="mt-5 text-xl tracking-tight font-medium text-secondary-text uppercase">{!! $property->displayPrice() !!}</div>
        </div>
        @include(themeViewPath('frontend.components.development.bed-bath-info'))
    </div>
</div>
