<a href="{{ localeUrl('/news/'.$article->url_key) }}" class="grid grid-cols-12 gap-4 items-center {{ $css }}">
    <div class="col-span-12 sm:col-span-4 relative min-h-40">
        <img class="w-full h-96 sm:h-40 md:h-48 lg:h-40 object-cover" src="{{ assetPath($article->image) }}" loading="lazy">
        <div class="absolute left-0 top-0 w-full h-full opacity-50 bg-black"></div>
    </div>
    <div class="sm:col-span-8 col-span-12">
        <span class="uppercase text-gray-600">
            {{ $article->created_at->formatLocalized('%d %b %Y') }}
        </span>
        <p class="mt-2 text-lg leading-tight tracking-tight">
            {!! Str::limit(strip_tags($article->content, 360)) !!}
        </p>
    </div>
</a>
