@php
    use Illuminate\Support\Str;
    $splitMapMode = $splitMapMode ?? false;
    $images = $property->images->take(3);
    $swiperId = Str::random()
@endphp

<div class="swiper-slide flex-shrink-0 relative list-single">
    <a href="{{ localeUrl('/property/'.$property->url_key) }}">

        <div class="s-slider swiper mySwiperPropertiesInner overflow-hidden md:p-0 z-1 list-none relative z-10 max-w-5xl">
            <div class="swiper-wrapper flex z-1 relative w-full transition-transform box-content ">
                @foreach($images as $image)
                    <div class="swiper-slide flex-shrink-0 relative">
                        <img class="w-full h-112 md:h-118 lg:h-84 xl:h-104 object-cover" src="{{ getPropertyImage($image->filepath, 'lg') }}" alt="img" loading="lazy">
                        <div class="bg-opacity-40 bg-black absolute top-0 left-0 w-full h-full z-0 transition-all duration-500"></div>
                    </div>
                @endforeach
            </div>
            <div class="cursor-pointer flex absolute right-2 top-2">
                <img class="svg-inject swiper-button-nextInner" src="{{ themeImage('team/arrow-left.svg') }}" alt="prev" loading="lazy">
                <img class="svg-inject swiper-button-prevInner" src="{{ themeImage('team/arrow-right.svg') }}" alt="next" loading="lazy">
            </div>
        </div>

        <div
            class="absolute bottom-8 left-0 xl:px-8 px-4 w-full z-10  transition-all hovered-fade duration-500">
            <p class="text-2xl text-white block line-clamp-1">{!! $property->displayName() !!}</p>
            <span class="text-sm tracking-tight text-white">{{ $property->location->displayAddress() }}</span>
            <p class="text-xl tracking-tight font-medium text-white md:mt-7 mt-2 mb-4">{!! $property->displayPrice() !!}</p>
            @include(themeViewPath('frontend.components.property.bed-bath-info'), ['textColour' => 'white',])
        </div>

    </a>

    @if(hasFeature(\App\Models\TenantFeature::FEATURE_SHORTLIST_SYSTEM))
        @if (user() === null)
            @php
                $actionClass = 'modal-button';
                $dataAttribs = 'data-target="preauth-modal"';
                $imgClass = '';
            @endphp
        @else
            @php
                if (in_array($property->uuid, $shortlistedProperties->pluck('property_uuid')->toArray())) {
                    // this property is in the shortlist
                    $actionClass = 'shortlist-toggle-simple';
                    $dataAttribs = 'data-on-class="primary-text fill-current stroke-current" data-property="'.$property->uuid.'"';
                    $imgClass = 'primary-text fill-current stroke-current';
                } else {
                    // this property is in the shortlist
                    $actionClass = 'shortlist-toggle-simple';
                    $dataAttribs = 'data-on-class="primary-text fill-current stroke-current" data-property="'.$property->uuid.'"';
                    $imgClass = '';
                }
            @endphp
        @endif
        <div class="flex items-center absolute top-4 right-2 z-20">
            <div class="cursor-pointer mr-2 -mt-1">
                <a href="javascript:" class="{{ $actionClass }}" {!! $dataAttribs !!}><img loading="lazy" class="svg-inject {{ $imgClass }}" src="{{ themeImage('team/heart-white.svg') }}" alt="heart" title="{{ trans('shortlist.save') }}"></a>
            </div>
        </div>
    @endif

</div>
