<div class="swiper-slide flex-shrink-0 relative">
    <img src="{{ themeImage('team/quote.png') }}" alt="quote" class="md:absolute left-0 top-0 mb-4">
    <div class="md:pl-24">
        <h3 class="leading-tight font-book text-white mb-4 text-xl lg:text-3xl header-text line-clamp-5">{!! $testimonial->content !!}</h3>
        <span class="leading-loose tracking-tight font-light text-white">{{ $testimonial->name }}, {{ $testimonial->company_name }}</span>
    </div>
</div>
