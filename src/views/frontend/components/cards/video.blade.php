@php
    $embedUrl = $video->getEmbedUrl();
@endphp

<!-- Single -->
<div class="mb-7 lg:mb-0 relative">
    <iframe loading="lazy" class="w-full min-h-58" style="width: 100%" src="{{ $embedUrl }}" title="Video Player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    <a href="#" class="absolute bottom-4 left-4 z-20 block">
        <h3 class="text-2xl  font-medium text-white">{{ $video->title }}</h3>
    </a>
</div>
