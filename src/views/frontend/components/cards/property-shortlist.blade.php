@php
    $splitMapMode = $splitMapMode ?? false;

    $class = $data = '';
    if (!isset($extraCss)) {
        $extraCss = '';
    }
    if ($splitMapMode) {
        $class = 'map-listing cursor-pointer';
        $data =  'data-zoom="12" data-latitude="'.$property->location->latitude.'" data-longitude="'.$property->location->longitude.'"';
    } else {
        $data =  'data-gallery_images="'.propertyImagesString($property).'"';
    }
@endphp

<div class="rounded-lg property-listing inline-listing-gallery {{ $class }} {{ $extraCss }}" {{ $data }}>
    <div class="relative min-h-76 h-76 max-h-76 overflow-hidden">
        @if ($property->images->first() === null)
            <div class="bg-gray-200 text-center">
                <span class="">No Image Available</span>
            </div>
        @else
            @if(hasFeature(\App\Models\TenantFeature::FEATURE_LISTINGS_INLINE_GALLERY) && !$splitMapMode && $property->images->count() > 1)
                <div class="inline-gallery-control hidden previous-image p-4 absolute left-0 top-1/2 -mt-8 text-white cursor-pointer" data-target="property-{{ $property->uuid }}">
                    <img class="inline-block svg-inject text-white fill-current stroke-current h-6 p-0 m-0" src="{{ themeImage('icons/left-carat.svg') }}" loading="lazy">
                </div>
                <div class="inline-gallery-control hidden next-image p-4 absolute right-0 top-1/2 -mt-8 text-white cursor-pointer" data-target="property-{{ $property->uuid }}">
                    <img class="inline-block svg-inject text-white fill-current stroke-current h-6 p-0 m-0" src="{{ themeImage('icons/right-carat.svg') }}" loading="lazy">
                </div>
                <a href="{{ localeUrl('/property/'.$property->url_key) }}" title="{{ $property->displayName() }}">
                    <img id="property-{{ $property->uuid }}" class="property-card-image w-full h-full rounded-t-2xl" src="{{ getPropertyImage($property->images->first()->filepath) }}" alt="img" loading="lazy">
                </a>
            @else
                <img id="property-{{ $property->uuid }}" class="property-card-image w-full h-full rounded-t-2xl" src="{{ getPropertyImage($property->images->first()->filepath) }}" alt="img" loading="lazy">
            @endif

        @endif
    </div>
    <div class="relative px-5 lg:px-7 md:py-7 pt-4 pb-7 shadow-md rounded-lg">

        @if(hasFeature(\App\Models\TenantFeature::FEATURE_SHORTLIST_SYSTEM))
            @if (user() === null)
                @php
                    $dataAttribs = 'data-target="preauth-modal"';
                    $imgClass = ''
                @endphp
            @else
                @php
                    // this property is in the shortlist
                    $dataAttribs = 'data-on-class="primary-text fill-current stroke-current" data-property="'.$property->uuid.'"';
                    $imgClass = 'primary-text fill-current stroke-current'
                @endphp
            @endif
            <div class="inline-block absolute -top-3 right-4">
                <a href="{{ localeUrl('/property/'.$property->url_key.'/toggle-shortlist') }}" class="bg-white rounded-full p-4 h-8 w-8 shadow-lg"  title="{{ trans('shortlist.save') }}" {!! $dataAttribs !!}>
                    <img src="{{ themeImage('search/heart.svg') }}" class="svg-inject {{ $imgClass }} inline-block" alt="love" loading="lazy">
                </a>
            </div>
        @endif

        <div>
            <span class="text-2xl leading-loose font-bold text-primary uppercase block">
                {!! $property->displayPrice() !!}
            </span>
            <a class="text-lg text-primary m-0 p-0 h-14 overflow-hidden block" href="{{ localeUrl('/property/'.$property->url_key) }}">{{ $property->displayName() }}</a>
            <span class="text-base text-gray-600">{{ $property->location->displayAddress() }}</span>
        </div>

        @include(themeViewPath('frontend.components.property.bed-bath-info'))

        <a href="{{ localeUrl('/property/'.$property->url_key) }}" class="text-sm md:text-base text-center tracking-wide font-bold uppercase text-white rounded-3xl shadow max-w-xs px-5 py-2 inline-block mt-6 transition-all cta cta-text">
            {{ trans('generic.view_details') }}
        </a>
    </div>

</div>
