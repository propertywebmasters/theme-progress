 <section class="bg-white mb-12 ">
    <div class="container mx-auto px-4 ">
        <div class="bg-white grid grid-cols-1 lg:grid-cols-2 gap-4 border">
            <div class="py-14 px-7 lg:px-14">
                <h3 class="text-xl lg:text-3xl leading-normal text-primary mb-9 heading-text">{{ $branch->name }}</h3>
                <div>
                    <div class="flex items-baseline">
                        <img src="{{ themeImage('contact/map-marker-alt.svg') }}" class="svg-inject text-gray-600 w-6" alt="marker" loading="lazy">
                        <address class="ml-4 text-base leading-tight tracking-tight inline-block not-italic text-left">{{ $branch->displayAddress(true) }}</address>
                    </div>
                    <div class="flex py-4">
                        <img src="{{ themeImage('contact/email2.svg') }}" class="svg-inject text-gray-600 w-6" alt="marker" loading="lazy">
                        <a href="mailto:{{ $branch->email }}" class="ml-4 text-base leading-tight tracking-tight inline-block">{{ $branch->email }}</a>
                    </div>
                    <div class="flex">
                        <img src="{{ themeImage('contact/phone-alt.svg') }}" class="svg-inject text-gray-600 w-6" alt="phone" loading="lazy">
                        <a href="tel:{{ $branch->tel }}" class="ml-4 text-base leading-tight tracking-tight inline-block">{{ $branch->tel }}</a>
                    </div>
                    <a href="mailto:{{ $branch->email }}" class="text-sm text-center leading-tight text-white px-4 sm:px-9 rounded-full right-1 top-1 hover:bg-hover transition-all inline-block py-3 mt-9 cta">
                        {{ trans('contact.contact_branch') }}
                    </a>
                </div>
            </div>
            <div class="mapouter relative text-right w-full h-80 lg:h-full">
                <iframe class="w-full h-full" id="gmap_canvas" src="https://maps.google.com/maps?q={{urlencode($branch->displayAddress(true))}}&t=&z=15&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
            </div>
        </div>
    </div>
</section>
