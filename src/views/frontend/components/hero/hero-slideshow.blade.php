{{-- loop this --}}
<div id="home-slides" class="s-slider swiper mySwiperHero overflow-hidden md:p-0 z-1 list-none relative z-10 h-full" data-speed="12000">
    <div class="swiper-wrapper flex z-1 relative w-full transition-transform box-content pb-8">
        @php $i = 1 @endphp
        @foreach ($slides as $slide)
            @php
                $displayClass = $i === 1 ? '' : ' hidden';
                $active = $i === 1 ? 'active' : '';
                $tag = $i === 1 ? 'h1' : 'h2';

                $bottom = 'bottom-72';

                if ($reviewServices->count()) {
                    $bottom = 'bottom-112';
                }

                $bottom .= ' lg:bottom-48'
            @endphp

            <div class="swiper-slide flex-shrink-0 relative" style="width: 100%;">
                <div class="relative center-cover-bg bg-lazy-load" data-style="background: linear-gradient(rgba(0, 0, 0, 0.3), rgba(0, 0, 0, 0.3)), url('{{ assetPath($slide->image) }}')">
                    <div class="absolute z-10 container mx-auto px-4 lg:mt-9 left-0 right-0 lg:pt-0 {{ $bottom }}">
                        <div class="mx-auto">
                            <{{$tag}} class="text-3xl md:text-5xl header-text text-white text-left">{!! $slide->title !!}</{{$tag}}>
                        </div>
                    </div>
                </div>
            </div>

            @php $i++ @endphp
        @endforeach
    </div>
</div>
