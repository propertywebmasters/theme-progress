<div class="relative">

    <div class="container mx-auto px-4 relative hero-video-title-container">
        <div class="absolute z-10 container mx-auto px-4 mt-9 lg:bottom-48 left-0 pt-14 lg:pt-0">
            <h1 class="max-w-screen-lg mx-auto lg:pl-5 xl:text-16 lg:text-5xl text-4xl header-text text-white text-left">
                {!! $video->title !!}
            </h1>
        </div>
    </div>

    <div class="absolute w-full inset-0">

        @if($video->provider === 'internal')
            <video  class="min-h-screen block w-full transform scale-500 md:scale-300 lg:scale-250 xl:scale-175" style="filter: blur(20px); " muted autoplay>
                <source src="{{ $video->getEmbedUrl() }}" type="video/mp4" />
                Your browser does not support the video tag.
            </video>
            <video  class="min-h-screen block w-full transform" muted autoplay>
                <source src="{{ $video->getEmbedUrl() }}" type="video/mp4" />
                Your browser does not support the video tag.
            </video>
        @else
            <iframe loading="lazy" class="w-full h-full block transform scale-600 sm:scale-400 md:scale-200" src="{{ $video->getEmbedUrl() }}" title="Video Player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        @endif

    </div>

    <div class="absolute video-overlay w-full h-full min-h-screen inset-0 bg-black" @if($video->overlay_opacity) style="opacity: {{ $video->overlay_opacity }}" @else style="opacity: 0;" @endif></div>
</div>
