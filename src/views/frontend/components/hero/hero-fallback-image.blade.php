<div class="relative min-h-screen">
    <div class="absolute left-0 top-0 w-full h-full z-20 bg-black opacity-50 video-overlay" @if($video->overlay_opacity) style="opacity: {{ $video->overlay_opacity }}" @else style="opacity: 0;" @endif></div>

    <div class="container mx-auto px-4 relative z-20 hero-video-title-container">
        <div class="absolute z-10 container mx-auto px-4 mt-9 lg:bottom-48 left-0 pt-14 lg:pt-0">
            <h1 class="max-w-screen-lg mx-auto lg:pl-5 xl:text-16 lg:text-5xl text-4xl header-text text-white text-left">
                {!! $video->title !!}
            </h1>
        </div>
    </div>

    <div class="absolute z-10 w-full top-0">
        <div style="background-repeat: no-repeat; background-image: url('{{ assetPath($video->fallback_image) }}')" class="center-cover-bg min-h-screen w-full"></div>
    </div>
</div>
