<!-- Banner -->
<div id="home-hero-wrapper" class="relative bg-cover bg-center bg-lazy-load">

    <section id="home-hero" class="relative h-full overflow-hidden">
        @if ($video)
            @if ($video->fallback_image_enabled)
                <div class="hidden min-h-screen hero-fallback-image-container">
                    @include(themeViewPath('frontend.components.hero.hero-fallback-image'))
                </div>

                <div class="hidden hero-video-container">
                    @include(themeViewPath('frontend.components.hero.hero-video'))
                </div>
            @else
                @include(themeViewPath('frontend.components.hero.hero-video'))
            @endif
        @else
            @include(themeViewPath('frontend.components.hero.hero-slideshow'))
        @endif

    </section>

    @include(themeViewPath('frontend.components.home-search'))

    @php
        $bottom = 'bottom-66';

        if ($reviewServices->count()) {
            $bottom = 'bottom-104';
        }

        $bottom .= ' lg:bottom-40';
    @endphp

    @if($slides->count() > 1)
        <div class="container max-w-screen-lg absolute z-20 inset-x-1/2 transform -translate-x-1/2 lg:pr-0 pr-8 swiper-pagination-hero-container {{ $bottom }}">
            <div class="flex justify-end">
                <div class="swiper-pagination-hero max-w-screen"></div>
            </div>
        </div>
    @endif
</div>
