@php
    $dataList = app()->make(App\Services\Search\Contracts\SearchServiceInterface::class)->dataListLocations();
    $locationValue = $searchRequest->request->get('location') !== null ? locationUrlStringToText($searchRequest->request->get('location')) : '';
@endphp
<div class="relative location-datalist-container">
    {{-- <input class="block w-full focus:outline-none h-10 bg-white text-sm border border-gray-400 py-1 px-2 location-datalist-input" autocomplete="off" role="combobox" list="" name="location" placeholder="{{ trans('placeholder.search_location') }}" value="{{ $locationValue }}" style="border-radius: 5px !important;"> --}}

    @include(themeViewPath('frontend.components.location-search-input'), [
        'datalist' => true, 
        'additionalClasses' => 'h-10 border border-gray-400 py-1 px-2', 
        'value' => $locationValue,
        'styles' => 'border-radius: 5px !important;',
    ])

    <datalist id="browsers" role="listbox" class="shadow absolute w-full text-xs bg-gray-100 top-12 left-0" style="z-index: 999;">
        <div class="text-sm overflow-y-scroll overflow-x-hidden max-h-44 md:max-h-50">
            @php $i = 1 @endphp
            @foreach($dataList as $text => $value)
                <option class="text-xs bg-gray-100 text-black cursor-pointer hover:text-white hover:bg-white py-1.5 px-2.5 md:py-2 md:px-4 @if($i === 1) pt-2 @endif" data-value="{{ $value }}">{{ $text }}</option>
                @php $i++ @endphp
            @endforeach
        </div>
    </datalist>
    <input type="hidden" name="location_url">
</div>
