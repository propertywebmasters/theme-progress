@if ($popularSearches->count() > 0)
    <section id="popular-searches" class="bg-whiter py-8 md:py-16">
        <div class="container mx-auto px-4">

            <h3 class="text-3xl text-center header-text mb-4 md:mb-9">{{ trans('search.popular_searches') }}</h3>

            <div class="grid lg:grid-cols-4 sm:grid-cols-2 gap-4">
                @foreach($popularSearches->slice(0, 4) as $search)
                    <div class="relative location-single min-h-112 max-h-112" data-url="{{ localeUrl($search->url) }}">
                        <img class="w-full h-full object-cover object-center" src="{{ assetPath($search->image) }}" alt="location">

                        <h3 class="text-3xl text-white absolute bottom-20 text-center w-full z-20 transition-all duration-500 header-text">{!! $search->title !!}</h3>
                        <div class="absolute bottom-20 z-30 w-full left-0 text-center opacity-0 duration-500">
                            <a class="popular-search-button text-sm text-center tracking-wide rounded-3xl border border-white w-44 max-w-xs inline-block ml-4 py-3 px-8 text-white hover-primary-bg duration-500 transition-all" href="{{ localeUrl($search->url) }}">
                                {{ trans('generic.view') }}
                            </a>
                        </div>
                    </div>
                @endforeach

                @if ($popularSearches->count() > 4)
                    <div class="text-center">
                        <a class="text-sm text-center tracking-wide nav-text rounded-3xl border max-w-xs inline-block ml-4 py-3 px-16 transition-all cta hover:bg-hover hover:text-white duration-500 mx-auto"
                        href="#">{{ trans('generic.view') }}</a>
                    </div>
                @endif
            </div>
        </div>
    </section>
@endif
