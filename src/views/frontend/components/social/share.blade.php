@php
use App\Models\SiteSetting;
use App\Services\SiteSetting\Contracts\SiteSettingServiceInterface;

$useWhatsApp = true;

$twitterUrl = 'https://www.twitter.com/intent/tweet?url='.urlencode(url()->current());
$facebookUrl = 'https://www.facebook.com/sharer/sharer.php?u='.urlencode(url()->current());

$settingService = app()->make(SiteSettingServiceInterface::class);
$siteName = $settingService->retrieve(SiteSetting::SETTING_SITE_NAME['key']);

if (isset($property)) {
    $whatsAppUrl = 'https://api.whatsapp.com/send?text='.urlencode($property->displayName().' - '.url()->current());
    $emailSubject = 'Check out this property on '.$siteName;
    $emailBody = 'Check out this property here - '.url()->to('property/'.$property->url_key);
    $emailUrl = 'mailto:?body='.$emailBody.'&subject='.$emailSubject;
} elseif(isset($article)) {
    $whatsAppUrl = 'https://api.whatsapp.com/send?text='.urlencode($article->title.' - '.url()->current());
    $emailSubject = 'Check out this property on '.$siteName;
    $emailBody = 'Check out this news article here - '.url()->to('news/'.$article->url_key);
}

$emailUrl = 'mailto:?body='.$emailBody.'&subject='.$emailSubject
@endphp

<div class="flex justify-center px-0 md:px-5 py-3 md:py-0 text-center md:text-left mt-4">
    <a href="{{ $twitterUrl }}" class="pr-4 primary-text" target="_BlANK">
        <img class="svg-inject h-6 w-6 primary-text fill-current" src="{{ themeImage('twitter.svg') }}" alt="twitter"  loading="lazy">
    </a>
    <a href="{{ $facebookUrl }}" class="pr-4 primary-text" target="_BlANK">
        <img class="svg-inject h-6 w-6 primary-text fill-current" src="{{ themeImage('facebook.svg') }}" alt="facebook" loading="lazy">
    </a>
    <a href="{{ $whatsAppUrl }}" class="pr-4 primary-text" target="_BlANK">
        <img class="svg-inject h-6 w-6 primary-text fill-current" src="{{ themeImage('whatsapp.svg') }}" alt="whatsapp" loading="lazy">
    </a>
    @if(isset($property))
        <a class="modal-button primary-text" href="javascript:" data-target="share-this-property-modal">
            <img class="svg-inject h-6 w-6 primary-text fill-current" src="{{ themeImage('contact/email2.svg') }}" alt="email" loading="lazy">
        </a>
    @endif
</div>
