@php
use App\Models\SiteSetting;
use App\Services\SiteSetting\Contracts\SiteSettingServiceInterface;

$useWhatsApp = true;

$twitterUrl = 'https://www.twitter.com/intent/tweet?url='.urlencode(url()->current());
$facebookUrl = 'https://www.facebook.com/sharer/sharer.php?u='.urlencode(url()->current());

$settingService = app()->make(SiteSettingServiceInterface::class);
$siteName = $settingService->retrieve(SiteSetting::SETTING_SITE_NAME['key']);

if (isset($property)) {
    $whatsAppUrl = 'https://api.whatsapp.com/send?text='.urlencode($property->displayName().' - '.url()->current());
    $emailSubject = 'Check out this property on '.$siteName;
    $emailBody = 'Check out this property here - '.url()->to('property/'.$property->url_key);
    $emailUrl = 'mailto:?body='.$emailBody.'&subject='.$emailSubject;
} elseif(isset($article)) {
    $whatsAppUrl = 'https://api.whatsapp.com/send?text='.urlencode($article->title.' - '.url()->current());
    $emailSubject = 'Check out this property on '.$siteName;
    $emailBody = 'Check out this news article here - '.url()->to('news/'.$article->url_key);
}

$emailUrl = 'mailto:?body='.$emailBody.'&subject='.$emailSubject
@endphp

<div class="border-l pl-4">
    <span class="text-xs block pb-4">{{ trans('Share') }}</span>
    <div class="flex flex-col text-center w-8">
        <a href="{{ $twitterUrl }}" class="primary-text p-1 rounded-full primary-border mb-2" target="_BlANK">
            <img class="svg-inject h-5 w-5 primary-text fill-current" src="{{ themeImage('twitter.svg') }}" alt="twitter"  loading="lazy">
        </a>
        <a href="{{ $facebookUrl }}" class="primary-text p-1 rounded-full primary-border mb-2" target="_BlANK">
            <img class="svg-inject h-5 w-5 primary-text fill-current" src="{{ themeImage('facebook.svg') }}" alt="facebook" loading="lazy">
        </a>
        <a href="{{ $whatsAppUrl }}" class="primary-text p-1 rounded-full primary-border" target="_BlANK">
            <img class="svg-inject h-5 w-5 primary-text fill-current" src="{{ themeImage('whatsapp.svg') }}" alt="whatsapp" loading="lazy">
        </a>
    </div>
</div>
