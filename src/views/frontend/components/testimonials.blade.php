@if ($testimonials->count() > 0)
<section id="testimonials" class="py-24 relative bg-lazy-load center-cover-bg" data-style="{{ backgroundCSSImage('home.testimonials') }}">
    <div class="container mx-auto px-4">
        <div
            class="s-slider swiper mySwiperTestimonial overflow-hidden md:p-0 z-1 list-none relative z-10 max-w-5xl mx-auto">
            <div class="swiper-wrapper flex z-1 relative w-full transition-transform box-content pb-8">
                @foreach($testimonials as $testimonial)
                    @include(themeViewPath('frontend.components.cards.testimonial'))
                @endforeach
            </div>
            <div class="swiper-paginationT md:ml-24"></div><!-- /.swiper-paginationT -->
        </div>
    </div>
</section>
@endif
