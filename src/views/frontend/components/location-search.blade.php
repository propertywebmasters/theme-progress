@php
    if (!isset($refinedSearch)) {
        $refinedSearch = false;
    }

    if (!isset($value)) {
        $value = false;
    }

    if (!isset($additionalClasses)) {
        $additionalClasses = '';
    }

    if (!isset($mobile)) {
        $mobile = false;
    }

    if (!isset($value)) {
        $value = '';
    }
@endphp

@if(isset($searchControls->location) && $searchControls->location)
    @if(hasFeature(\App\Models\TenantFeature::FEATURE_USE_LOCATION_DATALIST))
        @if ($mobile)
            <div class="search-filter-mobile">
                @include(themeViewPath('frontend.components.location-datalist-refine-mobile'))
            </div>
        @elseif ($refinedSearch)
            @include(themeViewPath('frontend.components.location-datalist-refine'))
        @else
            <div class="bg-transparent order-1 lg:order-2 {{ $colSpan }}" style="flex-grow: 1;">
                @include(themeViewPath('frontend.components.location-datalist'))
            </div>
        @endif

    @elseif ($mobile)
        @include(themeViewPath('frontend.components.location-search-input'), ['additionalClasses' => $additionalClasses, 'value' => $value])
        <input type="hidden" name="location_url" value="">
    @elseif ($refinedSearch)
        <div class="relative w-180">
            <div id="autocomplete-results" class="click-outside absolute -left-2 top-8 z-30" data-classes="py-1 px-4 text-sm border bg-white hover:bg-gray-200 text-gray-800 text-left cursor-pointer">
                <ul class="search-results-container"></ul>
            </div>
        </div>

        @include(themeViewPath('frontend.components.location-search-input'), ['additionalClasses' => $additionalClasses])

        <input type="hidden" name="location_url" value="">
    @else
        <div class="bg-transparent order-1 lg:order-2 {{ $colSpan }}" style="flex-grow: 1;">
            <div class="bg-white px-4 py-3 rounded-full {{ $rounded }} lg:rounded-r-none my-2 lg:my-0">
                <div class="relative w-180">
                    <div id="autocomplete-results" class="click-outside absolute bottom-3 z-30" data-classes="py-1 px-4 text-sm border bg-white hover:bg-gray-200 text-gray-800 text-left cursor-pointer">
                        <ul class="search-results-container"></ul>
                    </div>
                </div>

                @include(themeViewPath('frontend.components.location-search-input'), ['additionalClasses' => $additionalClasses])

                <input type="hidden" name="location_url" value="">
            </div>
        </div>
    @endif
@endif
