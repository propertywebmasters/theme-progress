@php
$title = $customHeader ?? trans('header.latest_news')
@endphp

@if ($articles->count() > 0)
    <section class="bg-white py-8 md:py-16">
        <div class="container mx-auto px-4 relative">
            <h3 class="text-3xl text-center header-text mb-9">{{ $title }}</h3>
            <div class="md:absolute right-4 lg:top-0 md:-top-3 mt-3 lg:mt-0 text-center md:text-right mb-8 md:mb-0 hidden md:block">
                <a class="primary-text primary-border text-sm text-center tracking-wide rounded-3xl border max-w-xs ml-4 py-2.5 px-8 transition-all inline-block duration-500" href="{{ localeUrl('/news') }}">{{ trans('header.view_archive') }}</a>
            </div>

            <div class="grid lg:grid-cols-2 gap-4 pb-4">
                @php
                    $article = $articles->first()
                @endphp

                <div class="relative">
                    <img class="w-full h-96 sm:h-122 md:h-142 lg:h-128 xl:h-132 object-cover" src="{{ assetPath($article->image) }}" loading="lazy">

                    <div class="absolute left-0 top-0 w-full h-full opacity-50 bg-black"></div>
                    <a href="{{ localeUrl('/news/'.$article->url_key) }}" class="block absolute left-0 bottom-0 lg:p-8 p-4 z-20">
                            <span class="text-base leading-loose tracking-wide text-white opacity-80 uppercase">
                                {{ $article->created_at->formatLocalized('%d %b %Y') }}
                            </span>
                            <h3 class="text-2xl  font-medium text-white hidden sm:block">{!! \Illuminate\Support\Str::limit(strip_tags($article->content, 360)) !!}</h3>
                            <h3 class="text-2xl  font-medium text-white sm:hidden">{{ $article->title }}</h3>
                    </a>
                </div>

                @foreach ($articles->slice(1, 3) as $i => $article)
                    <div class="relative block sm:hidden">
                        <img class="w-full h-96 sm:h-122 md:h-142 lg:h-128 xl:h-132 object-cover" src="{{ assetPath($article->image) }}" loading="lazy">
    
                        <div class="absolute left-0 top-0 w-full h-full opacity-50 bg-black"></div>
                        <a href="{{ localeUrl('/news/'.$article->url_key) }}" class="block absolute left-0 bottom-0 lg:p-8 p-4 z-20">
                                <span class="text-base leading-loose tracking-wide text-white opacity-80 uppercase">
                                    {{ $article->created_at->formatLocalized('%d %b %Y') }}
                                </span>
                            <h3 class="text-2xl  font-medium text-white hidden sm:block">{!! \Illuminate\Support\Str::limit(strip_tags($article->content, 360)) !!}</h3>
                            <h3 class="text-2xl  font-medium text-white sm:hidden">{{ $article->title }}</h3>
                        </a>
                    </div>
                @endforeach

                <div class="hidden sm:flex flex-col justify-between">
                    @foreach ($articles->slice(1, 3) as $i => $article)
                        @php
                            $css = [];
                            $css[] = $i > 3 ? 'xl:hidden' : '';
                            $css[] = $loop->last ? '' : 'mb-4';
                            $css = implode(' ', $css)
                        @endphp

                        @include(themeViewPath('frontend.components.cards.article'), ['article' => $article, 'css' => $css])
                    @endforeach
                </div>

            </div>

            <div class="md:absolute right-4 lg:top-0 md:-top-3 mt-6 sm:mt-8 text-center md:text-right mb-8 md:mb-0 block md:hidden">
                <a class="primary-text primary-border text-sm text-center tracking-wide rounded-3xl border max-w-xs ml-4 py-2.5 px-8 transition-all inline-block duration-500" href="{{ localeUrl('/news') }}">{{ trans('header.view_archive') }}</a>
            </div>
        </div>
    </section>
@endif
