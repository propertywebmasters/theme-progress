<section>
    <div class="container mx-auto px-4 md:pt-4">

        <div class="flex flex-col z-10 mb-2 md:pt-3">
            <div class="flex flex-col lg:flex-row w-full justify-between items-center py-4">
                <div class="w-full lg:w-auto self-start sm:self-center flex md:flex-row flex-col md:flex-none  sm:flex-wrap justify-between items-center mb-4 lg:mb-0">
                    <div class="inline-block lg:w-auto md:w-full">
                        <h1 class="leading-loose tracking-tight md:text-xl text-lg ">{!! $breadcrumbResultsString  !!} ({{ number_format($properties->total()) }})</h1>
                    </div>
                </div>

                <div class="w-full lg:w-auto md:pt-4 lg:pt-0">
                    @include(themeViewPath('frontend.components.listings.view-modes'))
                    @include(themeViewPath('frontend.components.listings.search-options'))
                </div>
            </div>
        </div>

        <hr class="pb-8">

    </div>
</section>
