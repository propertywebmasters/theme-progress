<section class="bg-whiter py-8 md:py-16">
    <div class="container mx-auto px-4">
        <div class="mx-auto max-w-4xl text-center">
            <h3 class="text-3xl text-center header-text mb-8 md:mb-12">{!! translatableContent('home', 'about-title') !!}</h3>
            <p class="leading-normal text-center tracking-tight font-light">
                {!! translatableContent('home', 'about-text') !!}
            </p>
            <div class="text-center mt-10 md:mt-14">
                <a id="about-find-out-more" class="text-gray-800 border-gray-800 text-sm text-center tracking-wide rounded-full border max-w-xs inline-block ml-4 py-2.5 px-12 transition-all hover:text-white duration-500 transition-all hover:primary-bg"
                   href="{{ localeUrl('/about') }}">{{ trans('generic.find_out_more') }}</a>
            </div>
        </div>
    </div>
</section>
