@php
    use App\Models\TenantFeature;$searchControls = searchControlOptions(config('platform-cache.search_controls_enabled'));
    $bedroomFilterMode = isset($searchControls->bedrooms) && $searchControls->bedrooms ? 'single' : null;
    $bedroomFilterMode = isset($searchControls->bedrooms_as_range) && $searchControls->bedrooms_as_range ? 'multi' : $bedroomFilterMode;

    $hasShortTermRentals = hasShortTermRentals();
    $hasLongTermRentals = hasLongTermRentals();
    $hasStudentRentals = isset($searchControls->student_rentals) && $searchControls->student_rentals;
    $hasHolidayRentals = isset($searchControls->holiday_rentals) && $searchControls->holiday_rentals;
    $hiddenRentalInput = $searchControls->tender_type === 'rental_only' && (!$hasShortTermRentals && !$hasLongTermRentals && !$hasStudentRentals && !$hasHolidayRentals);
@endphp

<div class="py-6 absolute bottom-0 w-full z-30 bg-black bg-opacity-80">
    <div class="container mx-auto px-4">

        @include(themeViewPath('frontend.components.full-screen-filter'))

        <form id="home-search" action="{{ localeUrl('/search') }}" method="post" enctype="application/x-www-form-urlencoded">

            <div class="w-full grid grid-cols-7 mx-auto">
                <div class="lg:mb-0 mb-3 text-center lg:text-left pr-4 hidden lg:flex items-center col-span-1">
                    <span style="text-overflow: ellipsis;"
                          class="text-lg text-white tracking-tightest whitespace-nowrap overflow-hidden">{{ trans('search.start_your_search') }}</span>
                </div>

                <div class="col-span-7 lg:col-span-5 grid grid-cols-2 lg:grid-cols-5 rounded-2xl">
                    @if(isset($searchControls->tender_type) && $searchControls->tender_type === 'both')
                        <div class="bg-transparent col-span-1 order-2 lg:order-1">
                            <div class="bg-white px-4 py-3 rounded-full lg:rounded-r-none my-2 lg:my-0 mr-1 lg:mr-0">
                                <select id="search_type" name="type" class="w-full bg-white block focus:outline-none -ml-1 text-sm appearance-none select-carat">
                                    <option value="sale">{{ trans('label.sale') }}</option>
                                    <option value="rental">{{ trans('label.rental') }}</option>
                                    @if($hasShortTermRentals)
                                        <option value="short_term_rental">{{ trans('label.short_term_rental') }}</option>
                                    @endif
                                    @if($hasLongTermRentals)
                                        <option value="long_term_rental">{{ trans('label.long_term_rental') }}</option>
                                    @endif
                                    @if($hasStudentRentals)
                                        <option value="student_rental">{{ trans('label.student_rental') }}</option>
                                    @endif
                                    @if($hasHolidayRentals)
                                        <option value="holiday_rental">{{ trans('label.holiday_rental') }}</option>
                                    @endif

                                </select>
                            </div>
                        </div>
                    @elseif(isset($searchControls->tender_type) && ($searchControls->tender_type === 'sale_only' || $searchControls->tender_type === 'rental_only'))
                        @if($searchControls->tender_type === 'sale_only')
                            <input id="search_type" name="type" type="hidden" value="sale">
                        @elseif($hiddenRentalInput)
                            <input id="search_type" name="type" type="hidden" value="rental">
                        @elseif($searchControls->tender_type === 'rental_only')
                            <div class="bg-white px-4 py-3 rounded-full lg:rounded-r-none my-2 lg:my-0 col-span-1 order-2 lg:order-1">
                                <select id="search_type" name="type" class="w-full block focus:outline-none -ml-1 text-sm appearance-none select-carat">
                                    <option value="rental">{{ trans('label.rental') }}</option>
                                    @if($hasShortTermRentals)
                                        <option value="short_term_rental">{{ trans('label.short_term_rental') }}</option>
                                    @endif
                                    @if($hasLongTermRentals)
                                        <option value="long_term_rental">{{ trans('label.long_term_rental') }}</option>
                                    @endif
                                    @if($hasStudentRentals)
                                        <option value="student_rental">{{ trans('label.student_rental') }}</option>
                                    @endif
                                    @if($hasHolidayRentals)
                                        <option value="holiday_rental">{{ trans('label.holiday_rental') }}</option>
                                    @endif
                                </select>
                            </div>
                        @endif
                    @endif

                    {{-- @if(isset($searchControls->location) && $searchControls->location)
                        @php
                            $rounded = '';
                            $border = '';

                            if (isset($searchControls->tender_type) && $searchControls->tender_type !== 'sale_only') {
                                $rounded = 'lg:rounded-l-none';
                                $border = 'lg:border-l';
                                $colSpan = 'col-span-3';
                            }

                            if (isset($searchControls->tender_type) && $searchControls->tender_type === 'sale_only') {
                                $colSpan = 'col-span-4';
                            }
                        @endphp

                        @if(hasFeature(\App\Models\TenantFeature::FEATURE_USE_LOCATION_DATALIST))
                            <div class="bg-transparent order-1 lg:order-2 {{ $colSpan }}" style="flex-grow: 1;">
                                @include(themeViewPath('frontend.components.location-datalist'))
                            </div>
                        @else
                            <div class="bg-transparent order-1 lg:order-2 {{ $colSpan }}" style="flex-grow: 1;">
                                <div class="bg-white px-4 py-3 rounded-full {{ $rounded }} lg:rounded-r-none my-2 lg:my-0">
                                    <div class="relative w-180">
                                        <div id="autocomplete-results" class="click-outside absolute bottom-3 z-30" data-classes="py-1 px-4 text-sm border bg-white hover:bg-gray-200 text-gray-800 text-left cursor-pointer">
                                            <ul class="search-results-container"></ul>
                                        </div>
                                    </div>
                                    <input id="search_location" name="location"
                                           class="autocomplete-location w-full block focus:outline-none -ml-1 {{ $border }} lg:pl-2 border-l-0 pl-0 text-sm bg-transparent"
                                           placeholder="{{ trans('placeholder.search_location') }}" autocomplete="off" data-search-results-container="search-results-container">
                                    <input type="hidden" name="location_url" value="">
                                </div>
                            </div>
                        @endif
                    @endif --}}

                    @php
                        $rounded = '';
                        $additionalClasses = 'bg-transparent -ml-1 secondary-text lg:pl-2 border-l-0 pl-0';

                        if (isset($searchControls->tender_type) && $searchControls->tender_type === 'both') {
                            $rounded = 'lg:rounded-l-none';
                            $additionalClasses .= ' lg:border-l';
                            $colSpan = 'col-span-3';
                        }

                        if (isset($searchControls->tender_type) && ($searchControls->tender_type === 'sale_only' || $hiddenRentalInput)) {
                            $colSpan = 'col-span-4';
                        }
                    @endphp

                    @include(themeViewPath('frontend.components.location-search'), ['colSpan' => $colSpan, 'additionalClasses' => $additionalClasses, 'rounded' => $rounded])

                    @php
                        if ((isset($searchControls->tender_type) && ($searchControls->tender_type === 'sale_only' || $hiddenRentalInput))) {
                            $colSpan = 'col-span-2 lg:col-span-1';
                        } else {
                            $colSpan = 'col-span-1';
                        }
                    @endphp

                    <div class="bg-transparent text-sm order-3 {{ $colSpan }}" style="flex-grow: 1;">
                        <div class="bg-white px-4 py-3 rounded-full lg:rounded-l-none my-2 lg:my-0 ml-1 lg:ml-0">
                            <span class="full-screen-filter-toggle tracking-tight lg:border-l lg:pl-2 border-l-0 cursor-pointer block text-sm">
                                <img src="{{ themeImage('filter.svg') }}" class="svg-inject h-4 inline-block pr-1" loading="lazy"> {{ trans('label.filters') }}
                            </span>
                        </div>
                    </div>
                </div>

                <div class="lg:ml-2 col-span-7 lg:col-span-1">
                    <input type="submit"
                           class="mt-2 lg:mt-0 py-2 xl:py-0 lg:rounded-full block text-base cta hover:text-white w-full h-full cursor-pointer hover:bg-hover hover:text-white hover:border-hover duration-500 rounded-3xl"
                           value="{{ trans('label.search') }}"/>
                </div>

            </div>

            @csrf

            <input type="hidden" name="locale" value="{{ app()->getLocale() }}">
            @if(hasFeature(TenantFeature::FEATURE_DEVELOPMENT_SEARCH_ON_HOMEPAGE))
                <input type="hidden" name="is_development" value="1">
            @endif

        </form>
    </div>
</div>
