<div class="container mx-auto py-12">
    <div class="flex md:justify-between flex-col md:flex-row">
        <div class="flex items-center px-0 md:px-5 py-3 md:py-0 text-center md:text-left ">
            <a href="#" class="pr-4 inline-block">
                <img src="assets/img/fb.svg" alt="social">
            </a>
            <a href="#" class="pr-4 inline-block">
                <img src="assets/img/tw.svg" alt="social">
            </a>
            <a href="#" class="pr-4 inline-block">
                <img src="assets/img/insta.svg" alt="social">
            </a>
            <a href="#" class="pr-4 inline-block">
                <img src="assets/img/li.svg" alt="social">
            </a>
            <a href="#" class="pr-4 inline-block">
                <img src="assets/img/youtube.svg" alt="social">
            </a>
        </div>
    </div>
</div>
