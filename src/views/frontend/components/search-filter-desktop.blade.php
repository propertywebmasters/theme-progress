<section class="py-6 tertiary-bg">
    <div class="container mx-auto px-4">
        @include(themeViewPath('frontend.components.full-screen-filter'))
        <form id="advanced-search" action="{{ localeUrl('/search') }}" method="post" enctype="application/x-www-form-urlencoded">
            <div class="w-full flex flex-wrap items-center justify-center">
                <div class="lg:w-44 w-full lg:mb-0 mb-3 text-center lg:text-right pr-4">
                    <span class="text-lg text-white tracking-tightest">{{ trans('search.start_your_search') }}</span>
                </div>
                <div class="flex lg:w-210 xl:w-180 w-full content-between flex-wrap">

                    @if(isset($searchControls->tender_type) && $searchControls->tender_type === 'both')
                        <div class="bg-white px-4 py-3 rounded-l-0 lg:rounded-l-full lg:w-36 w-full rounded-full lg:rounded-r-none my-2 lg:my-0" style="flex-grow: 1;">
                            <select id="search_type" name="type" class="w-full block focus:outline-none -ml-1 bg-transparent text-sm appearance-none select-carat">
                                <option value="sale" @if ($searchRequest->get('type') === 'sale') selected @endif>{{ trans('label.sale') }}</option>
                                <option value="rental" @if ($searchRequest->get('type') === 'rental' && $searchRequest->get('rental_length') === null) selected @endif>{{ trans('label.rental') }}</option>
                                @if($hasShortTermRentals)
                                    <option value="short_term_rental" @if ($searchRequest->get('type') === 'rental' && $searchRequest->get('rental_length') === \App\Models\Property::RENTAL_LENGTH_SHORT) selected @endif>{{ trans('label.short_term_rental') }}</option>
                                @endif
                                @if($hasLongTermRentals)
                                    <option value="long_term_rental" @if ($searchRequest->get('type') === 'rental' && $searchRequest->get('rental_length') === \App\Models\Property::RENTAL_LENGTH_LONG) selected @endif>{{ trans('label.long_term_rental') }}</option>
                                @endif
                                @if($hasStudentRentals)
                                    <option value="student_rental" @if ($searchRequest->get('type') === 'rental' && $searchRequest->get('is_student_rental')) selected @endif>{{ trans('label.student_rental') }}</option>
                                @endif
                                @if($hasHolidayRentals)
                                    <option value="holiday_rental" @if ($searchRequest->get('type') === 'rental' && $searchRequest->get('is_holiday_rental')) selected @endif>{{ trans('label.holiday_rental') }}</option>
                                @endif
                            </select>
                        </div>
                    @elseif(isset($searchControls->tender_type) && ($searchControls->tender_type === 'sale_only' || $searchControls->tender_type === 'rental_only'))
                        @if($searchControls->tender_type === 'sale_only')
                            <input id="search_type" name="type" type="hidden" value="sale">
                        @elseif($searchControls->tender_type === 'rental_only')
                            <div class="bg-white px-4 py-3 rounded-l-0 lg:rounded-l-full lg:w-36 w-full rounded-full lg:rounded-r-none my-2 lg:my-0" style="flex-grow: 1;">
                                <select id="search_type" name="type" class="w-full block focus:outline-none -ml-1 bg-transparent  text-sm appearance-none select-carat">
                                    <option value="rental" @if ($searchRequest->get('type') === 'rental' && $searchRequest->get('rental_length') === null) selected @endif>{{ trans('label.rental') }}</option>
                                    @if($hasShortTermRentals)
                                        <option value="short_term_rental" @if ($searchRequest->get('type') === 'rental' && $searchRequest->get('rental_length') === \App\Models\Property::RENTAL_LENGTH_SHORT) selected @endif>{{ trans('label.short_term_rental') }}</option>
                                    @endif
                                    @if($hasLongTermRentals)
                                        <option value="long_term_rental" @if ($searchRequest->get('type') === 'rental' && $searchRequest->get('rental_length') === \App\Models\Property::RENTAL_LENGTH_LONG) selected @endif>{{ trans('label.long_term_rental') }}</option>
                                    @endif
                                    @if($hasStudentRentals)
                                        <option value="student_rental" @if ($searchRequest->get('type') === 'rental' && $searchRequest->get('is_student_rental')) selected @endif>{{ trans('label.student_rental') }}</option>
                                    @endif
                                    @if($hasHolidayRentals)
                                        <option value="holiday_rental" @if ($searchRequest->get('type') === 'rental' && $searchRequest->get('is_holiday_rental')) selected @endif>{{ trans('label.holiday_rental') }}</option>
                                    @endif
                                </select>
                            </div>
                        @endif
                    @endif

                    @php
                        $rounded = '';

                        if (isset($searchControls->tender_type) && $searchControls->tender_type !== 'sale_only') {
                            $rounded = 'lg:rounded-l-none';
                        }
                    @endphp

                    <div class="bg-white px-4 py-3 lg:w-104 w-full rounded-full {{ $rounded }} lg:rounded-r-none my-2 lg:my-0" style="flex-grow: 1;">

                        @include(themeViewPath('frontend.components.location-search'), [
                            'additionalClasses' => '-ml-1 secondary-text lg:pl-2 border-l-0 pl-0',
                            'refinedSearch' => true,
                            'value' => $searchRequest->get('location'),
                        ])

                    </div>

                    <div class=" bg-white px-4 py-3 lg:rounded-r-full rounded-r-0 text-sm lg:w-40 w-full rounded-full lg:rounded-l-none my-2 lg:my-0" style="flex-grow: 1;">
                        <span class="full-screen-filter-toggle tracking-tight lg:pl-2 border-l-0 cursor-pointer block text-sm">
                                <img src="{{ themeImage('filter.svg') }}" class="svg-inject h-4 inline-block pr-1" loading="lazy"> {{ trans('label.filters') }}
                            </span>
                    </div>
                </div>

                <div class="lg:w-36 w-full mt-2 lg:mt-0 lg:ml-2">
                    <input type="submit"
                           class="py-3 lg:py-2 lg:rounded-full block text-lg cta text-white w-full h-full focus:outline-none cursor-pointer hover:bg-hover hover:text-white hover:border-hover duration-500 rounded-3xl"
                           value="{{ trans('label.search') }}">
                </div>
            </div>
            @if($searchRequest->get('is_development'))
                <input type="hidden" name="is_development" value="1">
            @endif

            @if(isset($searchRequest) && is_array($searchRequest->get('property_type')))
                @foreach($searchRequest->get('property_type') as $propType)
                    <input type="hidden" name="property_type[]" value="{{ $propType }}">
                @endforeach
            @endif

        </form>
    </div>
</section>
