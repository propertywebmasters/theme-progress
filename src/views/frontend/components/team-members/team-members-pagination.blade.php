@php
    if (!isset($extraClass)) {
        $extraClass = '';
    }
@endphp

<section class="pagination">
    <div class="{{ $extraClass }}">
        {!! $data->withQueryString()->links('pagination::tailwind-alt') !!}
    </div>
</section>
