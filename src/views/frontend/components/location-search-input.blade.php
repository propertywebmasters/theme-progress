@php
    if (!isset($datalist)) {
        $datalist = false;
    }

    if (!isset($additionalClasses)) {
        $additionalClasses = '';
    }

    if (!isset($value)) {
        $value = '';
    }

    if (!isset($styles)) {
        $styles = '';
    }
@endphp

<div class="relative">
    @if ($datalist)
        <input class="location-datalist-input w-full block focus:outline-none -ml-1 {{ $additionalClasses }} text-sm bg-transparent"  autocomplete="off" role="combobox" list="" id="location_datalist" name="location" placeholder="{{ trans('placeholder.search_location') }}" style="{{ $styles }}" value="{{ $value }}">
    @else
        <input id="search_location" name="location"
            class="autocomplete-location w-full block focus:outline-none text-sm {{ $additionalClasses }}"
            placeholder="{{ trans('placeholder.search_location') }}" autocomplete="off" data-search-results-container="search-results-container" value="{{ $value }}">
    @endif

    <div class="clear-search-button absolute top-1/2 right-2 transform -translate-y-1/2 cursor-pointer {{ $value ? '' : 'hidden' }}">
        <i class="w-4 h-4" data-feather="x"></i>
    </div>
</div>
