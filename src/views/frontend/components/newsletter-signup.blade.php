<section class="footer-section relative z-10">
    <div class="container mx-auto px-4 border-b">
        <form id="newsletter-signup" method="post" action="javascript:" enctype="application/x-www-form-urlencoded">
            @csrf

            <div class="py-10 rounded-lg flex flex-col lg:flex-row lg:justify-between items-center">
                <div class="mb-4 lg:mb-0 w-full">
                    <h3 class="text-2xl md:text-4xl leading-loose header-text mb-1 text-center lg:text-left">{{ trans('mailing-list.signup_to_our_newsletter') }}</h3>
                    <span class="block text-base leading-tight tracking-tight text-browngrey font-normal text-center lg:text-left">{!! trans('mailing-list.stay_upto_date_with_our_latest_news') !!}</span>
                </div>
                <div
                    class="max-w-2xl w-full relative mt-5 md:mt-0 flex lg:justify-end justify-center sm:flex-row flex-col">
                    <div class="flex sm:max-w-md max-w-full sm:pr-2 pr-0 mb-1 lg:mb-0 sm:flex-row flex-col">
                        <div class="bg-white rounded-full sm:rounded-l-full sm:rounded-r-none flex items-center py-2.5  mb-1 sm:mb-0">
                            <input name="newsletter_name" type="text" class="text-black text-sm w-full px-5 focus:border-0 focus:outline-none border-r-0 sm:border-r bg-transparent" placeholder="{{ trans('contact.full_name') }}" required>
                        </div>
                        <div class="bg-white rounded-full sm:rounded-r-full sm:rounded-l-none flex items-center py-2.5">
                            <input name="newsletter_email" type="email" class="text-black w-full px-4 focus:border-0 focus:outline-none bg-transparent text-sm" placeholder="{{ trans('contact.email_address') }}" required>
                        </div>
                    </div>
                    <button type="submit" class="text-center tracking-tight md:tracking-wide text-white cta px-5 sm:px-9 rounded-full h-11 min-w-36  hover:bg-hover transition-all duration-500">
                        {{ trans('mailing-list.signup') }}
                    </button>
                </div>
            </div>
        </form>
    </div>
</section>
