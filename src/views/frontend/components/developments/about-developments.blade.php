<section id="developments-about" class="bg-whiter">
    <div class="container mx-auto py-14 md:pb-24 px-4">
        <div class="w-full lg:w-4/5 lg:pl-4 pl-0 mt-10 lg:mt-0 text-center mx-auto">
            <h2 class="header-text text-2xl lg:text-3xl xl:text-5xl tracking-tight text-primary mb-4 lg:mb-9">{!! translatableContent('developments', 'about-title') !!}</h2>
            <p class="tracking-tight text-base leading-normal text-center">{!! translatableContent('developments', 'about-text') !!}</p>
        </div>
    </div>
</section>
