@if ($searchRequest->get('location') !== null)
    @if (user())
        <div class="inline-block px-0 ml-2">
            <a class="modal-button leading-normal text-sm hover:underline primary-text" href="javascript:" data-target="create-alert-modal">
                <img src="{{ themeImage('icons/bell.svg') }}" class="svg-inject h-5 inline-block fill-current stroke-current primary-text" alt="alert">
            </a>
        </div>
    @else
        <div class="inline-block px-0 ml-2">
            <a class="modal-button leading-normal text-sm hover:underline" href="javascript:" data-target="preauth-modal">
                <img src="{{ themeImage('icons/bell.svg') }}" class="click-this svg-inject h-5 inline-block fill-current stroke-current primary-text"
                     alt="alert"> <span class="hidden xl:inline-block text-sm">{{ trans('search.create_property_alert') }}</span>
            </a>
        </div>
    @endif
@endif

<div class="inline-block px-0 ml-2">
    <a class="modal-button cursor-pointer" href="javascript:" data-target="share-this-search-modal">
        <img src="{{ themeImage('search/share.svg') }}" alt="share"  class="svg-inject primary-text inline-block" loading="lazy"> <span class="hidden xl:inline-block text-sm">{{ trans('search.share_this_search') }}
    </a>
</div>

@if(hasFeature(\App\Models\TenantFeature::FEATURE_ACCOUNT_SYSTEM))
    @if(user())
        <div class="inline-block ml-2">
            <a class="modal-button mr-2 cursor-pointer" href="javascript:" data-target="create-alert-modal">
                <img src="{{ themeImage('search/bell.svg') }}" alt="alert" class="svg-inject primary-text inline-block" loading="lazy"> <span class="hidden xl:inline-block text-sm">{{ trans('search.save_search') }}</span>
            </a>
        </div>
    @else
        <div class="inline-block ml-2">
            <a class="modal-button mr-2 cursor-pointer" href="javascript:" data-target="preauth-modal">
                <img src="{{ themeImage('search/bell.svg') }}" alt="alert" class="svg-inject primary-text inline-block" loading="lazy"> <span class="hidden xl:inline-block text-sm">{{ trans('search.save_search') }}</span>
            </a>
        </div>
    @endif
@endif
<div class="inline-block float-right lg:ml-12">
    <select id="sort" name="sort" class="autojump w-full block focus:outline-none text-sm tracking-tight appearance-none select-carat bg-transparent py-1 px-2 rounded-full pr-8" style="background-position: right 0.5rem center;">
        <option value="">{{ trans('sort.sort_by') }}</option>
        <option value="{{ getSortByUrl('most-recent') }}" @if($searchRequest->get('sort') === 'most-recent') selected @endif>{{ trans('sort.most_recent') }}</option>
        <option value="{{ getSortByUrl('price-low') }}" @if($searchRequest->get('sort') === 'price-low') selected @endif>{{ trans('sort.lowest_price') }}</option>
        <option value="{{ getSortByUrl('price-high') }}" @if($searchRequest->get('sort') === 'price-high') selected @endif>{{ trans('sort.highest_price') }}</option>
    </select>
</div>
