@php
    if (!isset($extraClass)) {
        $extraClass = '';
    }
@endphp

@if(isset($properties))
    @if($properties->previousPageUrl() !== null || $properties->nextPageUrl() !== null)
    <section>
        <div class="container px-4 mx-auto mt-16 mb-16">
            <div class="flex justify-center items-center relative flex-col sm:flex-row">
                <div>
                    @if($properties->previousPageUrl() !== null)
                        <a class="text-sm text-center tracking-wide rounded-full border border-activeCcolor max-w-xs block ml-4 py-3 px-16 transition-all hover:bg-activeCcolor hover:text-white text-active-color font-medium text-activeCcolor duration-500 inline-block"
                           href="{{ $properties->withQueryString()->previousPageUrl() }}">{!! trans('pagination.previous')  !!}</a>
                    @endif
                    @if($properties->nextPageUrl() !== null)
                        <a class="text-sm text-center tracking-wide rounded-full border border-activeCcolor max-w-xs block ml-4 py-3 px-16 transition-all hover:bg-activeCcolor hover:text-white text-active-color font-medium text-activeCcolor duration-500 inline-block"
                           href="{{ $properties->withQueryString()->nextPageUrl() }}">{!! trans('pagination.next') !!}</a>
                    @endif
                </div>
            </div>
        </div>
    </section>
    @endif
@endif
