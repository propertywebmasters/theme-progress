@php
    use App\Models\TenantFeature;
    $hasListView = hasFeature(TenantFeature::FEATURE_LIST_VIEW);
    $hasSplitView = hasFeature(TenantFeature::FEATURE_SPLIT_MAP_VIEW);

    $modes = 1;
    if ($hasListView) { $modes++; }
    if ($hasSplitView) { $modes++; }

    $listModeSelected = request()->get('list') == 1;
    $mapModeSelected = request()->get('map') == 1;
    $gridModeSelected =  !$listModeSelected && !$mapModeSelected;
@endphp

@if($modes > 1)
    <div class="inline-block px-0 mr-1">

        <a href="{{ gridModeUrl() }}" title="{{ trans('search.grid') }}">
            <img src="{{ themeImage('search/grid.svg') }}" alt="grid" class="svg-inject stroke-current fill-current inline-block @if($gridModeSelected) primary-text @else text-gray-300 @endif" loading="lazy">
        </a>

        @if($hasListView)
            <a href="{{ listModeUrl() }}" class="ml-2">
                <img src="{{ themeImage('search/list.svg') }}" alt="list" class="svg-inject stroke-current fill-current inline-block @if($listModeSelected) primary-text @else text-gray-300 @endif" loading="lazy">
            </a>
        @endif

        @if($hasSplitView)
            <a href="{{ mapModeUrl() }}" class="ml-2 hidden lg:inline-block">
                <img src="{{ themeImage('search/pin.svg') }}" alt="map" class="svg-inject stroke-current fill-current inline-block @if($mapModeSelected) primary-text @else text-gray-300 @endif" loading="lazy">
            </a>
        @endif

    </div>
@endif
