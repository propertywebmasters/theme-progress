@php
    $searchControls = searchControlOptions(config('platform-cache.search_controls_enabled'));
    $bedroomFilterMode = isset($searchControls->bedrooms) && $searchControls->bedrooms ? 'single' : null;
    $bedroomFilterMode = isset($searchControls->bedrooms_as_range) && $searchControls->bedrooms_as_range ? 'multi' : $bedroomFilterMode;

    $hasShortTermRentals = hasShortTermRentals();
    $hasLongTermRentals = hasLongTermRentals();
    $hasStudentRentals = isset($searchControls->student_rentals) && $searchControls->student_rentals;
    $hasHolidayRentals = isset($searchControls->holiday_rentals) && $searchControls->holiday_rentals;
@endphp


<section class="bg-gray-100 relative z-1">
    <!-- small and lower -->
    <div class="block lg:hidden w-full relative">
        @include(themeViewPath('frontend.components.search-filter-mobile'))
    </div>

    <div class="hidden lg:block"> <!-- full search -->
        @include(themeViewPath('frontend.components.search-filter-desktop'))
    </div>
</section>
