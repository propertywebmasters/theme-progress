@if ($videos->count())
    <section class="py-8 md:py-16">
        <div class="container mx-auto px-4 relative">
            <h3 class="text-3xl text-center header-text mb-9">{{ trans('header.latest_videos') }}</h3>
            <div class="md:absolute right-4 lg:top-0 md:-top-3 mt-3 lg:mt-0 text-center md:text-right mb-8 md:mb-0 hidden md:block">
                <a class="primary-text primary-border text-sm text-center tracking-wide rounded-3xl border max-w-xs ml-4 py-2.5 px-8 transition-all inline-block duration-500" href="{{ localeUrl('/videos') }}">{{ trans('header.view_archive') }}</a>
            </div>
            <div class="grid lg:grid-cols-3 lg:gap-7">
                @foreach ($videos->slice(0, 3) as $i => $video)
                    @include(themeViewPath('frontend.components.cards.video'), ['video' => $video, 'css' => $i == 4 ? 'lg:hidden xl:hidden' : ''])
                @endforeach
            </div>
            
            <div class="md:absolute right-4 lg:top-0 md:-top-3 mt-3 lg:mt-0 text-center md:text-right mb-8 md:mb-0 block md:hidden">
                <a class="primary-text primary-border text-sm text-center tracking-wide rounded-3xl border max-w-xs ml-4 py-2.5 px-8 transition-all inline-block duration-500" href="{{ localeUrl('/videos') }}">{{ trans('header.view_archive') }}</a>
            </div>
        </div>
    </section>
@endif
