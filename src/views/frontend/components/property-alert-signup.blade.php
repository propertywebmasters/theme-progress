@php
$bgImage = assetPath(translatableContent('home', 'alert-signup-background-image'))
@endphp

@if(hasFeature(\App\Models\TenantFeature::FEATURE_ACCOUNT_SYSTEM))
    <section class="bg-whiter py-8 md:py-16">
        <div class="container mx-auto px-4 pt-20 md:pt-40 pb-20 relative center-cover-bg bg-lazy-load" data-style="background: linear-gradient(rgba(0,0,0,0.6), rgba(0,0,0,0.6)), url({{ $bgImage }});">
            <div class="grid grid-cols-1 md:grid-cols-4">
                <div class="md:col-span-3 text-center md:text-left md:pl-16 mb-6 md:mb-0">
                    <h3 class="text-3xl header-text text-white">{{ trans('property_alerts.register_for_our_property_alerts') }}</h3>
                    <span class="text-white block mt-2 lg:mt-0">{{ trans('property_alerts.dont_miss_out') }}</span>
                </div>

                <div class="text-center md:text-right md:pr-16">

                    @if(user())
                        <a class="cta text-white text-sm text-center tracking-wide rounded-full max-w-xs inline-block ml-4 py-2.5 px-6 transition-all hover:text-white duration-500 transition-all hover:primary-b" href="{{ localeUrl('/account/alerts') }}">
                            {{ trans('search.create_property_alert') }}
                        </a>
                    @else
                        <a data-target="preauth-modal" class="modal-button cta text-white text-sm text-center tracking-wide rounded-full max-w-xs inline-block ml-4 py-2.5 px-6 transition-all hover:text-white duration-500 transition-all hover:primary-b" href="javascript:">
                            {{ trans('search.create_property_alert') }}
                        </a>
                    @endif
                </div>
            </div>
        </div>
    </section>
@endif
