@php
    use App\Models\SiteSetting;use App\Services\SiteSetting\Contracts\SiteSettingServiceInterface;$useSettingsCache = config('platform-cache.site_settings_enabled');
    $useFooterCache = config('platform-cache.footers_enabled');
    $settingService = app()->make(SiteSettingServiceInterface::class);

    $siteLogo = assetPath($settingService->retrieve(SiteSetting::SETTING_LOGO['key'], $useSettingsCache));
    $footerLogo = assetPath($settingService->retrieve(SiteSetting::SETTING_FOOTER_LOGO['key'], $useSettingsCache));
    $siteAddress = $settingService->retrieve(SiteSetting::SETTING_SITE_ADDRESS['key'], $useSettingsCache);
    $siteEmail = $settingService->retrieve(SiteSetting::SETTING_SITE_EMAIL['key'], $useSettingsCache);
    $siteTel = $settingService->retrieve(SiteSetting::SETTING_SITE_TEL['key'], $useSettingsCache);
    $logoPath = !empty($footerLogo) ? $footerLogo : $siteLogo
@endphp

<footer class="footer-section lg:pt-16 pt-10">
    <div class="container mx-auto px-4 border-b">

        <div class="lg:flex lg:justify-between grid md:grid-cols-3 sm:grid-cols-2 grid-cols-1 gap-4 pb-14">
            <!-- Single -->

            @foreach($footerBlocks as $block)
                <div class="mb-8 sm:mb-0">
                    <span class="text-lg tracking-tight text-center sm:text-left block sm:inline">{!! localeStringFromCollection($block, null, config('platform-cache.footers_enabled')) !!}</span>
                    <ul class="mt-3">
                        @foreach ($block->navigation as $link)
                            <li><a href="{{ localeUrl($link->url) }}" target="{{ $link->target }}" class="text-sm hover:underline text-center sm:text-left block sm:inline {{ $link->class }}" title="View {{ localeStringFromCollection($link, null, config('platform-cache.footers_enabled')) }}">
                                    {!! localeStringFromCollection($link, null, config('platform-cache.footers_enabled'))  !!}
                                </a></li>
                        @endforeach
                    </ul>
                </div>
            @endforeach
        </div>

    </div>

    @include(themeViewPath('frontend.components.accreditations'))

    <div class="w-full hidden lg:block">
        <div class="container mx-auto">
            <div class="border-t border-b py-8">
                <div class="grid grid-cols-3 gap-4">
                <div>
                    <div class="whitespace-nowrap text-center mx-auto">
                        <div>
                            <img src="{{ themeImage('contact/phone-alt.svg') }}" class="svg-inject primary-text mx-auto text-center h-6" alt="phone" loading="lazy">
                        </div>
                        <a href="tel:{{ $siteTel }}" class="text-xs leading-tight tracking-tight inline-block">{{ $siteTel }}</a>
                    </div>
                </div>
                <div>
                    <div class="whitespace-nowrap text-center mx-auto">
                        <div>
                            <img src="{{ themeImage('contact/map-marker-alt.svg') }}" class="svg-inject primary-text fill-current mx-auto text-center h-6" alt="marker" loading="lazy">
                        </div>
                        <address class="text-xs leading-tight tracking-tight inline-block not-italic text-left">{{ $siteAddress }}</address>
                    </div>
                </div>
                <div>
                    <div class="whitespace-nowrap text-center mx-auto">
                        <div>
                            <img src="{{ themeImage('contact/email2.svg') }}" class="svg-inject primary-text fill-current mx-auto text-center h-6" alt="email" loading="lazy">
                        </div>
                        <a href="mailto:{{ $siteEmail }}" class="text-xs leading-tight tracking-tight inline-block">{{ $siteEmail }}</a>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>

    @include(themeViewPath('frontend.components.social-footer'))

</footer>


