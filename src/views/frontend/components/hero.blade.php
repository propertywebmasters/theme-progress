@php
    // if the slide is empty then we will inherit from theme default
    if($slides->count() === 0) {
        $imagePath = \App\Support\ThemeManager::backgroundCSS('home.hero', tenantManager()->getTenant(), true);
        $slide = new \App\Models\Slide;
        $slide->image = $imagePath;
        $slide->title = optional(app()->make(\App\Services\Content\ContentBlockService::class)->getByIdentifier('home', 'hero-slogan', tenantManager()->getTenant(), app()->getLocale()))->content;
        $slides = collect()->push($slide);
    }
@endphp

@include(themeViewPath('frontend.components.hero.hero-media'))
