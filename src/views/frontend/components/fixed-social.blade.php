@if (hasFeature(\App\Models\TenantFeature::FEATURE_SOCIAL_FIXED_TAB))

    @php
        $service = app()->make(\App\Services\SocialAccount\Contracts\SocialAccountServiceInterface::class);
        $socialAccounts = $service->get();

        $socialCount = 0;
        if ($socialAccounts !== null) {
            $networks = ['twitter', 'linked_in', 'facebook', 'instagram', 'tiktok'];
            foreach ($networks as $network) {
                if ($socialAccounts->$network !== null) {
                    $socialCount++;
                }
            }
        }
    @endphp

    @if($socialCount > 0)
        <div id="fixed-social-tab" class="hidden lg:block fixed z-40 top-1/2 right-0 secondary-bg text-white px-2 py-4" style="transform: translate(0%, -50%);">
            <div class="grid grid-cols-1 gap-2">
                @foreach($networks as $network)
                    @if($socialAccounts->$network === null)
                        @continue
                    @endif
                    <div class="text-center block" style="text-align: center; margin: 0 auto;">
                        <a href="{{ $socialAccounts->$network }}" class="block p-2" target="_BLANK">
                            <img src="{{ themeImage('social/'.$network.'.svg') }}" class="svg-inject text-white fill-current h-4" alt="{{ $network }}">
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
    @endif
@endif

