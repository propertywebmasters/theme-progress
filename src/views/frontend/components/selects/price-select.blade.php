@php
    $classes = 'border-b bg-transparent border-grey2 focus:border-0 border-opacity-30 focus:outline-none text-white py-3 w-full'
@endphp

<select id="{{ $id }}" name="{{ $name }}" class="{{ $classes }}">
    <option value="">{{ $initialOption }}</option>
    @foreach($prices as $price)
        @php
            $priceFormat = $defaultCurrency->symbol.number_format($price);
            if ($mode === 'rental') {
                $priceFormat .= ' '.trans('rental_frequency.'.$defaultRentalFrequency);
            }
            $selected = $price != 0 && $price == $currentValue ? ' selected="selected"' : ''
        @endphp
        <option value="{{ $price }}" {{ $selected }} class="text-black">{!! $priceFormat !!}</option>
    @endforeach
</select>
