@php
    $dataList = app()->make(App\Services\Search\Contracts\SearchServiceInterface::class)->dataListLocations();
    $roundedClass = '';
    $additionalClasses = 'lg:pl-2 border-l-0 pl-0';
    $locationValue = $searchRequest->request->get('location') !== null ? locationUrlStringToText($searchRequest->request->get('location')) : '';

    if (isset($searchControls->tender_type) && $searchControls->tender_type !== 'sale_only') {
        $roundedClass = 'lg:rounded-l-none';
        $additionalClasses .= ' lg:border-l';
        $colSpan = 'col-span-3';
    }

    if (isset($searchControls->tender_type) && $searchControls->tender_type === 'sale_only') {
        $colSpan = 'col-span-4';
    }

    if (!isset($extraCss)) {
        $extraCss = '';
    }
@endphp

<div id="location-datalist" class="location-datalist-container rounded-full {{ $roundedClass }} bg-transparent lg:rounded-r-none my-2 lg:my-0 relative {{ $extraCss }}">

    {{-- <input class="location-datalist-input w-full bg-white block focus:outline-none -ml-1 {{ $borderClass }} lg:pl-2 border-l-0 pl-0 text-sm bg-transparent"  autocomplete="off" role="combobox" list="" id="location_datalist" name="location" placeholder="{{ trans('placeholder.search_location') }}" style="z-index: 2;"> --}}

    @include(themeViewPath('frontend.components.location-search-input'), [
        'datalist' => true,
        'additionalClasses' => $additionalClasses,
        'styles' => 'z-index: 2;',
        'value' => $locationValue
    ])

    <datalist id="browsers" role="listbox" class="absolute w-full text-xs top-8 left-0" style="z-index: 50;">
        <div class="pr-1 rounded-lg rounded-t-none bg-white" style="z-index: -1;">
            <div class="drop-shadow-md shadow-inner text-sm overflow-y-scroll overflow-x-hidden max-h-44 md:max-h-52 rounded-b-lg p-0">
                @php $i = 1 @endphp
                @foreach($dataList as $text => $value)
                    <option class="overflow-hidden text-xs text-black cursor-pointer hover:text-white hover-primary-bg py-2 px-4 @if($i == 1)  @endif" data-value="{{ $value }}">{{ $text }}</option>
                    @php $i++ @endphp
                @endforeach
            </div>
        </div>
    </datalist>

    <input type="hidden" name="location_url">
</div>
