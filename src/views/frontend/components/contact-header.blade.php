@php
    use App\Services\Branch\Contracts\BranchServiceInterface;
    use App\Models\SiteSetting;
    use App\Services\SiteSetting\Contracts\SiteSettingServiceInterface;
    $email = app()->make(SiteSettingServiceInterface::class)->retrieve(SiteSetting::SETTING_SITE_EMAIL['key']);
    $siteTel = app()->make(SiteSettingServiceInterface::class)->retrieve(SiteSetting::SETTING_SITE_TEL['key']);
    $branches = app()->make(BranchServiceInterface::class)->all();
    $hasBranches = $branches->count() > 0;
    $whatsappTel = app()->make(SiteSettingServiceInterface::class)->retrieve(SiteSetting::SETTING_SITE_WHATSAPP_NUMBER['key']);
@endphp

<header class="bg-whiter hidden lg:block py-2">
    <div class="lg:container mx-auto text-right px-4">
        <ul class="flex justify-between md:justify-end">
            <li class="border-r-2 pr-5">
                <a class="text-white flex" href="{{ localeUrl('/contact') }}">
                    <img class="svg-inject primary-text mr-2 h-3" src="{{ themeImage('contact/email2.svg') }}" alt="email">
                    <span class="text-xs text-black transition-all">{{ $email }}</span>
                </a>
            </li>
            <li class="ml-5">
                <a class="text-white flex" href="tel:{{$siteTel}}">
                    <img class="svg-inject primary-text mr-2 h-3" src="{{ themeImage('contact/phone-alt.svg') }}" alt="phone">
                    <span class="text-xs text-black transition-all">{{ $siteTel }}</span>
                </a>
            </li>

            @if($whatsappTel !== null && $whatsappTel !== '')
                <li class="hidden lg:inline-block text-center md:text-left ml-5">
                    <a class="text-white text-sm flex" href="https://api.whatsapp.com/send?phone={{ $whatsappTel }}" target="_BLANK">
                        <img class="svg-inject fill-current stroke-current primary-text mr-2 h-3 mt-0.5" src="{{ themeImage('whatsapp.svg') }}" alt="whatsapp">
                        <span class="text-xs text-black transition-all">{{ $whatsappTel }}</span>
                    </a>
                </li>
            @endif

            @if ($branches->count() > 0)
                <li class="ml-5 border-l-2 pl-5">
                    <a class="branch-contacts-toggle text-sm flex" href="javascript:">
                        <img class="svg-inject primary-text mr-2 h-3" src="{{ themeImage('contact/map-marker-alt.svg') }}" alt="branches">
                        <span class="text-xs text-black">{{ trans('generic.our_branches') }}</span>
                    </a>
                </li>
            @endif

        </ul>
    </div>
</header>

<div class="branch-contacts-dropdown absolute left-0 w-full tertiary-bg opacity-95 z-50 hidden" style="border-top: 1px solid var(--ap-nav-bg);">
    <div class="container mx-auto py-4 md:py-12 pb-4 md:pb-16">
        <div>
            <div class="grid grid-cols-1 md:grid-cols-3 gap-8 text-white text-center md:text-left px-2 md:px-0">

                @foreach($branches as $branch)
                    <div>
                        <span class="block text-2xl py-6">{{ $branch->name }}</span>

                        <div class="grid grid-cols-12 pb-2 md:pb-4">
                            <div class="text-center md:text-left col-span-12 md:col-span-1 pb-2 md:pb-0"><img src="{{ themeImage('contact/map-marker-alt.svg') }}"
                                                                                                              class="svg-inject primary-text inline-block" alt="phone"
                                                                                                              loading="lazy"></div>
                            <div class="col-span-12 md:col-span-11 md:pl-2 lg:pl-0">{{ $branch->displayAddress() }}</div>
                        </div>

                        <div class="grid grid-cols-12 pb-2 md:pb-4">
                            <div class="text-center md:text-left col-span-12 md:col-span-1 pb-2 md:pb-0"><img src="{{ themeImage('contact/email2.svg') }}"
                                                                                                              class="svg-inject primary-text inline-block" alt="phone"
                                                                                                              loading="lazy"></div>
                            <div class="col-span-12 md:col-span-11 md:pl-2 lg:pl-0">{{ $branch->email}}</div>
                        </div>

                        <div class="grid grid-cols-12 pb-2 md:pb-4">
                            <div class="text-center md:text-left col-span-12 md:col-span-1 pb-2 md:pb-0"><img src="{{ themeImage('contact/phone-alt.svg') }}"
                                                                                                              class="svg-inject primary-text inline-block" alt="phone"
                                                                                                              loading="lazy"></div>
                            <div class="col-span-12 md:col-span-11 md:pl-2 lg:pl-0">{{ $branch->tel }}</div>
                        </div>

                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
