@extends('layouts.app')

@section('content')

    {{-- site header component, includes navigation --}}
    @include(themeViewPath('frontend.components.header'))

    <div class="relative">
        <section id="home-hero" class="relative">
            @include(themeViewPath('frontend.components.developments.hero-slideshow'))
        </section>
    </div>

    {{-- Featured properties band --}}
    @if($featuredProperties->count() > 0)
        <div class="bg-white">
            @include(themeViewPath('frontend.components.developments.featured-developments'), ['customHeader' => trans('header.featured_developments'), 'properties' => $featuredProperties])
        </div>
    @endif

    {{-- About us band --}}
    @include(themeViewPath('frontend.components.developments.about-developments'))

    {{-- site footer --}}
    @include(themeViewPath('frontend.components.footer'))

@endsection
