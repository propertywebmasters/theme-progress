@extends('layouts.app')

@section('content')

    {{-- site header component, includes navigation --}}
    @include(themeViewPath('frontend.components.header'), ['transparentNavigation' => false,])

    @php
        use App\Models\TenantFeature;
        $multiColumn = hasFeature(TenantFeature::FEATURE_GENERIC_CONTACT_FORM);
        $breadcrumb[] = [trans('header.home') => localeUrl('/')];

        if ($page->parent !== null) {
            $breadcrumb[] = [$page->parent->title => '/'.$page->parent->url_key];
        }

        $breadcrumb[] = [$page->title => null]
    @endphp

    <section class="bg-white">
        <div class="container px-4 mx-auto">
            <h2 class="text-2xl md:text-3xl pb-2 font-medium py-6 header-text">{!! $page->title !!}</h2>
            <!--  Breadcrumb -->
            @include(themeViewPath('frontend.components.page-breadcrumbs'), ['navigation' => $breadcrumb, 'class' => 'border-t pt-5'])
        </div>

        @include(themeViewPath('frontend.components.system-notifications'), ['customClass' => '-mt-4 mb-4'])

    </section>

    <!-- Content -->
    <section class="py-8">
        <div class="container px-4 mx-auto">
            <div class="flex flex-wrap">
                @php
                    $width = $multiColumn ? 'lg:w-4/6' : ''
                @endphp
                <div class="w-full {{ $width }} pr-0 md:pr-8">
                    <div class="mb-16">
                        @if ($page->image)
                            <img src="{{ assetPath($page->image) }}" class="w-full mb-6">
                        @endif

                        <div class="generic-page-content">
                            <p class="text-base leading-normal tracking-tight font-light">{!! parseContentForShortcodes($page->content) !!}</p>
                        </div>
                    </div>
                </div>

                @if($multiColumn)
                    <div class="w-full lg:w-2/6">
                        <div class="shadow-md py-9 px-4 lg:px-7">
                            <form action="{{ url()->current() }}" method="post" enctype="application/x-www-form-urlencoded" class="recaptcha">
                                <h3 class="text-2xl leading-normal text-center tracking-tight text-primary mb-7 header-text">{{ trans('contact.get_in_touch') }}</h3>
                                <input type="text" name="name" placeholder="{{ trans('contact.full_name') }}"
                                       class="rounded-sm bg-whiter h-14 w-full px-4 mb-3 font-light">
                                <input type="email" name="email" placeholder="{{ trans('contact.email_address') }}"
                                       class="rounded-sm bg-whiter h-14 w-full px-4 mb-3 font-light">
                                <input type="text" name="tel" placeholder="{{ trans('contact.telephone_number') }}"
                                       class="rounded-sm bg-whiter h-14 w-full px-4 mb-3 font-light">
                                <textarea name="message" id="" cols="30" rows="10" placeholder="{{ trans('contact.your_message') }}"
                                          class="rounded-sm bg-whiter h-32 w-full px-4 py-3 mb-3 font-light"></textarea>
                                <input type="hidden" name="url" value="{{ url()->current() }}">
                                @include(themeViewPath('frontend.forms.recaptcha.recaptcha-fallback-inputs'), ['inputClass' => 'rounded-sm bg-whiter h-14 w-full px-4 mb-3 font-light'])
                                <div class="text-center">
                                    <button type="submit" class="text-center tracking-wide text-white cta px-4 sm:px-9 rounded-full h-9 sm:h-12  right-1 top-1 hover:bg-hover transition-all duration-500">
                                        {{ trans('button.enquire_now') }}
                                    </button>
                                    @csrf
                                </div>
                            </form>
                        </div>
                    </div>
                @endif

            </div>

        </div>
    </section>

    <!-- Newsletter Signup Form -->
    @include(themeViewPath('frontend.components.newsletter-signup'))

    {{-- site footer --}}
    @include(themeViewPath('frontend.components.footer'))

@endsection
