@extends('layouts.app')

@section('content')

    {{-- site header component, includes navigation --}}
    @include(themeViewPath('frontend.components.header'))

    <!-- Banner -->
    <div class="md:min-h-158 py-16 md:py-0 relative pt-8 bg-cover bg-center flex items-center center-cover-bg bg-lazy-load" data-style="{{ backgroundCSSImage('about.hero') }}">
        <div class="relative z-10 container mx-auto px-4 mt-9">
            <h3 class="text-4xl md:text-5xl header-text text-white text-center md:text-left py-32 md:py-0">
                {!! translatableContent('about', 'about-title') !!}
            </h3>
        </div>
    </div>
    <!-- Welcome -->
    <section class="bg-whiter pt-8 pb-14 md:py-16">
        <div class="container mx-auto px-4">
            <div class="flex items-center flex-wrap">
                <div class="lg:w-1/2 mb-8">
                    <h3 class="text-3xl lg:text-4xl header-text mb-8 md:mb-12">{!! translatableContent('about', 'about-section-1-title') !!}</h3>
                    <p class="leading-normal tracking-tight font-light">
                        {!! translatableContent('about', 'about-section-1-text') !!}
                    </p>
                </div>
                <div class="lg:w-1/2 w-full xl:pl-20 lg:pl-9">
                    <img class="w-full object-cover" src="{{ assetPath(translatableContent('about', 'about-main-image')) }}" alt="about">
                </div>
            </div>
        </div>
    </section>

    <div>
        @include(themeViewPath('frontend.components.features'))
    </div>

    <!-- =================================================== -->
    <section class="py-8 md:pb-24 md:pt-0">
        <div class="container mx-auto px-4">
            <div class="mx-auto text-center w-2/3 pb-2">
                <hr class="block"/>
            </div>

            <div class="mx-auto max-w-4xl">
                <h3 class="lg:text-4xl text-3xl leading-tight text-center header-text pt-10 md:pt-16">{!! translatableContent('about', 'about-main-title') !!}</h3>
            </div>
        </div>
    </section>

    <!-- Content -->
    <section class="bg-whiter py-8 md:py-16">
        <div class="container mx-auto px-4">
            <div class="flex items-center flex-wrap">
                <div class="lg:w-1/2 w-full xl:pr-20 lg:pr-9 mb-8 lg:mb-0">
                    <img class="w-full object-cover" src="{{ assetPath(translatableContent('about', 'about-additional-image-1')) }}" alt="about" loading="lazy">
                </div>
                <div class="lg:w-1/2 lg:mb-8">
                    <h3 class="text-3xl lg:text-4xl header-text mb-8">
                        {!! translatableContent('about', 'about-section-2-title') !!}
                    </h3>
                    <p class="leading-normal tracking-tight font-light text-primary-text">
                        {!! translatableContent('about', 'about-main-text') !!}
                    </p>
                </div>

            </div>
        </div>
    </section>

    <!-- Latest News -->
    @include(themeViewPath('frontend.components.latest-news'))

    <!-- Latest Videos -->
    @include(themeViewPath('frontend.components.latest-videos'))

    <!-- Newsletter Signup Form -->
    @include(themeViewPath('frontend.components.newsletter-signup'))

    {{-- Site Footer --}}
    @include(themeViewPath('frontend.components.footer'))

@endsection
