@extends('layouts.app')
@section('content')

    {{-- site header component, includes navigation --}}
    @include(themeViewPath('frontend.components.header'))

    <section id="career-page" class="pt-4">
        <div class="container pb-12 px-8 xl:px-0 mx-auto">
            <div>
                <h2 class="text-2xl md:text-4xl pb-2 font-medium py-6 header-text">{!! trans('career.careers') !!}</h2>
            </div>
            <hr class="mb-4">

            <div class="text-sm mb-4">
                @include(themeViewPath('frontend.components.page-breadcrumbs'), ['navigation' => [
                   [trans('header.home') => localeUrl('/')],
                   [trans('career.careers') => localeUrl('/careers')],
                   [$career->data->title => null],
                ]])
            </div>

            @include(themeViewPath('frontend.components.system-notifications'), ['customClass' => '-mt-4 mb-4'])

            <div class="pt-8">
                <div class="grid grid-cols-1 lg:grid-cols-7 lg:gap-16">
                    <div class="col-span-1 lg:col-span-4 xl:col-span-5 pt-2 mb-10 lg:mb-0">

                        <h3 class="text-xl font-medium inline primary-text">{!!  optional($career->data)->title  !!}<span class="float-right text-xl font-bold">{{ $career->salary }}</span></h3>
                        <div class="generic-page-content text-base text-justify pt-6">{!! optional($career->data)->description !!}</div>
                    </div>

                    <div class="lg:col-span-3 xl:col-span-2">
                        <div class="p-6 bg-whiter">
                            <h3 class="text-2xl leading-relaxed text-center pb-4 header-text">{{ trans('career.interested') }}</h3>
                            @include(themeViewPath('frontend.forms.career'))
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    {{-- site footer --}}
    @include(themeViewPath('frontend.components.footer'))

@endsection
