@extends('layouts.app')

@section('content')

    @php
        $searchControls = searchControlOptions(config('platform-cache.search_controls_enabled'))
    @endphp

    {{-- site header component, includes navigation --}}
    @include(themeViewPath('frontend.components.header'))

    <!-- Banner -->
    <div class="min-h-158 relative pt-8 bg-cover bg-center center-cover-bg bg-lazy-load flex items-center" data-style="{{ 'background: linear-gradient(rgba(0,0,0,0.5), rgba(0,0,0,0.5)), url('.assetPath($areaGuide->image).'); background-size: cover !important;' ?? backgroundCSSImage('area-guide.hero') }}">
        <div class="absolute left-0 top-0 w-full h-full z-10 bg-black opacity-50"></div>
        <div class="relative z-10 container mx-auto px-4 mt-9">
            <h1 class="text-4xl lg:text-5xl xl:text-7xl header-text text-white text-center mx-auto">{{ $areaGuide->search_title }}</h1>
            <div id="area-guide-hero-search">
            @include(themeViewPath('frontend.components.home-search'))
            </div>
        </div>
    </div>
    <!-- Welcome -->
    <section class="bg-whiter py-24">
        <div class="container mx-auto px-4">
            <div class="flex items-center flex-wrap">
                <div class="lg:w-1/2 mb-8">
                    <h3 class="text-5xl header-text mb-12">{!! translatableContent('home', 'about-title') !!}</h3>
                    <p class="font-worksans leading-normal tracking-tight font-light text-primary-text">
                        {!! translatableContent('home', 'about-text') !!}
                    </p>
                </div>
                <div class="lg:w-1/2 w-full xl:pl-20 lg:pl-9">
                    @php
                        $sliderSizes = ['sm' => 30, 'md' => 10, 'lg' => 10, 'xl' => 10];
                        $sliderCounts = ['sm' => 1, 'md' => 1, 'lg' => 1, 'xl' => 1];
                        echo generateSlider($sliderCounts, $sliderSizes, $areaGuide->images->toArray())
                    @endphp
                </div>
            </div>
        </div>
    </section>
    <!-- Featured Durham Properties -->

    <div class="bg-white">
        @include(themeViewPath('frontend.components.featured-properties'), ['customHeader' => trans('header.featured_properties'), 'properties' => $featuredProperties])
    </div>

    {{-- property alert signup band --}}
    @include(themeViewPath('frontend.components.property-alert-signup'))

    <!-- Buying Property in Durham -->
    <section class="my-16">
        <div class="container mx-auto px-4">
            <div class="mx-auto max-w-4xl">
                <h3 class="text-5xl text-center primary-text mb-12">{{ $areaGuide->footer_title }}</h3>
                <p class="leading-normal text-center tracking-tight font-light primary-text">{!! $areaGuide->footer_content  !!}</p>
                <div class="text-center mt-14">
                    <a class="text-sm text-center tracking-wide text-activeCcolor rounded-full border border-activeCcolor max-w-xs inline-block ml-4 py-3 px-12 transition-all hover:bg-hover hover:text-white duration-500" href="{{ localeUrl('all-properties-for-sale/in/'.$areaGuide->location ) }}">Find properties</a>
                </div>
            </div>
        </div>
    </section>
    <!-- 3 Column Block -->

    @if(!$areaGuide->amenities->isEmpty())
        <section>
            <div class="container mx-auto px-4">
                <div class="grid lg:grid-cols-{{ $areaGuide->amenities->count() }} lg:gap-7 border-b-2 pb-16">
                    @foreach($areaGuide->amenities as $amenity)
                        <div class="mb-7 lg:mb-0 text-center">
                            <h3 class="mt-6 mb-2 text-2xl font-medium secondary-text">{{ $amenity->title }}</h3>
                            <p class="leading-tight tracking-tight font-light secondary-text ">{{ $amenity->content }}</p>
                        </div>
                    @endforeach
                </div>
            </div>
        </section>
    @endif

    @if(isset($articles) && $articles->count() > 0)
        @include(themeViewPath('frontend.components.latest-news'), ['customHeader' => $newsIsRelated ? trans('header.latest_news_in').' '.$areaGuide->location : trans('header.latest_news')])
    @endif

    {{-- site footer --}}
    @include(themeViewPath('frontend.components.newsletter-signup'))

    {{-- site footer --}}
    @include(themeViewPath('frontend.components.footer'))

@endsection
