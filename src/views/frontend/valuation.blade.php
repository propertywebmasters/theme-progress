@extends('layouts.app')

@section('content')

    @php
        $fullName = isset($lead->payload['first_name']) ? $lead->payload['first_name'].' '.$lead->payload['surname'] : old('name');
        $email = $lead->payload['email'] ?? old('email');
        $tel = $lead->payload['tel'] ?? old('tel');

        if ($fullName === null) {
            $fullName = optional(user())->name();
        }

        if ($email === null) {
            $email = optional(user())->email;
        }

        if ($tel === null) {
            $tel = optional(user())->tel;
        }
    @endphp

    {{-- site header component, includes navigation --}}
    @include(themeViewPath('frontend.components.header'))
    <section id="valuation" class="center-cover-bg bg-lazy-load" data-style="{{ backgroundCSSImage('valuation.hero') }} padding-top: calc(1.75rem + 67px);">
        <div class="pt-8 pb-60">
            <div class="container mx-auto transparent pt-7 px-8 lg:px-32 xl:px-64 pb-12 text-center">
                <h1 class="text-white text-3xl lg:text-5xl mx-auto pt-6 pb-2 header-text">{!! dynamicContent($pageContents, 'valuation-intro-title') !!}</h1>

                @php
                    $content = trim(html_entity_decode(dynamicContent($pageContents, 'valuation-intro-text')), " \t\n\r\0\x0B\xC2\xA0")
                @endphp

                @if (!empty($content))
                    <p class="pb-6 text-white text-xs lg:text-sm">{!! dynamicContent($pageContents, 'valuation-intro-text') !!}</p>
                @endif
            </div>
        </div>
    </section>
    <div class="container mx-auto px-8 lg:px-32 xl:px-64 -mt-60 pb-12">
        <div class="">
            <div class="bg-white p-8 mx-auto text-center shadow">

                <h2 class="text-lg mx-auto">{!! dynamicContent($pageContents, 'valuation-form-title') !!}</h2>
                <span class="block text-xs">{!! dynamicContent($pageContents, 'valuation-form-subtitle') !!}</span>

                @include(themeViewPath('frontend.components.system-notifications'))

                <form action="{{ localeUrl('/valuation') }}" method="post" enctype="application/x-www-form-urlencoded" class="recaptcha">

                    <span class="block text-center font-medium pt-8">{{ trans('valuation.address_details') }}</span>

                    <div class="grid grid-cols-1 md:grid-cols-2 gap-0 md:gap-2">
                        <div>
                            <input type="text" name="line1" placeholder="{{ trans('valuation.address_line_1') }} *" class="text-sm block bg-gray-100 text-gray-900 block w-full my-3 p-3 py-4 placeholder-gray-600"
                                   autocomplete="off" required>

                            <input type="text" name="line2" placeholder="{{ trans('valuation.address_line_2') }}" class="text-sm block bg-gray-100 text-gray-900 block w-full my-3 p-3 py-4 placeholder-gray-600"
                                   autocomplete="off">

                            <input type="text" name="city" placeholder="{{ trans('valuation.town_city') }}*" class="text-sm block bg-gray-100 text-gray-900 block w-full my-3 p-3 py-4 placeholder-gray-600"
                                   autocomplete="off" required>
                        </div>
                        <div class="-mt-3 md:mt-0">
                            <input type="text" name="state" placeholder="{{ trans('valuation.state_area') }}" class="text-sm block bg-gray-100 text-gray-900 block w-full my-3 p-3 py-4 placeholder-gray-600"
                                   autocomplete="off" required>

                            <select id="country" name="country" class="text-sm block bg-gray-100 text-gray-900 block w-full my-3 p-3 py-4 placeholder-gray-600">
                                <option value="">{{ trans('valuation.select_country') }} *</option>
                                @foreach($countries as $country)
                                    <option value="{{ $country->name }}">{{ $country->name }}</option>
                                @endforeach
                            </select>

                            <input id="valuation-postcode" type="text" name="postcode" placeholder="{{ trans('valuation.postal_zipcode') }}" class="text-sm bg-gray-100 text-gray-900 block w-full my-3 p-3 py-4 placeholder-gray-600"
                                   autocomplete="off" required>
                        </div>
                    </div>

                    <span class="text-center block font-medium pt-4">{{ trans('header.property_details') }}</span>

                    <select id="tender_type" name="tender_type" class="text-sm block bg-gray-100 text-gray-900 block w-full my-3 p-3 py-4 placeholder-gray-600" required>
                        <option value="">{{ trans('valuation.tender_type') }} *</option>
                        <option value="for sale">{{ trans('valuation.for_sale') }}</option>
                        <option value="for rent">{{ trans('valuation.for_rent') }}</option>
                        <option value="other enquiry">{{ trans('valuation.other_enquiry') }}</option>
                    </select>

                    <select id="sell_when" name="sell_when" class="text-sm block bg-gray-100 text-gray-900 block w-full my-3 p-3 py-4 placeholder-gray-600" required>
                        <option value="">{{ trans('valuation.when_selling') }} *</option>
                        <option value="immediately">{{ trans('valuation.immediately') }}</option>
                        <option value="3 Months">{{ trans('valuation.3_months') }}</option>
                        <option value="6 Months">{{ trans('valuation.6_months') }}</option>
                        <option value="12 Months">{{ trans('valuation.12_months') }}</option>
                        <option value="curious">{{ trans('valuation.curious') }}</option>
                    </select>

                    <select id="furnished" name="furnished" class="text-sm block bg-gray-100 text-gray-900 block w-full my-3 p-3 py-4 placeholder-gray-600" required>
                        <option value="">{{ trans('valuation.furnishings') }} *</option>
                        <option value="fully_furnished">{{ trans('valuation.fully_furnished') }}</option>
                        <option value="part_furnished">{{ trans('valuation.part_furnished') }}</option>
                        <option value="not_furnished">{{ trans('valuation.not_furnished') }}</option>
                    </select>

                    <select id="bedrooms" name="bedrooms" class="text-sm block bg-gray-100 text-gray-900 block w-full my-3 p-3 py-4 placeholder-gray-600" required>
                        <option value="">{{ trans('label.num_bedrooms') }} *</option>
                        @foreach(range(1, 10) as $bedroomCount)
                            <option value="{{ $bedroomCount }}">{{ $bedroomCount }}</option>
                        @endforeach
                    </select>

                    <select id="property_type" name="property_type" class="text-sm block bg-gray-100 text-gray-900 block w-full my-3 p-3 py-4 placeholder-gray-600" required>
                        <option value="">{{ trans('label.property_type') }} *</option>
                        @foreach($propertyTypes as $propertyType)
                            @php
                                $propertyTypeName = strtolower($propertyType->name);
                            @endphp
                            <option value="{{ $propertyType->name }}">{{ trans('property_types.'.$propertyTypeName) }}</option>
                        @endforeach
                    </select>

                    <select id="garage" name="garage" class="text-sm block bg-gray-100 text-gray-900 block w-full my-3 p-3 py-4 placeholder-gray-600" required>
                        <option value="">{{ trans('valuation.garage') }} *</option>
                        <option value="Yes">{{ trans('generic.yes') }}</option>
                        <option value="No">{{ trans('generic.no') }}</option>
                    </select>

                    <span class="block text-center font-medium pt-4">{{ trans('valuation.your_details') }}</span>

                    <input type="text" name="name" placeholder="{{ trans('contact.full_name') }} *" class="text-sm block bg-gray-100 text-gray-900 block w-full my-3 p-3 py-4 placeholder-gray-600" autocomplete="off" value="{{ $fullName }}" required>
                    <input type="email" name="email" placeholder="{{ trans('contact.email_address') }} *" class="text-sm block bg-gray-100 text-gray-900 block w-full my-3 p-3 py-4 placeholder-gray-600" autocomplete="off" value="{{ $email  }}" required>
                    <input type="text" name="tel" placeholder="{{ trans('contact.telephone_number') }} *" class="text-sm block bg-gray-100 text-gray-900 block w-full my-3 p-3 py-4 placeholder-gray-600" autocomplete="off" value="{{ $tel  }}" required>
                    <input type="text" name="budget" placeholder="{{ trans('valuation.your_budget') }} *" class="text-sm block bg-gray-100 text-gray-900 block w-full my-3 p-3 py-4 placeholder-gray-600" autocomplete="off" value="" required>

                    <textarea name="comments" class="text-sm block bg-gray-100 text-gray-900 block w-full my-3 p-3 py-4 placeholder-gray-600" rows="6" placeholder="{{ trans('valuation.additional_information') }}"></textarea>

                    @include(themeViewPath('frontend.forms.recaptcha.recaptcha-fallback-inputs'), ['inputClass' => 'text-sm block bg-gray-100 text-gray-900 block w-full my-3 p-3 py-4 placeholder-gray-600'])

                    <div class="text-xs py-4 modal-terms">
                        {!! trans('terms.modal_terms') !!}
                    </div>

                    <button type="submit" role="button" class="mx-auto w-full lg:w-1/2 primary-bg block text-center p-4 text-sm leading-normal tracking-wide font-semibold text-white uppercase cursor-pointer rounded-3xl cta hover:bg-hover">
                        {{ trans('valuation.send_details') }}</button>

                    @csrf
                </form>

            </div>

        </div>

    </div>

    {{-- Testimonials band --}}
    @include(themeViewPath('frontend.components.testimonials'))

    <!-- Newsletter Signup Form -->
    @include(themeViewPath('frontend.components.newsletter-signup'))

    {{-- site footer --}}
    @include(themeViewPath('frontend.components.footer'))

@endsection
