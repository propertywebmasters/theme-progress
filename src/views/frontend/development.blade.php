@php use App\Models\Property;use Illuminate\Support\Str; @endphp
@php use App\Models\TenantFeature; @endphp
@extends('layouts.app')

@push('open-graph-tags')
    @include(themeViewPath('frontend.components.property.open-graph'))
@endpush

@section('content')

    @php
        $imageString = $captionString = '';
        foreach ($property->images as $image) {
            $imageString .= getPropertyImage($image->filepath, 'xl').'|';
            $captionString .= $image->caption.'|';
        }

        $imageString = trim($imageString, '|');
        $captionString = trim($captionString, '|');
        $imageCount = $property->images->count()
    @endphp

    {{-- site header component, includes navigation --}}
    @include(themeViewPath('frontend.components.header'))

    <section id="property-detail">
        <div>
            <div class="w-full relative">
                <div class="absolute top-0 left-0 w-full h-full bg-black opacity-20 z-10 cursor-pointer start-gallery"></div>

                <div class="absolute top-4 left-4 z-10">
                    <a class="cursor-pointer tertiary-bg text-white leading-loose text-center font-bold text-xs md:text-base bg-opacity-70 px-4 py-1 rounded-full"
                       href="{{ localeUrl('/developments') }}">{{ trans('search.back_to_search') }}</a>
                </div>

                <div class="sl-btns absolute bottom-2 left-2 md:bottom-7 md:left-4 xl:bottom-4 xl:left-4 flex z-30">

                    @if ($property->images->count() > 0)
                        <a href="javascript:"
                           class="start-gallery cursor-pointer tertiary-bg text-white mr-2 leading-loose text-center font-bold text-white text-xs md:text-base bg-opacity-70 px-4 py-1 rounded-full">
                            {{ trans('generic.images') }}
                        </a>
                    @endif

                    @if($useMap && $property->location->latitude !== null && $property->location->longitude !== null)
                        <a href="javascript:"
                           class="tertiary-bg text-white mr-2 leading-loose text-center font-bold text-xs md:text-base bg-opacity-70 px-4 py-1 rounded-full modal-button"
                           data-target="property-location-modal">{{ trans('header.location') }}</a>
                    @endif
                    @if ($property->documents->count() > 0)
                        <a href="javascript:"
                           class="tertiary-bg text-white mr-2 leading-loose text-center font-bold text-xs md:text-base bg-opacity-70 px-4 py-1 rounded-full modal-button"
                           data-target="property-documents-modal">{{ trans('header.documents') }}</a>
                    @endif
                    @if ($property->video_url)
                        <a href="javascript:"
                           class="tertiary-bg text-white mr-2 leading-loose text-center font-bold text-white text-xs md:text-base bg-opacity-70 px-4 py-1 rounded-full modal-button"
                           data-target="property-video-modal">{{ trans('header.video') }}</a>
                    @endif
                    @if ($property->virtual_tour_url)
                        <a href="javascript:"
                           class="tertiary-bg text-white mr-2 leading-loose text-center font-bold text-white text-xs md:text-base bg-opacity-70 px-4 py-1 rounded-full modal-button"
                           data-target="property-virtual-tour-modal">{{ trans('header.virtual_tour') }}</a>
                    @endif
                </div>

                <div class="absolute top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2 z-10 text-center text-white">
                    <h3 class="text-2xl pb-2 font-medium">{{ $property->displayName() }}</h3>
                    <span class="text-sm block pb-4 leading-normal font-medium">{{ $property->location->displayAddress() }}</span>
                </div>

                <div class="swiper swiper-gallery-main overflow-hidden md:p-0 z-1 list-none relative">
                    @php
                        $mainClasses = 'h-54 max-h-54 md:h-114 md:max-h-114 md:h-114 md:max-h-114 lg:min-h-180 lg:h-180 lg:max-h-180 xl:h-180 xl:min-h-180 xl:max-h-180 overflow-hidden';
                        $thumbClasses = 'h-16 mx-auto overflow-hidden';
                        $buttonClasses = 'block align-middle w-full text-sm xl:text-base text-center tracking-wide font-bold uppercase rounded-full transition-all py-4 primary-bg text-white'
                    @endphp
                    <div class="swiper-wrapper flex z-1 relative w-full transition-transform box-content slider {{ $mainClasses }}" data-images="{{ $imageString }}"
                         data-captions="{{ $captionString }}">

                        @if($property->tender_status !== Property::TENDER_STATUS_AVAILABLE)
                            <div class="absolute tender-status top-2 right-2 primary-bg text-white p-2 rounded-l-2xl rounded-r-2xl z-10">{{ $property->tender_status }}</div>
                        @endif

                        @foreach($property->images as $image)
                            <div class="swiper-slide flex-shrink-0" style="height: unset;">
                                <img id="gallery-image-focus" src="{{ getPropertyImage($image->filepath, 'xl') }}" alt="img" loading="lazy"
                                     class="start-gallery w-full cursor-pointer h-full">
                            </div>
                        @endforeach
                    </div>

                    <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span>
                </div>
            </div>
        </div>
        <div class="container px-4 md:px-0 mx-auto">
            <div class="grid grid-cols-1 lg:grid-cols-3 gap-4">

                <div class="col-span-1 lg:col-span-2 pb-12 order-last lg:order-first">
                    <div class="pt-8">
                        <div class="mb-16">
                            @include(themeViewPath('frontend.components.development.features'))
                        </div>
                        <div class="pr-12">
                            @include(themeViewPath('frontend.components.development.description'))
                        </div>
                    </div>
                </div>

                <div class="col-span-1 order-2 order-first lg:order-last">
                    <div class="bg-whiter p-8">
                        <div class="">
                            <span class="block text-xs text-gray-400 uppercase">{{ trans('label.reference') }}: {{ $property->displayReference() }}</span>
                            <h3 class="text-2xl font-bold primary-text uppercase pt-6">{!! $property->displayPrice() !!}</h3>
                            <div>
                                <a href="javascript:" class="modal-button text-xs underline text-gray-400"
                                   data-target="mortgage-calculator-modal">{{ trans('header.mortgage_calculator') }}</a>
                            </div>

                            <div class="py-12">
                                @include(themeViewPath('frontend.components.development.bed-bath-info'))
                            </div>
                            <div>

                                <div class="grid grid-cols-1 md:grid-cols-2 gap-2">
                                    <div>
                                        <a href="javascript:" data-target="property-enquiry-form"
                                           class="{{ $buttonClasses }} block cta cta-text scroll-to">{{ trans('button.enquire_now') }}</a>
                                    </div>
                                    <div>
                                        @if(hasFeature(TenantFeature::FEATURE_SHORTLIST_SYSTEM))
                                            @if(user())
                                                @php
                                                    $shortlistText = in_array($property->uuid, $shortlistedProperties->pluck('property_uuid')->toArray())
                                                        ? trans('shortlist.saved')
                                                        : trans('shortlist.save')
                                                @endphp
                                                <a href="{{ localeUrl('/property/'.$property->url_key.'/toggle-shortlist') }}" class="{{ $buttonClasses }}">{{ $shortlistText }}</a>
                                            @else
                                                <a href="javascript:" data-target="preauth-modal"
                                                   class="modal-button {{ $buttonClasses }}">{{ Str::limit(trans('shortlist.save'), 14) }}</a>
                                            @endif
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="pt-8">
                                <hr class="pb-4">
                                <p class="text-xl leading-loose text-center header-text tracking-tight text-primary mb-2">{{ trans('generic.share_this_development') }}</p>
                                @include(themeViewPath('frontend.components.social.share'))
                            </div>

                        </div>
                    </div>

                </div>

            </div>

        </div>
    </section>

    @if($property->units->count() > 0)
        <section id="enquire_property bg-whiter">
            <div class="bg-whiter">
                <div class="container mx-auto">
                    <div class="pt-12 pb-8">
                        @include(themeViewPath('frontend.components.development.units'))
                    </div>
                </div>
            </div>
        </section>
    @endif

    <section id="enquire_property">
        <div class="bg-white">
            <div class="container mx-auto">
                <div class="w-full pb-20">
                    <div id="property-enquiry-form" class="w-full shadow-md border border-gray-100 py-8 px-4 lg:px-7 mt-4 bg-white">
                        <h3 class="header-text text-2xl leading-normal text-center tracking-tight text-primary mb-7">{{ trans('header.enquire_about_this_development') }}</h3>
                        <form action="{{ localeUrl('/property/'.$property->url_key.'/enquiry') }}" method="post" enctype="application/x-www-form-urlencoded" class="recaptcha">
                            <input type="text" name="name" placeholder="{{ trans('contact.full_name') }} *" value="{{ optional(user())->name() }}"
                                   class="rounded-full bg-whiter h-14 w-full px-4 mb-3 focus:outline-none" required>
                            <input type="email" name="email" placeholder="{{ trans('contact.email_address') }} *" value="{{ optional(user())->email }}"
                                   class="rounded-full bg-whiter h-14 w-full px-4 mb-3 focus:outline-none" required>
                            <input type="text" name="tel" placeholder="{{ trans('contact.telephone_number') }}" value="{{ optional(user())->tel }}"
                                   class="rounded-full bg-whiter h-14 w-full px-4 mb-3 focus:outline-none">
                            <textarea name="comment" cols="30" rows="10" placeholder="{{ trans('contact.your_message') }} *"
                                      class="rounded-3xl bg-whiter h-32 w-full px-4 py-3 mb-3 focus:outline-none" required></textarea>

                            @include(themeViewPath('frontend.forms.recaptcha.recaptcha-fallback-inputs'), ['inputClass' => 'rounded-full bg-whiter h-14 w-full px-4 mb-3 focus:outline-none'])

                            <div class="text-center">
                                <button type="submit"
                                        class="cta text-base text-center tracking-wide font-bold uppercase px-4 sm:px-9 rounded-full h-9 sm:h-12 right-1 top-1 cta-bg cta-text transition-all">{{ trans('button.send_enquiry') }}</button>
                            </div>
                            @csrf
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </section>

    {{-- site footer --}}
    @include(themeViewPath('frontend.components.footer'))

    {{-- CTA Modals --}}

    @if ($property->video_url !== null)
        @include(themeViewPath('frontend.components.modals.video'))
    @endif
    @if ($property->virtual_tour_url !== null)
        @include(themeViewPath('frontend.components.modals.virtual-tour'))
    @endif
    @if($useMap && $property->location->latitude !== null && $property->location->longitude !== null)
        @include(themeViewPath('frontend.components.modals.location'))
    @endif
    @if ($property->documents->count() > 0)
        @include(themeViewPath('frontend.components.modals.documents'))
    @endif
    @include(themeViewPath('frontend.components.modals.gallery-overlay'))
    @include(themeViewPath('frontend.components.modals.gallery'))
    @include(themeViewPath('frontend.components.modals.request-details'))
    @include(themeViewPath('frontend.components.modals.book-viewing'))
    @include(themeViewPath('frontend.components.modals.property-note'))
    @include(themeViewPath('frontend.components.modals.mortgage-calculator'))
    @include(themeViewPath('frontend.components.modals.stamp-duty-calculator'))
    @include(themeViewPath('frontend.components.modals.property-note'))
    @include(themeViewPath('frontend.components.modals.share-this-property'))

    @if (hasFeature(TenantFeature::FEATURE_PDF_BEHIND_FORM))
        @include(themeViewPath('frontend.components.modals.brochure-request'))
    @endif
@endsection
